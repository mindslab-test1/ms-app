var languageCode = "en";
var sounds_path = "/srv/maum/KB_POC/sounds/";
var message;
var fd;

function sendAppEvent(msg)
{
   e = new Event("CUSTOM", "MyEvent::Notify");
   e.addHeader("My-Header", "IVR App Data");
   e.addHeader("Unique-ID", session.uuid);
   e.addBody(msg + '\n');
   e.fire();
}

function playFile(fileName, callback, callbackArgs)
{
    sendAppEvent("Playback: " + sounds_path + fileName);
    console_log('info', 'Playback file: ' + sounds_path + fileName + '\n');
    session.streamFile(sounds_path + fileName, callback, callbackArgs);
}

console_log('notice', 'Session UUID: ' + session.uuid + '\n');
sendAppEvent("> Answering call");
session.answer();
sendAppEvent("> Sleep the call");
session.sleep(100);

session.execute('set', 'playback_terminators=none');
session.execute('playback', sounds_path + 'Normal_1.0_20201214_93220607_890.wav');
sendAppEvent("Speak(google_tts, ko-KR-Standard-B): Welcomse to my IVR test!");
//playFile("Normal_1.0_20201214_93220607_890.wav");
//sendAppEvent("Speak(google_tts, ko-KR-Standard-B): Welcomse to my IVR test!");
//session.speak("google_tts", "ko-KR-Standard-B", "한글을 사랑합시다.");

/*
sendAppEvent("Speak(flite, awb): Please click the call button to make a call to see visually!");
session.speak("flite", "awb", "Please click the call button to make a call to see visually.");
sendAppEvent("Speak(flite, awb): You can select the menu using DTMF key or voices!");
session.speak("flite", "awb", "You can select the menu using DTMF key or voices.");
*/
sendAppEvent("> End of Call");

exit();
