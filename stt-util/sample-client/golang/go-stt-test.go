package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"time"
	"sync"
	"io/ioutil"

	pb "./brain_stt"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

const (
	address      = "localhost:9801"
	defaultFname = "../../../pysrc/samples/8k.pcm"
)

var mutex = &sync.Mutex{}
var Fname *string
var Dname *string
var total_time time.Duration
var Lang string
var Model string
var SampleRate string
var Host string

func test_stream() {
	start := time.Now()
	conn, err := grpc.Dial(Host, grpc.WithInsecure())
	defer conn.Close()
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return
	}
	c := pb.NewSpeechToTextServiceClient(conn)
	md := metadata.Pairs(
		"in.model", Model,
		"in.lang", Lang,
		"in.samplerate", SampleRate,
	)
	ctx := metadata.NewOutgoingContext(context.Background(), md)
	stream, err := c.StreamRecognize(ctx)
	if err != nil {
		fmt.Println("could not recog: %v", err)
	}
	waitc := make(chan struct{})
	go func() {
		for {
			in, err := stream.Recv()
			if err == io.EOF {
				close(waitc)
				return
			}
			if err != nil {
				log.Fatalf("Fail to message: %v", err)
			}
			log.Printf("[%.02f - %.02f] Got message: %s",
				float64(in.Start)/100.0, float64(in.End)/100.0, in.Txt)
		}
	}()

	f, err := os.Open(*Fname)
	buf := make([]byte, 320)
	for {
		n, err := f.Read(buf)
		if err != nil {
			if err == io.EOF {
				break
			} else {
				fmt.Println(err)
				break
			}
		}
		if n < 0 {
			fmt.Println("error")
		}
		audio := &pb.Speech{Bin: buf}
		if err := stream.Send(audio); err != nil {
			fmt.Println(err)
		}
		time.Sleep(time.Duration(n/16) * time.Millisecond)
	}
	stream.CloseSend()
	<-waitc
	elapsed := time.Since(start)
	mutex.Lock()
	total_time += elapsed
	mutex.Unlock()
	log.Printf("elapsed time is %s", elapsed)
	if err != nil {
		fmt.Println(err)
	}
}

func test_simple() {
	start := time.Now()

	// Set up a connection to the server.
	conn, err := grpc.Dial(Host, grpc.WithInsecure())
	defer conn.Close()
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return
	}
	c := pb.NewSpeechToTextServiceClient(conn)

	md := metadata.Pairs(
		"in.model", Model,
		"in.lang", Lang,
		"in.samplerate", SampleRate,
	)
	ctx := metadata.NewOutgoingContext(context.Background(), md)
	stream, err := c.SimpleRecognize(ctx)
	if err != nil {
		fmt.Println("could not recog: %v", err)
		return
	}

	f, err := os.Open(*Fname)
	buf := make([]byte, 320)
	for {
		n, err := f.Read(buf)
		if err != nil {
			if err == io.EOF {
				break
			} else {
				fmt.Println(err)
				break
			}
		}
		if n < 0 {
			fmt.Println("error")
		}

		audio := &pb.Speech{Bin: buf}
		if err := stream.Send(audio); err != nil {
			fmt.Println(err)
		}
	}
	result, err := stream.CloseAndRecv()
	elapsed := time.Since(start)
	mutex.Lock()
	total_time += elapsed
	mutex.Unlock()
	log.Printf("elapsed time is %s", elapsed)
	if err != nil {
		fmt.Println(err)
	} else {
		log.Printf("resulit is %s", result.Txt)
	}
}

func main() {
	total_time = 0
	chan_cnt := flag.Int("c", 1, "thread count")
	loop_cnt := flag.Int("l", 1, "loop count")
	Fname = flag.String("f", defaultFname, "file path")
	Dname = flag.String("d", "", "directory path")
	service := flag.String("s", "simple", "service name (simple, stream)")
	flag.StringVar(&Lang, "k", "kor", "language");
	flag.StringVar(&Model, "m", "baseline", "model");
	flag.StringVar(&SampleRate, "r", "8000", "sample rate");
	flag.StringVar(&Host, "h", address, "stt server address");

	flag.Parse()
	log.SetFlags(log.Lmicroseconds)

	var files []string
	if *Dname != "" {
		pcmFiles, err := ioutil.ReadDir(*Dname)
		if err != nil {
			log.Fatal(err)
		} else {
			for _, f := range pcmFiles {
				files = append(files, *Dname + "/" + f.Name())
			}
		}
	} else {
		files = append(files, *Fname)
	}

	done := make(chan bool)

	for i := 0; i < *chan_cnt; i++ {
		go func() {
			for n := 0; n < *loop_cnt; n++ {
				for _, fname := range files {
					*Fname = fname
					if *service == "simple" {
						test_simple()
					} else if *service == "stream" {
						test_stream()
					}
				}
			}
			done <- true
		}()
	}

	// Wait All Routine
	for i := 0; i < *chan_cnt; i++ {
		<-done
	}

	log.Printf("total elapsed time is %s", total_time)
	log.Printf("average elapsed time is %s",
		time.Duration(float64(total_time) / float64(*chan_cnt * *loop_cnt)))
}
