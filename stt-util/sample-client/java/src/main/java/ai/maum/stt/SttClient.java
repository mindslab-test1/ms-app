package ai.maum.stt;

import com.google.protobuf.ByteString;
import io.grpc.*;
import io.grpc.stub.StreamObserver;
import maum.brain.stt.SpeechToTextServiceGrpc;
import maum.brain.stt.Stt;

import java.io.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SttClient {
    private static final Logger logger = Logger.getLogger(SttClient.class.getName());

    private final ManagedChannel channel;
    private final SpeechToTextServiceGrpc.SpeechToTextServiceStub asyncStub;

    public SttClient(String host, int port) {
        this(ManagedChannelBuilder.forAddress(host, port)
                // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
                // needing certificates.
                .usePlaintext()
                .build());
    }

    /**
     * Construct client for accessing HelloWorld server using the existing channel.
     */
    public SttClient(ManagedChannel channel) {
        this.channel = channel;
        asyncStub = SpeechToTextServiceGrpc.newStub(channel).withCallCredentials(new CallCredentials() {
            @Override
            public void applyRequestMetadata(RequestInfo requestInfo, Executor appExecutor, MetadataApplier applier) {
                Metadata headers = new Metadata();
                Metadata.Key<String> model      = Metadata.Key.of("in.model", Metadata.ASCII_STRING_MARSHALLER);
                Metadata.Key<String> lang       = Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER);
                Metadata.Key<String> sampleRate = Metadata.Key.of("in.samplerate", Metadata.ASCII_STRING_MARSHALLER);
                headers.put(model, "baseline");
                headers.put(lang, "kor");
                headers.put(sampleRate, "8000");
                applier.apply(headers);
            }

            @Override
            public void thisUsesUnstableApi() {

            }
        });
    }

    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    public CountDownLatch streamRecognize() {
        info("*** streamRecognize");
        final CountDownLatch finishLatch = new CountDownLatch(1);
        StreamObserver<Stt.Speech> requestObserver =
                asyncStub.streamRecognize(new StreamObserver<Stt.Segment>() {
                    @Override
                    public void onNext(Stt.Segment segment) {
                        info("Got message \"{0}\" at {1}, {2}", segment.getStart(), segment.getEnd(), segment.getTxt());
                    }

                    @Override
                    public void onError(Throwable t) {
                        finishLatch.countDown();
                    }

                    @Override
                    public void onCompleted() {
                        info("Finished RouteChat");
                        finishLatch.countDown();
                    }
                });

        try {
            BufferedInputStream bs = null;

            try {
                bs = new BufferedInputStream(new FileInputStream("8k.pcm"));
                byte[] buffer = new byte[1024];
                int length;
                while ((length = bs.read(buffer, 0, 1024)) != -1) {
                    Stt.Speech.Builder speech = Stt.Speech.newBuilder();
                    requestObserver.onNext(
                            Stt.Speech.newBuilder().setBin(ByteString.copyFrom(buffer, 0, length)).build()
                    );
                }
                bs.close();
            } catch (Exception e) {
                System.out.println(e);
            }
        } catch (RuntimeException e) {
            // Cancel RPC
            requestObserver.onError(e);
            throw e;
        }
        // Mark the end of requests
        requestObserver.onCompleted();

        // return the latch while receiving happens asynchronously
        return finishLatch;
    }

    /**
     * Greet server. If provided, the first element of {@code args} is the name to use in the
     * greeting.
     */
    public static void main(String[] args) throws Exception {
        SttClient client = new SttClient("localhost", 9801);
        try {
            /* Access a service running on the local machine on port 50051 */
            // Send and receive some notes.
            CountDownLatch finishLatch = client.streamRecognize();

            if (!finishLatch.await(1, TimeUnit.MINUTES)) {
                client.warning("streamRecognize can not finish within 1 minutes");
            }
        } finally {
            client.shutdown();
        }
    }

    private void info(String msg, Object... params) {
        logger.log(Level.INFO, msg, params);
    }

    private void warning(String msg, Object... params) {
        logger.log(Level.WARNING, msg, params);
    }
}


