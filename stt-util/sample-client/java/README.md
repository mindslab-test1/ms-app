# brain stt -  java sample client

## build

```bash
mvn compile
mvn package
```

## run (STT 서버는 127.0.0.1:9801, 샘플 음성은 8k.pcm 준비)

```bash
java -jar stt-0.0.1-SNAPSHOT-jar-with-dependencies.jar
```

