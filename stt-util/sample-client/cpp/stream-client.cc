#include <memory>
#include <iostream>
#include <thread>
#include <stdio.h>
#include <unistd.h>

#include <grpc++/grpc++.h>
#include "stt.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReaderWriter;

using maum::brain::stt::SpeechToTextService;
using maum::brain::stt::Speech;
using maum::brain::stt::Text;
using maum::brain::stt::Segment;

void ReadStream(ClientReaderWriter<Speech, Segment> *stream)
{
  Segment segment;
  while (stream->Read(&segment)) {
    std::cout << "Result : " << segment.txt() << std::endl;
  }
  std::cout << "stream read end..." << std::endl;
}

int main(int argc, char *argv[])
{
  auto channel = grpc::CreateChannel("125.132.250.242:15001", grpc::InsecureChannelCredentials());
  auto stub = SpeechToTextService::NewStub(channel);

  ClientContext ctx;
  ctx.AddMetadata("in.lang", "kor");
  ctx.AddMetadata("in.samplerate", "8000");
  ctx.AddMetadata("in.model", "callbot");

  auto stream = stub->StreamRecognize(&ctx);

  int read_cnt;
  const char *filename = "8k.pcm";
  int16_t buf[1024];
  FILE *fp = fopen(filename, "rb");
  if (fp == NULL) {
    std::cerr << "cannot open file : " << filename << std::endl;
    return -1;
  }

  std::thread result_thrd(ReadStream, stream.get());

  while ((read_cnt = fread(buf, 2, 80 * 10 /* 0.01초 * 10 */, fp)) > 0) {
    Speech speech;
    speech.set_bin(buf, read_cnt * sizeof(int16_t));
    stream->Write(speech);
  }

  stream->WritesDone();

  result_thrd.join();

  auto status = stream->Finish();
  if (status.ok()) {
    std::cout << "result: OK" << std::endl;
  } else {
    std::cout << "failed: " << status.error_message() << std::endl;
  }

  return 0;
}
