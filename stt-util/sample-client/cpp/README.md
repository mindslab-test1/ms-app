# grpc c++ sample client

## dependency 
- protobuf
- grpc++
- pthread

## build.sh 실행
- build proto, simple client1(c++), stream client2(c++)

## Sample Client 실행
- simple client : simple speaking file(8k.pcm)에 대해 Text 결과 출력
- stream client : stream file(201800035684339I.wav)에 대해 Text 결과 출력
- 실행예 1 ) ./sample-client 
- 실행결과 ) result: 오늘 날씨 어때
- 실행예 2 ) ./stream-client 
- 실행결과 ) Speaking Time ---> 0 seconds
             Speaking Time ---> 1 seconds
             Speaking Time ---> 2 seconds
             Speaking Time ---> 3 seconds
             Speaking Time ---> 4 seconds
             Speaking Time ---> 5 seconds
             Result : 행복을 드리는 신한 카드 김진영입니다
             Result : 무엇을 도와드릴까요
             Speaking Time ---> 6 seconds
             ...
             Speaking Time ---> 112 seconds
             stream read end...
             result: OK

## config in stt-client1.cc, stt-client2.cc
- stt server ip : port
- stt server meta data (language, sample rate, model name)
- sample file name


