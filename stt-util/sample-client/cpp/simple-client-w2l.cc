#include <memory>
#include <iostream>
#include <thread>
#include <stdio.h>

#include <grpc++/grpc++.h>
#include "w2l.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;

using maum::brain::w2l::SpeechToText;
using maum::brain::w2l::Speech;
using maum::brain::w2l::Text;

int main(int argc, char** argv) {

  auto channel = grpc::CreateChannel("127.0.0.1:9801", grpc::InsecureChannelCredentials());
  auto stub = SpeechToText::NewStub(channel);

  ClientContext ctx;

  ctx.AddMetadata("in.lang", "kor");
  ctx.AddMetadata("in.samplerate", "8000");
  ctx.AddMetadata("in.model", "baseline");

  Text resp;
  auto writer = stub->Recognize(&ctx, &resp);

  int read_cnt;
  char filename[1024] = "8k.pcm";
  int16_t buf[500];
  FILE *fp = fopen(filename, "rb");

  if (fp == NULL) {
    std::cerr << "cannot open file : " << filename << std::endl;
    return -1;
  }

  while ((read_cnt = fread(buf, 2, 500, fp)) > 0) {
    Speech speech;
    speech.set_bin(buf, size_t(read_cnt * sizeof(int16_t)));
    writer->Write(speech);
  }

  writer->WritesDone();

  auto status = writer->Finish();
  if (status.ok()) {
    std::cout << "result: " << resp.txt() << std::endl;
  } else {
    std::cout << "failed: " << status.error_message() << std::endl;
  }

  return 0;
}
