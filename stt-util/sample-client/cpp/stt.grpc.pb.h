// Generated by the gRPC C++ plugin.
// If you make any local change, they will be lost.
// source: stt.proto
#ifndef GRPC_stt_2eproto__INCLUDED
#define GRPC_stt_2eproto__INCLUDED

#include "stt.pb.h"

#include <functional>
#include <grpcpp/impl/codegen/async_generic_service.h>
#include <grpcpp/impl/codegen/async_stream.h>
#include <grpcpp/impl/codegen/async_unary_call.h>
#include <grpcpp/impl/codegen/client_callback.h>
#include <grpcpp/impl/codegen/client_context.h>
#include <grpcpp/impl/codegen/completion_queue.h>
#include <grpcpp/impl/codegen/method_handler_impl.h>
#include <grpcpp/impl/codegen/proto_utils.h>
#include <grpcpp/impl/codegen/rpc_method.h>
#include <grpcpp/impl/codegen/server_callback.h>
#include <grpcpp/impl/codegen/server_context.h>
#include <grpcpp/impl/codegen/service_type.h>
#include <grpcpp/impl/codegen/status.h>
#include <grpcpp/impl/codegen/stub_options.h>
#include <grpcpp/impl/codegen/sync_stream.h>

namespace grpc_impl {
class CompletionQueue;
class ServerCompletionQueue;
class ServerContext;
}  // namespace grpc_impl

namespace grpc {
namespace experimental {
template <typename RequestT, typename ResponseT>
class MessageAllocator;
}  // namespace experimental
}  // namespace grpc

namespace maum {
namespace brain {
namespace stt {

//
// the following service should meta datas
//
// in.lang = (kor, eng)
// in.samplerate = 16000, 8000, default 8000
// in.model = default or weather baseline and so on
// in.seconds = integer , real seconds of stream
//
// When calling the DetailRecongnize function, send bottom meta data
// in.require.raw_mlf = raw mlf
//
//
// header
class SpeechToTextService final {
 public:
  static constexpr char const* service_full_name() {
    return "maum.brain.stt.SpeechToTextService";
  }
  class StubInterface {
   public:
    virtual ~StubInterface() {}
    std::unique_ptr< ::grpc::ClientWriterInterface< ::maum::brain::stt::Speech>> SimpleRecognize(::grpc::ClientContext* context, ::maum::brain::stt::Text* response) {
      return std::unique_ptr< ::grpc::ClientWriterInterface< ::maum::brain::stt::Speech>>(SimpleRecognizeRaw(context, response));
    }
    std::unique_ptr< ::grpc::ClientAsyncWriterInterface< ::maum::brain::stt::Speech>> AsyncSimpleRecognize(::grpc::ClientContext* context, ::maum::brain::stt::Text* response, ::grpc::CompletionQueue* cq, void* tag) {
      return std::unique_ptr< ::grpc::ClientAsyncWriterInterface< ::maum::brain::stt::Speech>>(AsyncSimpleRecognizeRaw(context, response, cq, tag));
    }
    std::unique_ptr< ::grpc::ClientAsyncWriterInterface< ::maum::brain::stt::Speech>> PrepareAsyncSimpleRecognize(::grpc::ClientContext* context, ::maum::brain::stt::Text* response, ::grpc::CompletionQueue* cq) {
      return std::unique_ptr< ::grpc::ClientAsyncWriterInterface< ::maum::brain::stt::Speech>>(PrepareAsyncSimpleRecognizeRaw(context, response, cq));
    }
    std::unique_ptr< ::grpc::ClientReaderWriterInterface< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>> StreamRecognize(::grpc::ClientContext* context) {
      return std::unique_ptr< ::grpc::ClientReaderWriterInterface< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>>(StreamRecognizeRaw(context));
    }
    std::unique_ptr< ::grpc::ClientAsyncReaderWriterInterface< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>> AsyncStreamRecognize(::grpc::ClientContext* context, ::grpc::CompletionQueue* cq, void* tag) {
      return std::unique_ptr< ::grpc::ClientAsyncReaderWriterInterface< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>>(AsyncStreamRecognizeRaw(context, cq, tag));
    }
    std::unique_ptr< ::grpc::ClientAsyncReaderWriterInterface< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>> PrepareAsyncStreamRecognize(::grpc::ClientContext* context, ::grpc::CompletionQueue* cq) {
      return std::unique_ptr< ::grpc::ClientAsyncReaderWriterInterface< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>>(PrepareAsyncStreamRecognizeRaw(context, cq));
    }
    class experimental_async_interface {
     public:
      virtual ~experimental_async_interface() {}
      virtual void SimpleRecognize(::grpc::ClientContext* context, ::maum::brain::stt::Text* response, ::grpc::experimental::ClientWriteReactor< ::maum::brain::stt::Speech>* reactor) = 0;
      virtual void StreamRecognize(::grpc::ClientContext* context, ::grpc::experimental::ClientBidiReactor< ::maum::brain::stt::Speech,::maum::brain::stt::Segment>* reactor) = 0;
    };
    virtual class experimental_async_interface* experimental_async() { return nullptr; }
  private:
    virtual ::grpc::ClientWriterInterface< ::maum::brain::stt::Speech>* SimpleRecognizeRaw(::grpc::ClientContext* context, ::maum::brain::stt::Text* response) = 0;
    virtual ::grpc::ClientAsyncWriterInterface< ::maum::brain::stt::Speech>* AsyncSimpleRecognizeRaw(::grpc::ClientContext* context, ::maum::brain::stt::Text* response, ::grpc::CompletionQueue* cq, void* tag) = 0;
    virtual ::grpc::ClientAsyncWriterInterface< ::maum::brain::stt::Speech>* PrepareAsyncSimpleRecognizeRaw(::grpc::ClientContext* context, ::maum::brain::stt::Text* response, ::grpc::CompletionQueue* cq) = 0;
    virtual ::grpc::ClientReaderWriterInterface< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>* StreamRecognizeRaw(::grpc::ClientContext* context) = 0;
    virtual ::grpc::ClientAsyncReaderWriterInterface< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>* AsyncStreamRecognizeRaw(::grpc::ClientContext* context, ::grpc::CompletionQueue* cq, void* tag) = 0;
    virtual ::grpc::ClientAsyncReaderWriterInterface< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>* PrepareAsyncStreamRecognizeRaw(::grpc::ClientContext* context, ::grpc::CompletionQueue* cq) = 0;
  };
  class Stub final : public StubInterface {
   public:
    Stub(const std::shared_ptr< ::grpc::ChannelInterface>& channel);
    std::unique_ptr< ::grpc::ClientWriter< ::maum::brain::stt::Speech>> SimpleRecognize(::grpc::ClientContext* context, ::maum::brain::stt::Text* response) {
      return std::unique_ptr< ::grpc::ClientWriter< ::maum::brain::stt::Speech>>(SimpleRecognizeRaw(context, response));
    }
    std::unique_ptr< ::grpc::ClientAsyncWriter< ::maum::brain::stt::Speech>> AsyncSimpleRecognize(::grpc::ClientContext* context, ::maum::brain::stt::Text* response, ::grpc::CompletionQueue* cq, void* tag) {
      return std::unique_ptr< ::grpc::ClientAsyncWriter< ::maum::brain::stt::Speech>>(AsyncSimpleRecognizeRaw(context, response, cq, tag));
    }
    std::unique_ptr< ::grpc::ClientAsyncWriter< ::maum::brain::stt::Speech>> PrepareAsyncSimpleRecognize(::grpc::ClientContext* context, ::maum::brain::stt::Text* response, ::grpc::CompletionQueue* cq) {
      return std::unique_ptr< ::grpc::ClientAsyncWriter< ::maum::brain::stt::Speech>>(PrepareAsyncSimpleRecognizeRaw(context, response, cq));
    }
    std::unique_ptr< ::grpc::ClientReaderWriter< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>> StreamRecognize(::grpc::ClientContext* context) {
      return std::unique_ptr< ::grpc::ClientReaderWriter< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>>(StreamRecognizeRaw(context));
    }
    std::unique_ptr<  ::grpc::ClientAsyncReaderWriter< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>> AsyncStreamRecognize(::grpc::ClientContext* context, ::grpc::CompletionQueue* cq, void* tag) {
      return std::unique_ptr< ::grpc::ClientAsyncReaderWriter< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>>(AsyncStreamRecognizeRaw(context, cq, tag));
    }
    std::unique_ptr<  ::grpc::ClientAsyncReaderWriter< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>> PrepareAsyncStreamRecognize(::grpc::ClientContext* context, ::grpc::CompletionQueue* cq) {
      return std::unique_ptr< ::grpc::ClientAsyncReaderWriter< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>>(PrepareAsyncStreamRecognizeRaw(context, cq));
    }
    class experimental_async final :
      public StubInterface::experimental_async_interface {
     public:
      void SimpleRecognize(::grpc::ClientContext* context, ::maum::brain::stt::Text* response, ::grpc::experimental::ClientWriteReactor< ::maum::brain::stt::Speech>* reactor) override;
      void StreamRecognize(::grpc::ClientContext* context, ::grpc::experimental::ClientBidiReactor< ::maum::brain::stt::Speech,::maum::brain::stt::Segment>* reactor) override;
     private:
      friend class Stub;
      explicit experimental_async(Stub* stub): stub_(stub) { }
      Stub* stub() { return stub_; }
      Stub* stub_;
    };
    class experimental_async_interface* experimental_async() override { return &async_stub_; }

   private:
    std::shared_ptr< ::grpc::ChannelInterface> channel_;
    class experimental_async async_stub_{this};
    ::grpc::ClientWriter< ::maum::brain::stt::Speech>* SimpleRecognizeRaw(::grpc::ClientContext* context, ::maum::brain::stt::Text* response) override;
    ::grpc::ClientAsyncWriter< ::maum::brain::stt::Speech>* AsyncSimpleRecognizeRaw(::grpc::ClientContext* context, ::maum::brain::stt::Text* response, ::grpc::CompletionQueue* cq, void* tag) override;
    ::grpc::ClientAsyncWriter< ::maum::brain::stt::Speech>* PrepareAsyncSimpleRecognizeRaw(::grpc::ClientContext* context, ::maum::brain::stt::Text* response, ::grpc::CompletionQueue* cq) override;
    ::grpc::ClientReaderWriter< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>* StreamRecognizeRaw(::grpc::ClientContext* context) override;
    ::grpc::ClientAsyncReaderWriter< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>* AsyncStreamRecognizeRaw(::grpc::ClientContext* context, ::grpc::CompletionQueue* cq, void* tag) override;
    ::grpc::ClientAsyncReaderWriter< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>* PrepareAsyncStreamRecognizeRaw(::grpc::ClientContext* context, ::grpc::CompletionQueue* cq) override;
    const ::grpc::internal::RpcMethod rpcmethod_SimpleRecognize_;
    const ::grpc::internal::RpcMethod rpcmethod_StreamRecognize_;
  };
  static std::unique_ptr<Stub> NewStub(const std::shared_ptr< ::grpc::ChannelInterface>& channel, const ::grpc::StubOptions& options = ::grpc::StubOptions());

  class Service : public ::grpc::Service {
   public:
    Service();
    virtual ~Service();
    virtual ::grpc::Status SimpleRecognize(::grpc::ServerContext* context, ::grpc::ServerReader< ::maum::brain::stt::Speech>* reader, ::maum::brain::stt::Text* response);
    virtual ::grpc::Status StreamRecognize(::grpc::ServerContext* context, ::grpc::ServerReaderWriter< ::maum::brain::stt::Segment, ::maum::brain::stt::Speech>* stream);
  };
  template <class BaseClass>
  class WithAsyncMethod_SimpleRecognize : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    WithAsyncMethod_SimpleRecognize() {
      ::grpc::Service::MarkMethodAsync(0);
    }
    ~WithAsyncMethod_SimpleRecognize() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status SimpleRecognize(::grpc::ServerContext* /*context*/, ::grpc::ServerReader< ::maum::brain::stt::Speech>* /*reader*/, ::maum::brain::stt::Text* /*response*/) override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    void RequestSimpleRecognize(::grpc::ServerContext* context, ::grpc::ServerAsyncReader< ::maum::brain::stt::Text, ::maum::brain::stt::Speech>* reader, ::grpc::CompletionQueue* new_call_cq, ::grpc::ServerCompletionQueue* notification_cq, void *tag) {
      ::grpc::Service::RequestAsyncClientStreaming(0, context, reader, new_call_cq, notification_cq, tag);
    }
  };
  template <class BaseClass>
  class WithAsyncMethod_StreamRecognize : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    WithAsyncMethod_StreamRecognize() {
      ::grpc::Service::MarkMethodAsync(1);
    }
    ~WithAsyncMethod_StreamRecognize() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status StreamRecognize(::grpc::ServerContext* /*context*/, ::grpc::ServerReaderWriter< ::maum::brain::stt::Segment, ::maum::brain::stt::Speech>* /*stream*/)  override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    void RequestStreamRecognize(::grpc::ServerContext* context, ::grpc::ServerAsyncReaderWriter< ::maum::brain::stt::Segment, ::maum::brain::stt::Speech>* stream, ::grpc::CompletionQueue* new_call_cq, ::grpc::ServerCompletionQueue* notification_cq, void *tag) {
      ::grpc::Service::RequestAsyncBidiStreaming(1, context, stream, new_call_cq, notification_cq, tag);
    }
  };
  typedef WithAsyncMethod_SimpleRecognize<WithAsyncMethod_StreamRecognize<Service > > AsyncService;
  template <class BaseClass>
  class ExperimentalWithCallbackMethod_SimpleRecognize : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    ExperimentalWithCallbackMethod_SimpleRecognize() {
      ::grpc::Service::experimental().MarkMethodCallback(0,
        new ::grpc_impl::internal::CallbackClientStreamingHandler< ::maum::brain::stt::Speech, ::maum::brain::stt::Text>(
          [this] { return this->SimpleRecognize(); }));
    }
    ~ExperimentalWithCallbackMethod_SimpleRecognize() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status SimpleRecognize(::grpc::ServerContext* /*context*/, ::grpc::ServerReader< ::maum::brain::stt::Speech>* /*reader*/, ::maum::brain::stt::Text* /*response*/) override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    virtual ::grpc::experimental::ServerReadReactor< ::maum::brain::stt::Speech, ::maum::brain::stt::Text>* SimpleRecognize() {
      return new ::grpc_impl::internal::UnimplementedReadReactor<
        ::maum::brain::stt::Speech, ::maum::brain::stt::Text>;}
  };
  template <class BaseClass>
  class ExperimentalWithCallbackMethod_StreamRecognize : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    ExperimentalWithCallbackMethod_StreamRecognize() {
      ::grpc::Service::experimental().MarkMethodCallback(1,
        new ::grpc_impl::internal::CallbackBidiHandler< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>(
          [this] { return this->StreamRecognize(); }));
    }
    ~ExperimentalWithCallbackMethod_StreamRecognize() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status StreamRecognize(::grpc::ServerContext* /*context*/, ::grpc::ServerReaderWriter< ::maum::brain::stt::Segment, ::maum::brain::stt::Speech>* /*stream*/)  override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    virtual ::grpc::experimental::ServerBidiReactor< ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>* StreamRecognize() {
      return new ::grpc_impl::internal::UnimplementedBidiReactor<
        ::maum::brain::stt::Speech, ::maum::brain::stt::Segment>;}
  };
  typedef ExperimentalWithCallbackMethod_SimpleRecognize<ExperimentalWithCallbackMethod_StreamRecognize<Service > > ExperimentalCallbackService;
  template <class BaseClass>
  class WithGenericMethod_SimpleRecognize : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    WithGenericMethod_SimpleRecognize() {
      ::grpc::Service::MarkMethodGeneric(0);
    }
    ~WithGenericMethod_SimpleRecognize() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status SimpleRecognize(::grpc::ServerContext* /*context*/, ::grpc::ServerReader< ::maum::brain::stt::Speech>* /*reader*/, ::maum::brain::stt::Text* /*response*/) override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
  };
  template <class BaseClass>
  class WithGenericMethod_StreamRecognize : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    WithGenericMethod_StreamRecognize() {
      ::grpc::Service::MarkMethodGeneric(1);
    }
    ~WithGenericMethod_StreamRecognize() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status StreamRecognize(::grpc::ServerContext* /*context*/, ::grpc::ServerReaderWriter< ::maum::brain::stt::Segment, ::maum::brain::stt::Speech>* /*stream*/)  override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
  };
  template <class BaseClass>
  class WithRawMethod_SimpleRecognize : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    WithRawMethod_SimpleRecognize() {
      ::grpc::Service::MarkMethodRaw(0);
    }
    ~WithRawMethod_SimpleRecognize() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status SimpleRecognize(::grpc::ServerContext* /*context*/, ::grpc::ServerReader< ::maum::brain::stt::Speech>* /*reader*/, ::maum::brain::stt::Text* /*response*/) override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    void RequestSimpleRecognize(::grpc::ServerContext* context, ::grpc::ServerAsyncReader< ::grpc::ByteBuffer, ::grpc::ByteBuffer>* reader, ::grpc::CompletionQueue* new_call_cq, ::grpc::ServerCompletionQueue* notification_cq, void *tag) {
      ::grpc::Service::RequestAsyncClientStreaming(0, context, reader, new_call_cq, notification_cq, tag);
    }
  };
  template <class BaseClass>
  class WithRawMethod_StreamRecognize : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    WithRawMethod_StreamRecognize() {
      ::grpc::Service::MarkMethodRaw(1);
    }
    ~WithRawMethod_StreamRecognize() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status StreamRecognize(::grpc::ServerContext* /*context*/, ::grpc::ServerReaderWriter< ::maum::brain::stt::Segment, ::maum::brain::stt::Speech>* /*stream*/)  override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    void RequestStreamRecognize(::grpc::ServerContext* context, ::grpc::ServerAsyncReaderWriter< ::grpc::ByteBuffer, ::grpc::ByteBuffer>* stream, ::grpc::CompletionQueue* new_call_cq, ::grpc::ServerCompletionQueue* notification_cq, void *tag) {
      ::grpc::Service::RequestAsyncBidiStreaming(1, context, stream, new_call_cq, notification_cq, tag);
    }
  };
  template <class BaseClass>
  class ExperimentalWithRawCallbackMethod_SimpleRecognize : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    ExperimentalWithRawCallbackMethod_SimpleRecognize() {
      ::grpc::Service::experimental().MarkMethodRawCallback(0,
        new ::grpc_impl::internal::CallbackClientStreamingHandler< ::grpc::ByteBuffer, ::grpc::ByteBuffer>(
          [this] { return this->SimpleRecognize(); }));
    }
    ~ExperimentalWithRawCallbackMethod_SimpleRecognize() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status SimpleRecognize(::grpc::ServerContext* /*context*/, ::grpc::ServerReader< ::maum::brain::stt::Speech>* /*reader*/, ::maum::brain::stt::Text* /*response*/) override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    virtual ::grpc::experimental::ServerReadReactor< ::grpc::ByteBuffer, ::grpc::ByteBuffer>* SimpleRecognize() {
      return new ::grpc_impl::internal::UnimplementedReadReactor<
        ::grpc::ByteBuffer, ::grpc::ByteBuffer>;}
  };
  template <class BaseClass>
  class ExperimentalWithRawCallbackMethod_StreamRecognize : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    ExperimentalWithRawCallbackMethod_StreamRecognize() {
      ::grpc::Service::experimental().MarkMethodRawCallback(1,
        new ::grpc_impl::internal::CallbackBidiHandler< ::grpc::ByteBuffer, ::grpc::ByteBuffer>(
          [this] { return this->StreamRecognize(); }));
    }
    ~ExperimentalWithRawCallbackMethod_StreamRecognize() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status StreamRecognize(::grpc::ServerContext* /*context*/, ::grpc::ServerReaderWriter< ::maum::brain::stt::Segment, ::maum::brain::stt::Speech>* /*stream*/)  override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    virtual ::grpc::experimental::ServerBidiReactor< ::grpc::ByteBuffer, ::grpc::ByteBuffer>* StreamRecognize() {
      return new ::grpc_impl::internal::UnimplementedBidiReactor<
        ::grpc::ByteBuffer, ::grpc::ByteBuffer>;}
  };
  typedef Service StreamedUnaryService;
  typedef Service SplitStreamedService;
  typedef Service StreamedService;
};

}  // namespace stt
}  // namespace brain
}  // namespace maum


#endif  // GRPC_stt_2eproto__INCLUDED
