#!/bin/sh
protoc --cpp_out=. --grpc_out=. --plugin=protoc-gen-grpc=/usr/local/bin/grpc_cpp_plugin w2l.proto
g++ -std=c++11 -I /usr/local/include/ -L /usr/local/lib/ simple-client-w2l.cc w2l.grpc.pb.cc w2l.pb.cc -o simple-client-w2l -l grpc++ -l protobuf
g++ -std=c++11 -I /usr/local/include/ -L /usr/local/lib/ stream-client-w2l.cc w2l.grpc.pb.cc w2l.pb.cc -o stream-client-w2l -l pthread -l grpc++ -l protobuf
