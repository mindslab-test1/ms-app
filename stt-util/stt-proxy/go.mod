module mindslab-ai/stt-proxy

go 1.12

require (
	github.com/bigkevmcd/go-configparser v0.0.0-20200217161103-d137835d2579
	github.com/go-sql-driver/mysql v1.5.0
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/spf13/viper v1.6.3
	github.com/zaf/g711 v0.0.0-20190814101024-76a4a538f52b
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e
	golang.org/x/tools v0.0.0-20190524140312-2c0ae7006135 // indirect
	google.golang.org/grpc v1.28.1
	honnef.co/go/tools v0.0.0-20190523083050-ea95bdfd59fc // indirect
	maum v0.0.0-00010101000000-000000000000
)

replace maum => ./maum
