-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: 10.122.64.152    Database: HAPPYCALL4
-- ------------------------------------------------------
-- Server version	5.6.40

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `STT_PROXY_INFO`
--

DROP TABLE IF EXISTS `STT_PROXY_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STT_PROXY_INFO` (
  `ID` varchar(100) NOT NULL,
  `PROTO` int(11) NOT NULL DEFAULT '0' COMMENT 'DNN:0, LSTM:1, CNN:2',
  `ADDR` varchar(100) NOT NULL,
  `MODEL` varchar(100) NOT NULL DEFAULT '',
  `LANG` varchar(100) NOT NULL DEFAULT '',
  `SAMPLERATE` varchar(100) NOT NULL DEFAULT '',
  `DESC` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STT_PROXY_INFO`
--

LOCK TABLES `STT_PROXY_INFO` WRITE;
/*!40000 ALTER TABLE `STT_PROXY_INFO` DISABLE KEYS */;
INSERT INTO `STT_PROXY_INFO` VALUES
       ('02',0,'10.122.64.152:9802','cnn-kor-8000','','','CNN STT baseline'),
       ('05',0,'10.122.64.152:9801','voicebot_poc_dnn-kor-8000','kor','8000','ETRI DNN 모델'),
       ('09',0,'10.122.64.152:9801','hcard','kor','8000','ETRI DNN 모델'),
       ('13',0,'10.122.64.152:9801','HW_1113-kor-8000','kor','8000','ETRI DNN'),
       ('14',2,'172.17.0.2:15003','cnn9806','','','CNN baseline'),
       ('17',2,'10.122.64.152:9802','cnn152','','','CNN STT baseline'),
       ('base_unilstm',0,'10.122.64.152:9801','base_unilstm','kor','8000','ETRI DNN 모델'),
       ('car',0,'10.122.64.152:9801','car','kor','8000','ETRI DNN 모델'),
       ('default',0,'10.122.64.152:9801','voicebot','kor','8000','ETRI DNN 모델'),
       ('maum_intro_v2',0,'10.122.64.152:9801','maum_intro_v2','kor','8000','ETRI DNN 모델'),
       ('ulstm_eng',0,'182.162.19.10:9801','base_unilstm','eng','8000','English baseline'),
       ('voicebot',0,'10.122.64.152:9801','voicebot','kor','8000','ETRI DNN 모델');
/*!40000 ALTER TABLE `STT_PROXY_INFO` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-15 16:46:16
