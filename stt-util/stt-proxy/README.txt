개요

  stt-proxy 프로세스는 기존 brain-stt와 cnn-stt의 grpc proto파일이
  다르므로 brain-stt와 연동하던 기존 프로그램이 brain-stt 인터페이스를
  수정하지 않고 cnn-stt로 연동할 수 있도록 메시지를 중개해 주는
  프로세스입니다.

  또한 여러개의 STT 서버 주소 및 모델 정보를 database에 저장하고
  있으므로 stt client는 stt proxy의 주소만 알고 있다면 별도로 real
  server의 주소를 알고 있을 필요가 없는 장점이 있습니다.

  table에는 stt client가 보내는 모델명 (table에 ID컬럼)에 맞는 실제
  stt정보가 매핑되어 있습니다.


BUILD

  $ go build


사용법

  Usage of stt-proxy:
    -cfg string
         Proxy config file (default "stt-proxy.cfg")
    -port int
         The server port (default 9801)


테이블 샘플

  ID                              PROTO       ADDR                MODEL         LANG        SAMPLERATE  DESC            
  ------------------------------  ----------  ------------------  ------------  ----------  ----------  ----------------
  02                              0           10.122.64.152:9802  cnn-kor-8000                          CNN STT baseline
  05                              0           10.122.64.152:9801  voicebot_poc  kor         8000        ETRI DNN 모델 
  09                              0           10.122.64.152:9801  hcard         kor         8000        ETRI DNN 모델 
  13                              0           10.122.64.152:9801  HW_1113-kor-  kor         8000        ETRI DNN        
  14                              2           172.17.0.2:15003    cnn9806                               CNN baseline    
  17                              2           10.122.64.152:9802  cnn152                                CNN STT baseline
  base_unilstm                    0           10.122.64.152:9801  base_unilstm  kor         8000        ETRI DNN 모델 
  car                             0           10.122.64.152:9801  car           kor         8000        ETRI DNN 모델 
  default                         0           10.122.64.152:9801  voicebot      kor         8000        ETRI DNN 모델 
  maum_intro_v2                   0           10.122.64.152:9801  maum_intro_v  kor         8000        ETRI DNN 모델 
  ulstm_eng                       0           182.162.19.10:9801  base_unilstm  eng         8000        English baseline
  voicebot                        0           10.122.64.152:9801  voicebot      kor         8000        ETRI DNN 모델


메타 데이터 세팅

  stt client는 기존에 model, samplerate, lang 값을 설정해서 보냈지만
  stt-proxy를 이용하는 경우에 기존 model값에 위 테이블의 ID에 해당하는
  값만 넘겨주시면 됩니다.

