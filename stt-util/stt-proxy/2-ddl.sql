



create table if not exists AUTO_CALL_CD_HISTORY
(
	ID int auto_increment
		primary key,
	CD_NAME varchar(300) null,
	CD_CONTENTS varchar(500) null,
	START_DTM date null,
	END_DTM date null,
	CREATOR varchar(100) null,
	CREATE_DTM datetime null,
	WEEK_DAY varchar(20) null,
	DISPATCH_TIME time null,
	CALL_TIME datetime null,
	CD_DESC varchar(300) null,
	AUTO_CALL_CD_CUSTOM_IDS varchar(150) null
);

create table if not exists AUTO_CALL_CONDITION
(
	ID int auto_increment
		primary key,
	START_DTM date null,
	END_DTM date null,
	DISPATCH_TIME time null,
	OB_CALL_STATUS varchar(10) default 'AC0001' null,
	CALL_TRY_ACCOUNT int null,
	CREATE_DTM datetime default CURRENT_TIMESTAMP null,
	CREATOR varchar(50) null,
	CAMPAIGN_ID int null,
	PROGRESS_STATUS int null,
	ACTIVE_STATUS smallint null,
	USE_YN varchar(2) default 'Y' null,
	WEEK_DAY varchar(20) null,
	CD_DESC varchar(300) null,
	CD_NAME varchar(100) null,
	UPDATER varchar(50) null,
	UPDATE_DTM datetime null
);

create table if not exists AUTO_CALL_CONDITION_CUSTOM
(
	ID int auto_increment
		primary key,
	CUST_DATA_CLASS_ID int null,
	AUTO_CALL_CONDITION_ID int null,
	DATA_VALUE varchar(100) null,
	USE_YN varchar(1) null
);

create index AUTO_CALL_CONDITION_CUSTOM_AUTO_CALL_CONDITION_ID_fk
	on AUTO_CALL_CONDITION_CUSTOM (AUTO_CALL_CONDITION_ID);

create index AUTO_CALL_CONDITION_CUSTOM_CUST_DATA_CLASS_ID_fk
	on AUTO_CALL_CONDITION_CUSTOM (CUST_DATA_CLASS_ID);

create table if not exists AUTO_CALL_HISTORY
(
	ID int auto_increment
		primary key,
	CD_HISTORY_ID int null,
	CONTRACT_NO int null,
	CAMPAIGN_ID int null
);

create table if not exists AUTO_CALL_LIST
(
	ID int auto_increment
		primary key,
	CONTRACT_NO int null,
	CUST_INFO_ID int null,
	OB_CALL_STATUS int null,
	PROGRESS_STATUS int null,
	CALL_STATUS int null,
	CALL_STATUS_ETC varchar(100) null,
	AUTO_CALL_CONDITION_ID int null,
	CHECK_YN varchar(1) null
);

create index AUTO_CALL_LIST_AUTO_CALL_CONDITION_ID_fk
	on AUTO_CALL_LIST (AUTO_CALL_CONDITION_ID);

create index AUTO_CALL_LIST_CUST_INFO_ID_CUST_INFO_ID_fk
	on AUTO_CALL_LIST (CUST_INFO_ID);

create table if not exists CALL_HISTORY
(
	CALL_ID int(11) unsigned auto_increment comment '콜 메타 아이디
(stt_result_detail_tb에서 call_id 값)'
		primary key,
	CALL_DATE datetime null comment '콜 생성일',
	CALL_TYPE_CODE varchar(6) null comment '콜 타입
.

common_cd.first_cd = ''06
''
CT0001: Inbound Call
CT0002: Outbound Call',
	CONTRACT_NO int(11) unsigned null comment '고객code 값,
campaign_target_list_tb 테이블 참고',
	START_TIME datetime null comment '콜 시작 시간',
	END_TIME datetime null comment '콜 종료시간',
	DURATION float null comment '콜 통화 시간',
	CUST_OP_ID varchar(50) null,
	SIP_USER varchar(20) null,
	CALL_STATUS char(6) default 'CS0001' null comment '상태(콜백예약)
common_cd.first_cd = ''02''
CS0001 : 미실행
CS0002 : 진행중
CS0003 : 중지
CS0004 : 미응답
CS0005 : 완료
CS0006 : 연결중
CS0007 : 콜백
CS0008 : 콜대기중',
	DIAL_RESULT char(6) null comment 'RC0001: 연결성공, RC0002: 통화중, RC0003: 결번, RC0004: 수신거부',
	CAMP_STATUS varchar(45) null,
	CALL_MEMO varchar(255) null comment '콜메모',
	MONITOR_CONT varchar(255) null comment '모니터내용',
	CALLBACK_STATUS char(6) null,
	CALLBACK_DT datetime null comment '콜백요청일',
	MNT_STATUS varchar(45) null,
	MNT_STATUS_NAME varchar(100) null,
	TASK_ID int null,
	CREATOR_ID int null,
	UPDATER_ID int null,
	CREATED_DTM datetime default CURRENT_TIMESTAMP null,
	UPDATED_DTM datetime default CURRENT_TIMESTAMP null,
	CONSULT_TYPE1_DEPTH1 varchar(50) null comment '상담유형1 대분류',
	CONSULT_TYPE1_DEPTH2 varchar(50) null comment '상담유형1 중분류',
	CONSULT_TYPE1_DEPTH3 varchar(50) null comment '상담유형1 소분류',
	CONSULT_TYPE2_DEPTH1 varchar(50) null comment '상담유형2 대분류',
	CONSULT_TYPE2_DEPTH2 varchar(50) null comment '상담유형2 중분류',
	CONSULT_TYPE2_DEPTH3 varchar(50) null comment '상담유형2 소분류',
	CONSULT_TYPE3_DEPTH1 varchar(50) null comment '상담유형3 대분류',
	CONSULT_TYPE3_DEPTH2 varchar(50) null comment '상담유형3 중분류',
	CONSULT_TYPE3_DEPTH3 varchar(50) null comment '상담유형3 소분류',
	TA_STATUS char null,
	SIP_CALL_ID varchar(100) null
)
comment '콜 정보';

create index CNTRCT_IDX
	on CALL_HISTORY (CONTRACT_NO, CALL_ID);

create table if not exists CM_CAMPAIGN
(
	CAMPAIGN_ID int(11) unsigned auto_increment comment 'campaign seq'
		primary key,
	CAMPAIGN_NM varchar(50) null comment 'campaign 이름',
	DESCRIPTION varchar(255) null comment 'campaign 설명',
	MNT_CD varchar(10) null comment '모니터링 코드',
	CHATBOT_NAME varchar(50) not null,
	STT_MODEL varchar(50) default 'baseline-kor-8000' null,
	STT_AGENT_MODEL varchar(50) default 'baseline-kor-16000' null,
	TTS_TEMPO float default 1 null,
	START_DT date null comment '시작일',
	END_DT date null comment '종료일',
	LIMIT_DATE int default 5 null,
	LIMIT_CALL_COUNT int default 30 null,
	USE_YN char default 'Y' null,
	CREATOR_ID int null comment '등록자ID, 데이터 등록 발생시 등록자 ID',
	UPDATER_ID int null comment '수정자ID, 데이터 등록 수정시 수정자 ID',
	CREATED_DTM datetime default CURRENT_TIMESTAMP null comment '등록일시. 데이터 등록 발생 일시',
	UPDATED_DTM datetime default CURRENT_TIMESTAMP null comment '수정일시. 데이터 수정 발생 일시',
	STT_MODEL_ID int null,
	STT_AGENT_MODEL_ID int null,
	LANG varchar(10) default 'KOR' null,
	HMD_MODEL varchar(100) null,
	IS_INBOUND char null
)
comment '캠페인 관리';

create table if not exists CM_CAMPAIGN_INFO
(
	CAMPAIGN_INFO_ID int(11) unsigned auto_increment
		primary key,
	SEQ int not null,
	CAMPAIGN_ID int not null comment 'campaign seq',
	CATEGORY varchar(45) null comment '대분류',
	TASK varchar(45) null,
	TASK_TYPE varchar(15) null,
	TASK_ANSWER varchar(45) null,
	TASK_INFO varchar(45) null comment 'C(choose) : 양자 선택
V(value) : 값 입력
N(nothing) : 받지 않음',
	CREATOR_ID int null,
	UPDATER_ID int null,
	CREATED_DTM datetime default CURRENT_TIMESTAMP null,
	UPDATED_DTM datetime default CURRENT_TIMESTAMP null
)
comment '시나리오 정보';

create index SEQ_IDX
	on CM_CAMPAIGN_INFO (CAMPAIGN_ID, SEQ);

create table if not exists CM_CAMPAIGN_SCORE
(
	SEQ_NUM int auto_increment
		primary key,
	CALL_ID int(11) unsigned null comment '콜ID',
	CONTRACT_NO int null comment '고객 code 값',
	CAMPAIGN_ID int null,
	INFO_SEQ int null,
	INFO_TASK varchar(45) null,
	TASK_VALUE varchar(30) null,
	REVIEW_COMMENT text null comment '상세보기 리뷰 data',
	CREATOR_ID int null,
	UPDATER_ID int null,
	CREATED_DTM datetime default CURRENT_TIMESTAMP null,
	UPDATED_DTM datetime default CURRENT_TIMESTAMP null
)
comment '시나리오 탐지 결과';

create table if not exists CM_CAMPAIGN_SERVICE
(
	SERVICE_NM varchar(50) not null,
	CAMPAIGN_ID int not null,
	CAMPAIGN_NM varchar(45) null,
	primary key (SERVICE_NM, CAMPAIGN_ID)
);

create table if not exists CM_COMMON_CD
(
	FIRST_CD varchar(10) not null comment '01 : 모니터링 종류
02 : 통화상태 코드
03 : 상담원 부서코드
04 : 상담원 직급코드
05 : 상담원 상태',
	SECOND_CD varchar(10) null comment '두번째 코드 분류 값',
	THIRD_CD varchar(10) null comment '세번째 코드 분류 값',
	CODE varchar(10) default '' not null comment '코드. 코드 설명과 매칭되는 코드 값',
	CD_DESC varchar(255) null comment '코드 설명. 코드 설명',
	CD_DESC_ENG varchar(255) null comment '코드 설명 - 영어',
	NOTE varchar(255) null comment '코드 부연 or 상세 설명',
	CREATOR_ID int null comment '등록자ID, 데이터 등록 발생시 등록자 ID',
	UPDATER_ID int null comment '수정자ID, 데이터 등록 수정시 수정자 ID',
	CREATED_DTM datetime default CURRENT_TIMESTAMP null comment '등록일시. 데이터 등록 발생 일시',
	UPDATED_DTM datetime default CURRENT_TIMESTAMP null comment '수정일시. 데이터 수정 발생 일시',
	CAMPAIGN_ID varchar(10) null comment '캠패인아이디& 99 공통',
	primary key (FIRST_CD, CODE)
)
comment '공통코드';

create index CD_IDX
	on CM_COMMON_CD (CODE);

create table if not exists CM_COMPANY
(
	COMPANY_ID varchar(50) not null comment '회사ID'
		primary key,
	COMPANY_NAME varchar(100) not null comment '회사명(국문)',
	COMPANY_NAME_EN varchar(100) null comment '회사명(영문)',
	JURIRNO1 varchar(6) null comment '법인등록번호1',
	JURIRNO2 varchar(7) null comment '법인등록번호2',
	BIZRNO1 varchar(3) null comment '사업자등록번호1',
	BIZRNO2 varchar(2) null comment '사업자등록번호2',
	BIZRNO3 varchar(5) null comment '사업자등록번호3',
	MOBLPHON_NO1 varchar(4) null comment '휴대전화번호1',
	MOBLPHON_NO2 varchar(4) null comment '휴대전화번호2',
	MOBLPHON_NO3 varchar(4) null comment '휴대전화번호3',
	FXNUM1 varchar(4) null comment '팩스번호1',
	FXNUM2 varchar(4) null comment '팩스번호2',
	FXNUM3 varchar(4) null comment '팩스번호3',
	RPRSNTV_NM varchar(30) null comment '대표자명',
	REGISTER_ID varchar(30) null comment '등록자ID',
	REGIST_DT datetime not null comment '등록일시',
	UPDUSR_ID varchar(30) null comment '수정자ID',
	UPDT_DT datetime null comment '수정일시',
	DELETE_AT char default 'N' null comment '삭제여부',
	DELETE_DT datetime null comment '삭제일시',
	BASS_ADRES varchar(100) null comment '기본주소',
	DETAIL_ADRES varchar(100) null comment '상세주소'
)
comment '회사테이블';

create table if not exists CM_COMPANY_CAMPAIGNS
(
	COMPANY_ID varchar(8) not null,
	CAMPAIGN_ID varchar(8) not null,
	primary key (COMPANY_ID, CAMPAIGN_ID)
);

create table if not exists CM_CONTRACT
(
	CONTRACT_NO int(11) unsigned auto_increment comment '고객 code 값'
		primary key,
	CAMPAIGN_ID int null comment '캠페인ID',
	CUST_ID int null comment '가입자 고유 아이디',
	TEL_NO varchar(20) null comment '전화번호',
	CUST_OP_ID varchar(50) null comment '상담원ID',
	PROD_ID int null,
	CALL_TRY_COUNT int default 0 null comment '통화횟수',
	LAST_CALL_ID int null comment '통화상태 코드
(상태(콜백예약))

common_cd.first_cd = ''02''
CS0001 : 미실행
CS0002 : 진행중
CS0003 : 중지
CS0004 : 미응답
CS0005 : 완료
CS0006 : 연결중
CS0007 : 콜백
CS0008 : 콜대기중',
	IS_INBOUND char default 'N' null,
	TASK_SEQ int null comment 'TASK 진행 상황을 저장',
	CREATOR_ID int null comment '등록자ID. 데이터 등록 발생시 등록자 ID',
	UPDATER_ID int null comment '수정자ID. 데이터 등록 수정시 수정자 ID',
	CREATED_DTM datetime default CURRENT_TIMESTAMP null comment '등록일시. 데이터 등록 발생 일시',
	UPDATED_DTM datetime default CURRENT_TIMESTAMP null comment '수정일시. 데이터 수정 발생 일시',
	USE_YN char(2) default 'Y' null comment '사용:''Y'', 미사용:''N'''
)
comment '모니터링 대상관리';

create index CM_CONTRACT_CM_OP_INFO_CUST_OP_ID_fk
	on CM_CONTRACT (CUST_OP_ID);

create index CUST_ID_IDX
	on CM_CONTRACT (CUST_ID);

create index LASTCALL_IDX
	on CM_CONTRACT (LAST_CALL_ID);

create index MNT_TARGET_MNG_cust_tel_no_IDX
	on CM_CONTRACT (CAMPAIGN_ID, IS_INBOUND, TEL_NO);

create table if not exists CM_MNTR_TRGT_MNGMT_OB_INFO
(
	OB_ID int auto_increment,
	CONTRACT_NO int not null,
	ASSIGNED_DT date null,
	ASSIGNED_YN char default 'N' null,
	CUST_TYPE varchar(50) null,
	TARGET_DT date null,
	TARGET_YN char null,
	CALLBACK_DT date null,
	CALLBACK_STATUS char(6) default 'CB0001' null,
	CREATOR_ID int null,
	UPDATER_ID int null,
	CREATED_DTM datetime default CURRENT_TIMESTAMP null,
	UPDATED_DTM datetime default CURRENT_TIMESTAMP null,
	constraint id_UNIQUE
		unique (OB_ID)
)
charset=latin1;

alter table CM_MNTR_TRGT_MNGMT_OB_INFO
	add primary key (OB_ID);

create table if not exists CM_OP_INFO
(
	CUST_OP_ID varchar(50) not null comment '상담원ID',
	CUST_OP_NM varchar(50) not null comment '상담원명',
	PASSWORD varchar(50) null comment '비밀번호',
	IP_ADDR varchar(50) default '127.0.0.1' null,
	COMPANY_ID varchar(8) null,
	DEPT_CD varchar(10) null comment '부서코드
common_cd.first_cd = ''03''',
	POSITION_CD varchar(10) null comment '직급코드.
common_cd.first_cd = ''04''',
	CUST_OP_STATUS char(2) default '01' null comment '상태
common_cd.first_cd = ''05''
 01:근무
 02:휴식
 03:휴가',
	USE_YN char null comment '사용여부',
	CREATOR_ID int null comment '등록자ID. 데이터 등록 발생시 등록자 ID',
	UPDATER_ID int null comment '수정자ID. 데이터 등록 수정시 수정자 ID',
	CREATED_DTM datetime default CURRENT_TIMESTAMP null comment '등록일시, 데이터 등록 발생 일시',
	UPDATED_DTM datetime default CURRENT_TIMESTAMP null comment '수정일시, 데이터 수정 발생 일시',
	constraint cust_op_id_UNIQUE
		unique (CUST_OP_ID)
)
comment '상담원 정보';

alter table CM_OP_INFO
	add primary key (CUST_OP_ID);

create table if not exists COLUMN_DATA_TYPE
(
	ID int not null
		primary key,
	TYPE_NAME varchar(50) null comment '타입 정보(ex. int, date 등)',
	TYPE_OPERATE varchar(50) null comment 'equal, contain'
)
comment '데이터 타입';

create table if not exists CUST_BASE_INFO
(
	CUST_ID int auto_increment comment '가입자 고유 아이디',
	CUST_NM varchar(50) charset utf8 default '' null comment '고객명',
	JUMIN_NO char(13) charset utf8 null comment '고객 주민번호',
	CUST_TEL_NO varchar(20) charset utf8 null comment '고객 핸드폰 번호',
	CUST_COMPANY_NO varchar(20) null comment '고객 회사 번호',
	CUST_HOME_NO varchar(20) null comment '고객 자택 번호',
	CUST_ETC_NO varchar(20) null,
	CUST_TEL_COMP varchar(3) charset utf8 null comment '고객 통신사명',
	TEL_COMP_SAVE_YN char charset utf8 null comment '알뜰폰 여부',
	CERTIFI_NO char(6) charset utf8 null comment '본인 인증 위한 인증번호 ',
	CREATE_DT datetime default CURRENT_TIMESTAMP null comment '등록일시',
	MODIFY_DT datetime default CURRENT_TIMESTAMP null comment '수정일시',
	CUST_ADDRESS varchar(50) charset utf8 null comment '고객 주소
ex) 건물이름까지',
	CUST_DETAIL_ADDRESS varchar(50) charset utf8 null comment '고객 주소 동호수',
	CONSULT_CHAT_ID varchar(50) null comment '채팅 상담시 발생되는 Unique ID',
	CUST_TYPE int null comment '0: 개인, 1: 법인',
	CUST_STATE int null comment '0: 활성상태, 1: 해지상태, 2:휴면상태',
	CUST_SUBSC_PLAN varchar(50) null comment '구독플랜',
	CUST_API_ID varchar(50) null comment 'API 계정 ID',
	CUST_API_KEY varchar(50) null comment 'API 계정 KEY',
	CUST_REG_DATE datetime null comment '회원 가입일',
	CUST_REG_PATH varchar(50) null comment '가입 경로',
	CUST_TERM_DATE datetime null comment '회원 해지일',
	CUST_ADDRESS2 varchar(50) null comment '고객 주소2 (회사 주소 등) ex) 건물이름까지',
	CUST_DETAIL_ADDRESS2 varchar(50) null comment '고객 주소2 동호수',
	CUST_EMAIL varchar(50) null comment '고객 이메일',
	constraint CUST_BASE_INFO_CUST_ID_uindex
		unique (CUST_ID)
)
comment 'IB 고객 정보 테이블' collate=utf8_bin;

alter table CUST_BASE_INFO
	add primary key (CUST_ID);

create table if not exists CUST_CATEGORY_CD
(
	CODE varchar(50) not null comment '코드',
	CODE_NM varchar(50) not null comment '코드명',
	CODE_NM_ENG varchar(100) null,
	UPCODE varchar(50) null comment '상위코드',
	SORT int null comment '정렬순서',
	CAMPAIGN_ID varchar(50) not null comment '켐페인',
	USE_YN varchar(50) default 'Y' not null comment '사용여부',
	primary key (CODE, CAMPAIGN_ID)
)
comment '카테고리 코드 테이블';

create table if not exists CUST_DATA_CLASS
(
	CUST_DATA_CLASS_ID int auto_increment comment '아이디'
		primary key,
	CAMPAIGN_ID int null comment '캠페인 ID',
	DISPLAY_YN char null comment '노출여부',
	OB_CALL_STATUS char null comment '발송대상',
	DATA_TYPE varchar(30) null comment '데이터 타입',
	COLUMN_KOR varchar(50) null comment '컬럼명(한)',
	COLUMN_ENG varchar(50) null comment '컬럼명(영)',
	CASE_TYPE varchar(50) null comment '후보',
	DESCRIPTION varchar(100) null comment '설명',
	USE_YN char(4) default 'Y' null
)
comment '고객 정보 조건';

create table if not exists CUST_INFO
(
	CUST_ID int auto_increment comment '아이디'
		primary key,
	CAMPAIGN_ID int not null comment '캠페인 ID',
	CUST_NM varchar(45) not null comment '고객 이름',
	CUST_TEL_NO varchar(20) not null comment '고객 전화번호',
	CUST_DATA text null comment '고객 조건데이터'
)
comment '고객 데이터';

create table if not exists OB_CALL_QUEUE
(
	OB_CALL_QUEUE_ID int auto_increment
		primary key,
	CONTRACT_NO int not null,
	CAMPAIGN_ID int not null,
	USER_ID varchar(40) null
)
charset=latin1;

create table if not exists RECALL_HISTORY
(
	CALL_ID int auto_increment comment '재통화 고유 아이디'
		primary key,
	CONTRACT_NO int(11) unsigned not null comment '컨트랙트 ID',
	RECALL_TEL_NO varchar(20) null comment '재통화 연락처',
	RECALL_DATE datetime null comment '예약 일시',
	CREATOR_ID varchar(50) null comment '등록자ID. 데이터 등록 발생시 등록자 ID',
	UPDATER_ID varchar(50) null comment '수정자ID. 데이터 등록 수정시 수정자 ID',
	CREATED_DTM datetime null comment '등록일시. 데이터 등록 발생 일시',
	UPDATED_DTM datetime null comment '수정일시. 데이터 수정 발생 일시',
	constraint RECALL_HISTORY_CM_CONTRACT_CONTRACT_NO_fk
		foreign key (CONTRACT_NO) references CM_CONTRACT (CONTRACT_NO)
)
comment '재통화 테이블';

create table if not exists SIP_ACCOUNT
(
	SIP_DOMAIN varchar(64) not null,
	SIP_DOMAIN2 varchar(64) null comment 'secondary PBX',
	SIP_USER char(20) not null,
	PASSWD char(20) null,
	CLIENT_PORT int null,
	TEL_URI char(20) null,
	PBX_NAME varchar(45) null,
	CUST_OP_ID varchar(50) null,
	STATUS char(20) null comment 'online, offline',
	CONTRACT_NO int null,
	CUSTOMER_PHONE char(20) null,
	BOOT_TIME datetime null,
	LAST_EVENT char(20) null,
	LAST_EVENT_TIME datetime null,
	CAMPAIGN_ID varchar(100) null,
	IS_INBOUND char default 'N' not null,
	DIRECT_CALL char default 'N' null,
	OB_PREFIX varchar(10) default '' not null,
	CREATOR_ID int null,
	UPDATER_ID varchar(50) null,
	CREATED_DTM timestamp default CURRENT_TIMESTAMP not null,
	UPDATED_DTM timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
	primary key (SIP_DOMAIN, SIP_USER),
	constraint SIP_ACCOUNT_UN
		unique (CLIENT_PORT)
);

create table if not exists STAT_CALL_HISTORY_OP
(
	STAT_CALL_HIST_OP_ID int auto_increment comment '고유 아이디'
		primary key,
	CUST_OP_ID varchar(50) null comment '상담원ID',
	CUST_OP_NM varchar(50) null comment '상담원명',
	IN_CALL_COUNT int null comment '인바운드 콜 카운트',
	IN_CALL_TIME int null comment '인바운드 콜 타임',
	OUT_CALL_COUNT int null comment '아웃바운드 콜 카운트',
	OUt_CALL_TIME int null comment '아웃바운드 콜 타임',
	DAYS int null comment '요일 (1 ~ 7) 1:SUN, 2:MON, 3:TUES, 4:WED, 5:THURS, 6:FRI, 7:SAT',
	START_DT datetime null comment '집계 시작 시각',
	END_DT datetime null comment '집계 종료 시각'
)
comment 'STAT_CALL_HIST_OP_ID';

create index STAT_CALL_HISTORY_OP_CM_OP_INFO_CUST_OP_ID_fk
	on STAT_CALL_HISTORY_OP (CUST_OP_ID);

create table if not exists STAT_CALL_HISTORY_TOTAL
(
	STAT_CALL_HIST_ID int auto_increment comment '고유 아이디'
		primary key,
	CHANNEL_TYPE int null comment '채널 타입 (0: Call, 1:Chat)',
	INCOMING_COUNT int null comment '총 인입 수',
	BOT_COUNT int null comment 'BOT 응대 수',
	INTERVENT_COUNT int null comment '개입 응대 수',
	OP_COUNT int null comment '휴먼응대 수',
	GIVE_UP_COUNT int null comment '포기 호',
	FOLLOW_UP_CALL_COUNT int null comment 'Follow up call 수',
	AVG_CALL_TIME int null comment '평균통화시간',
	AVG_WAIT_TIME int null comment '평균대기시간',
	AVG_GIVE_UP_TIME int null comment '평균포기시간',
	DAYS int null comment '요일 (1 ~ 7) 1:SUN, 2:MON, 3:TUES, 4:WED, 5:THURS, 6:FRI, 7:SAT',
	START_DT datetime null comment '집계 시작 시각',
	END_DT datetime null comment '집계 종료 시각'
)
comment '콜 통계 종합 테이블';

create table if not exists STT_PROXY_INFO
(
	ID varchar(100) not null
		primary key,
	PROTO int default 0 not null comment 'DNN:0, LSTM:1, CNN:2',
	ADDR varchar(100) not null,
	MODEL varchar(100) default '' not null,
	LANG varchar(100) default '' not null,
	SAMPLERATE varchar(100) default '' not null,
	`DESC` varchar(100) null
);

create table if not exists STT_SERVER_INFO
(
	ID int auto_increment
		primary key,
	PROTO int default 0 not null comment 'DNN:0, LSTM:1, CNN:2',
	ADDR varchar(100) not null,
	MODEL varchar(100) not null,
	DIRECTORY varchar(100) not null,
	`DESC` varchar(100) null,
	constraint STT_SERVER_INFO_UN
		unique (MODEL)
);

create table if not exists TN_COMPANY
(
	COMPANY_ID varchar(50) not null comment '회사ID'
		primary key,
	COMPANY_NAME varchar(8) not null comment '회사명(국문)',
	COMPANY_NAME_EN varchar(8) null comment '회사명(영문)',
	JURIRNO1 varchar(6) null comment '법인등록번호1',
	JURIRNO2 varchar(7) null comment '법인등록번호2',
	BIZRNO1 varchar(3) null comment '사업자등록번호1',
	BIZRNO2 varchar(2) null comment '사업자등록번호2',
	BIZRNO3 varchar(5) null comment '사업자등록번호3',
	MOBLPHON_NO1 varchar(4) null comment '휴대전화번호1',
	MOBLPHON_NO2 varchar(4) null comment '휴대전화번호2',
	MOBLPHON_NO3 varchar(4) null comment '휴대전화번호3',
	FXNUM1 varchar(4) null comment '팩스번호1',
	FXNUM2 varchar(4) null comment '팩스번호2',
	FXNUM3 varchar(4) null comment '팩스번호3',
	RPRSNTV_NM varchar(30) null comment '대표자명',
	REGISTER_ID varchar(30) null comment '등록자ID',
	REGIST_DT datetime not null comment '등록일시',
	UPDUSR_ID varchar(30) null comment '수정자ID',
	UPDT_DT datetime null comment '수정일시',
	DELETE_AT char default 'N' null comment '삭제여부',
	DELETE_DT datetime null comment '삭제일시',
	BASS_ADRES varchar(100) null comment '기본주소',
	DETAIL_ADRES varchar(100) null comment '상세주소'
)
comment '회사테이블';

create table if not exists TN_COMPANY_MENU_GROUP
(
	COMPANY_GROUP_ID int not null comment '회사그룹아이디'
		primary key,
	COMPANY_ID varchar(50) not null comment '회사ID',
	GOUP_NM varchar(100) not null comment '메뉴명',
	GROUP_EXP varchar(500) null comment '제목',
	SORT_ORDR int not null comment '정렬순서',
	REGISTER_ID varchar(30) null comment '등록자ID',
	REGIST_DT datetime not null comment '등록일시',
	UPDUSR_ID varchar(30) null comment '수정자ID',
	UPDT_DT datetime null comment '수정일시',
	DELETE_AT char default 'N' null comment '삭제여부',
	DELETE_DT datetime null comment '삭제일시'
)
comment '사용자메뉴
';

create table if not exists TN_COMPANY_MENU_GROUP_MENU
(
	COMPANY_GROUP_ID int not null comment '회사그룹아이디',
	MENU_CODE varchar(50) not null comment '메뉴코드',
	SORT_ORDR int not null comment '정렬순서',
	REGISTER_ID varchar(30) null comment '등록자ID',
	REGIST_DT datetime not null comment '등록일시',
	UPDUSR_ID varchar(30) null comment '수정자ID',
	UPDT_DT datetime null comment '수정일시',
	DELETE_AT char default 'N' null comment '삭제여부',
	DELETE_DT datetime null comment '삭제일시',
	constraint FK_TN_COMPANY_MENU_GROUP_TO_TN_COMPANY_MENU_GROUP_MENU
		foreign key (COMPANY_GROUP_ID) references TN_COMPANY_MENU_GROUP (COMPANY_GROUP_ID)
)
comment '사용자메뉴
';

create index FK_TN_COMPANY_MENU_TO_TN_COMPANY_MENU_GROUP_MENU
	on TN_COMPANY_MENU_GROUP_MENU (MENU_CODE);

create table if not exists TN_MENU
(
	MENU_CODE varchar(50) not null comment '사용자메뉴코드'
		primary key,
	MENU_NM_KO varchar(100) not null comment '메뉴명',
	MENU_NM_EN varchar(500) not null comment '제목',
	BASS_COURS varchar(200) null comment '기본경로',
	MENU_COURS varchar(200) null comment '메뉴경로',
	TOP_MENU_CODE varchar(50) null comment '최상위메뉴코드',
	PARNTS_MENU_CODE varchar(50) null comment '부모메뉴코드',
	MENU_DP int not null comment '메뉴깊이',
	SORT_ORDR int not null comment '정렬순서',
	MNGR_MENU_URL varchar(200) null comment '관리자메뉴URL',
	USER_MENU_URL varchar(200) null comment '사용자메뉴URL',
	MENU_TY varchar(20) null comment '메뉴유형',
	LINK_TY varchar(20) null comment '링크유형
00 : 없음
01 : 페이지변경
02 : 팝업',
	CNTNTS_TY varchar(20) null comment '컨텐츠유형
00 : 없음
01 : URI
02 : IFRAME',
	MENU_ON_IMAGE varchar(200) null comment '메뉴온이미지',
	MENU_OFF_IMAGE varchar(200) null comment '메뉴오프이미지',
	REGISTER_ID varchar(30) null comment '등록자ID',
	REGIST_DT datetime default CURRENT_TIMESTAMP not null comment '등록일시',
	UPDUSR_ID varchar(30) null comment '수정자ID',
	UPDT_DT datetime null comment '수정일시',
	DELETE_AT char default 'N' null comment '삭제여부',
	DELETE_DT datetime null comment '삭제일시',
	USE_AT char default 'Y' null
)
comment '사용자메뉴
';

create table if not exists TN_COMPANY_MENU
(
	MENU_CODE varchar(50) not null comment '메뉴코드',
	COMPANY_ID varchar(50) not null comment '회사ID',
	SORT_ORDR int not null comment '정렬순서',
	REGISTER_ID varchar(30) null comment '등록자ID',
	REGIST_DT datetime not null comment '등록일시',
	UPDUSR_ID varchar(30) null comment '수정자ID',
	UPDT_DT datetime null comment '수정일시',
	DELETE_AT char null comment '삭제여부',
	DELETE_DT datetime null comment '삭제일시',
	constraint FK_TN_MENU_TO_TN_COMPANY_MENU
		foreign key (MENU_CODE) references TN_MENU (MENU_CODE)
)
comment '사용자메뉴
';

create index FK_CM_COMPANY_TO_TN_COMPANY_MENU
	on TN_COMPANY_MENU (COMPANY_ID);

create table if not exists TN_USER
(
	USER_ID varchar(50) not null comment '유저아이디'
		primary key,
	COMPANY_ID varchar(50) not null comment '회사ID',
	USER_NO int auto_increment comment '유저아이디(API에 사용)',
	USER_AUTH_TY char not null comment '사용자유형',
	CI_NFO varchar(88) null comment 'CI정보',
	DUP_INFO varchar(64) null comment 'DUP정보',
	SBSCRB_TY char not null comment '가입유형',
	USER_PW varchar(500) not null comment '비밀번호',
	USER_NM varchar(50) null comment '이름',
	BRTHDY varchar(10) null comment '생년월일',
	EMAIL varchar(100) null comment '이메일1',
	LOGIN_FAIL_CNT int default 0 not null comment '로그인실패횟수',
	EXPIRED_YN char default 'Y' null comment 'isAccountNonExpired',
	LOCK_YN char default 'Y' null comment 'isAccountNonLocked',
	CRT_EXPRIED_YN char default 'Y' null comment 'isCredentialsNonExpired',
	ENABLED_YN char default 'Y' null comment 'isEnabled',
	SSO_YN varchar(2) default 'N' null comment 'SSOYN',
	POSITION_CD varchar(10) null comment '직급코드.
	common_cd.first_cd = ''04''',
	CUST_OP_STATUS char(2) default '01' null comment '상태
	common_cd.first_cd = ''05''
	 01:근무
	 02:휴식
	 03:휴가',
	DEPT_CD varchar(10) null comment '부서코드
	common_cd.first_cd = ''03''',
	MOBLPHON_NO1 varchar(4) null comment '휴대전화번호1',
	MOBLPHON_NO2 varchar(4) null comment '휴대전화번호2',
	MOBLPHON_NO3 varchar(4) null comment '휴대전화번호3',
	ZIP varchar(3) null comment '우편번호1',
	BASS_ADRES varchar(100) null comment '기본주소',
	DETAIL_ADRES varchar(100) null comment '상세주소',
	RECENT_CONECT_DT datetime null comment '최근접속일시',
	PASSWORD_CHANGE_DE datetime null comment '비밀번호변경일',
	SEXDSTN int null comment '성별(0:남/1:여)',
	SESSION_ID varchar(1000) null comment '로그인세션ID',
	REGISTER_ID varchar(30) null comment '등록자ID',
	REGIST_DT datetime not null comment '등록일시',
	UPDUSR_ID varchar(30) null comment '수정자ID',
	UPDT_DT datetime null comment '수정일시',
	DELETE_AT char default 'N' null comment '삭제여부',
	DELETE_DT datetime null comment '삭제일시',
	USE_AT char default 'Y' null comment '삭제여부',
	constraint UIX_TN_USER
		unique (USER_NO)
)
comment '회원_마스터3';

create table if not exists TN_COMPANY_MENU_GROUP_USER
(
	COMPANY_GROUP_ID int not null comment '회사그룹아이디',
	USER_ID varchar(50) not null comment '유저아이디',
	SORT_ORDR int not null comment '정렬순서',
	REGISTER_ID varchar(30) null comment '등록자ID',
	REGIST_DT datetime not null comment '등록일시',
	UPDUSR_ID varchar(30) null comment '수정자ID',
	UPDT_DT datetime null comment '수정일시',
	DELETE_AT char default 'N' null comment '삭제여부',
	DELETE_DT date null comment '삭제일시',
	constraint FK_TN_COMPANY_MENU_GROUP_TO_TN_COMPANY_MENU_GROUP_USER
		foreign key (COMPANY_GROUP_ID) references TN_COMPANY_MENU_GROUP (COMPANY_GROUP_ID),
	constraint FK_TN_USER_TO_TN_COMPANY_MENU_GROUP_USER
		foreign key (USER_ID) references TN_USER (USER_ID)
)
comment '사용자메뉴
';

create table if not exists CM_STT_RSLT_DETAIL
(
	STT_RESULT_DETAIL_ID int auto_increment comment '세부 STT 결과 아이디',
	STT_RESULT_ID int null comment 'STT 결과 아이디(추후 삭제 예정)',
	CALL_ID int(11) unsigned not null comment 'call 아이디',
	SPEAKER_CODE char(6) null comment '화자
ST0001 : Client
ST0002 : Agent
ST0003 : Mono',
	SENTENCE_ID int null comment '문장 번호',
	SENTENCE text null comment '인식 결과',
	START_TIME decimal(10,2) null comment '시작 시간',
	END_TIME decimal(10,2) null comment '종료 시간',
	SPEED float null comment '스피드',
	SILENCE_YN char null comment '묵음 여부',
	IGNORED char default 'N' null,
	CREATOR_ID int null comment '생성자 아이디',
	UPDATER_ID int null comment '수정자 아이디',
	UPDATED_DTM datetime default CURRENT_TIMESTAMP null comment '수정일시',
	CREATED_DTM datetime default CURRENT_TIMESTAMP null comment '생성일시',
	primary key (STT_RESULT_DETAIL_ID, CALL_ID)
)
comment 'STT 결과';


