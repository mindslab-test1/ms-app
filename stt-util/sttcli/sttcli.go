package main

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	pb "maum/brain/stt"
	pb_common "maum/common"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
)

const (
	address = "127.0.0.1:9801"
	// defaultFname = "../../../pysrc/samples/8k.pcm"
)

var mutex = &sync.Mutex{}
var Fname *string
var Dname *string
var total_time time.Duration
var Lang string
var Model string
var SampleRate string
var Host string
var RealTime bool = false
var LoopCnt int
var ServiceName string
var ChannelCnt int
var Cert string
var Interim bool
var ResultSuffix string

var GitVersion string
var ShowVersion bool = false

var chan_file = make(chan string)
var done = make(chan bool)

// 인증서 유무에 따른 Connection 생성
func GetConnection() (*grpc.ClientConn, error) {
	if len(Cert) > 0 {
		creds, err := credentials.NewClientTLSFromFile(Cert, "")
		if err != nil {
			log.Fatalf("Failed - %v", err)
			return nil, err
		}
		return grpc.Dial(Host, grpc.WithTransportCredentials(creds))
	} else {
		return grpc.Dial(Host, grpc.WithInsecure())
	}
}

func test_streaming(fname string) {
	start := time.Now()
	conn, err := GetConnection()
	defer conn.Close()
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return
	}
	c := pb.NewSpeechToTextServiceClient(conn)
	md := metadata.Pairs(
		"in.model", Model,
		"in.lang", Lang,
		"in.samplerate", SampleRate,
	)
	ctx := metadata.NewOutgoingContext(context.Background(), md)
	stream, err := c.StreamingRecognize(ctx)
	if err != nil {
		fmt.Println(err)
		return
	}
	waitc := make(chan struct{})
	go func() {
		for {
			in, err := stream.Recv()
			if err == io.EOF {
				close(waitc)
				return
			}
			if err != nil {
				log.Fatalf("Fail to message: %v", err)
			}
			for _, result := range in.Results {
				log.Printf("Got message: [%v] %s", in.SpeechEventType, result.Transcript)
			}
		}
	}()

	nLang := pb_common.Lang(pb_common.LangCode_value[Lang])
	nSampleRate, _ := strconv.ParseInt(SampleRate, 10, 32)

	first := &pb.StreamingRecognizeRequest{
		StreamingRequest: &pb.StreamingRecognizeRequest_SpeechParam{
			SpeechParam: &pb.SpeechRecognitionParam{
				SampleRate: int32(nSampleRate),
				Lang:       nLang,
				Model:      Model,
			},
		},
	}
	log.Printf("Start audio streaming: %v", fname)
	if err := stream.Send(first); err != nil {
		fmt.Printf("Stream send error: %v\n", err)
	}

	f, err := os.Open(fname)
	buf := make([]byte, 320)
	for {
		n, err := f.Read(buf)
		if err != nil {
			if err == io.EOF {
				break
			} else {
				fmt.Printf("File read error: %v\n", err)
				break
			}
		}
		if n < 0 {
			fmt.Println("error")
		}

		audio := &pb.StreamingRecognizeRequest{
			StreamingRequest: &pb.StreamingRecognizeRequest_AudioContent{
				AudioContent: buf,
			},
		}

		if err := stream.Send(audio); err != nil {
			fmt.Printf("Stream send error: %v\n", err)
		}
		if RealTime {
			if SampleRate == "16000" {
				time.Sleep(time.Duration(n/32) * time.Millisecond)
			} else {
				time.Sleep(time.Duration(n/16) * time.Millisecond)
			}
		}
	}
	stream.CloseSend()
	log.Println("Streaming CloseSend()")
	<-waitc
	elapsed := time.Since(start)
	mutex.Lock()
	total_time += elapsed
	mutex.Unlock()
	log.Printf("elapsed time is %s", elapsed)
	if err != nil {
		fmt.Println(err)
	}
}

func test_stream(fname string) {
	start := time.Now()
	conn, err := GetConnection()
	defer conn.Close()
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return
	}
	c := pb.NewSpeechToTextServiceClient(conn)

	md := metadata.Pairs(
		"in.model", Model,
		"in.lang", Lang,
		"in.samplerate", SampleRate,
	)
	if Interim {
		md["in.need.interim"] = append(md["in.need.interim"], "true")
	}
	ctx := metadata.NewOutgoingContext(context.Background(), md)
	stream, err := c.StreamRecognize(ctx)
	if err != nil {
		fmt.Println(err)
		return
	}
	waitc := make(chan struct{})
	go func() {
		var f *os.File
		if ResultSuffix != "" {
			resultFile := strings.TrimSuffix(fname, ".pcm")
			resultFile = strings.TrimSuffix(resultFile, ".wav")
			resultFile += "." + ResultSuffix
			f, _ = os.Create(resultFile)
			defer f.Close()
		}

		for {
			in, err := stream.Recv()
			if err == io.EOF {
				close(waitc)
				return
			}
			if err != nil {
				log.Fatalf("Fail to message: %v", err)
			}
			log.Printf("[%.02f - %.02f] Got message: %s",
				float64(in.Start)/100.0, float64(in.End)/100.0, in.Txt)
			if ResultSuffix != "" {
				f.WriteString(in.Txt + "\n")
				f.Sync()
			}
		}
	}()

	log.Printf("Start audio streaming: %v", fname)

	f, err := os.Open(fname)
	buf := make([]byte, 320)
	for {
		n, err := f.Read(buf)
		if err != nil {
			if err == io.EOF {
				break
			} else {
				fmt.Printf("File read error: %v\n", err)
				break
			}
		}
		if n < 0 {
			fmt.Println("error")
		}
		audio := &pb.Speech{Bin: buf[:n]}
		if err := stream.Send(audio); err != nil {
			fmt.Printf("Stream send error: %v\n", err)
		}
		if RealTime {
			if SampleRate == "16000" {
				time.Sleep(time.Duration(n/32) * time.Millisecond)
			} else {
				time.Sleep(time.Duration(n/16) * time.Millisecond)
			}
		}
	}
	stream.CloseSend()
	log.Println("Stream CloseSend()")
	<-waitc
	elapsed := time.Since(start)
	mutex.Lock()
	total_time += elapsed
	mutex.Unlock()
	log.Printf("elapsed time is %s", elapsed)
	if err != nil {
		fmt.Println(err)
	}
}

func test_simple(fname string) {
	start := time.Now()

	// Set up a connection to the server.
	conn, err := GetConnection()
	defer conn.Close()
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return
	}
	c := pb.NewSpeechToTextServiceClient(conn)

	md := metadata.Pairs(
		"in.model", Model,
		"in.lang", Lang,
		"in.samplerate", SampleRate,
	)
	ctx := metadata.NewOutgoingContext(context.Background(), md)
	stream, err := c.SimpleRecognize(ctx)
	if err != nil {
		fmt.Println(err)
		return
	}

	log.Printf("Start audio streaming: %v", fname)

	f, err := os.Open(fname)
	buf := make([]byte, 320)
	for {
		n, err := f.Read(buf)
		if err != nil {
			if err == io.EOF {
				break
			} else {
				fmt.Printf("File read error: %v\n", err)
				break
			}
		}
		if n < 0 {
			fmt.Println("error")
		}

		audio := &pb.Speech{Bin: buf}
		if err := stream.Send(audio); err != nil {
			fmt.Println(err)
		}
	}
	result, err := stream.CloseAndRecv()
	elapsed := time.Since(start)
	mutex.Lock()
	total_time += elapsed
	mutex.Unlock()
	log.Printf("elapsed time is %s", elapsed)
	if err != nil {
		fmt.Println(err)
	} else {
		log.Printf("resulit is %s", result.Txt)
	}
}

func Restart() {
	conn, err := GetConnection()
	defer conn.Close()
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return
	}
	c := pb.NewSttModelResolverClient(conn)
	md := metadata.Pairs(
		"in.model", Model,
		"in.lang", Lang,
		"in.samplerate", SampleRate,
	)
	ctx := metadata.NewOutgoingContext(context.Background(), md)
	nSampleRate, _ := strconv.ParseInt(SampleRate, 10, 32)
	nLang := pb_common.LangCode(pb_common.LangCode_value[Lang])
	model := pb.Model{Model: Model, SampleRate: int32(nSampleRate), Lang: nLang}
	status, err := c.Restart(ctx, &model)
	if err != nil {
		fmt.Printf("can't restart model: %v\n", err)
		return
	} else {
		fmt.Printf("server status: %v\n", status)
	}
}

func Stop() {
	conn, err := GetConnection()
	defer conn.Close()
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return
	}
	c := pb.NewSttModelResolverClient(conn)
	md := metadata.Pairs(
		"in.model", Model,
		"in.lang", Lang,
		"in.samplerate", SampleRate,
	)
	ctx := metadata.NewOutgoingContext(context.Background(), md)
	nSampleRate, _ := strconv.ParseInt(SampleRate, 10, 32)
	nLang := pb_common.LangCode(pb_common.LangCode_value[Lang])
	model := pb.Model{Model: Model, SampleRate: int32(nSampleRate), Lang: nLang}
	status, err := c.Stop(ctx, &model)
	if err != nil {
		fmt.Printf("can't stop model: %v\n", err)
		return
	} else {
		fmt.Printf("server status: %v\n", status)
	}
}

func Ping() {
	conn, err := GetConnection()
	defer conn.Close()
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return
		// return nil
	}
	c := pb.NewSttModelResolverClient(conn)
	md := metadata.Pairs(
		"in.model", Model,
		"in.lang", Lang,
		"in.samplerate", SampleRate,
	)
	ctx := metadata.NewOutgoingContext(context.Background(), md)
	nSampleRate, _ := strconv.ParseInt(SampleRate, 10, 32)
	nLang := pb_common.LangCode(pb_common.LangCode_value[Lang])
	model := pb.Model{Model: Model, SampleRate: int32(nSampleRate), Lang: nLang}
	status, err := c.Find(ctx, &model)
	if err != nil {
		fmt.Printf("can't find model: %v\n", err)
		return
	}

	for i := 0; i < 100; i++ {
		if GetStatus(status) {
			fmt.Printf("Model %v is running\n", model)
			// test_simple()
			break
		}
		time.Sleep(2 * time.Second)
		fmt.Printf("Wait for loading model image file...\n")
	}
	// return status
}

func GetStatus(serverStatus *pb.ServerStatus) bool {
	modelHost := strings.Split(Host, ":")[0]
	modelPort := strings.Split(serverStatus.GetServerAddress(), ":")[1]
	fmt.Printf("model addr = %v\n", modelHost + ":" + modelPort)
	conn, err := grpc.Dial(modelHost + ":" + modelPort, grpc.WithInsecure())
	defer conn.Close()
	if err != nil {
		fmt.Println(err)
		return false
	}

	c := pb.NewSttRealServiceClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	nSampleRate, _ := strconv.ParseInt(SampleRate, 10, 32)
	nLang := pb_common.LangCode(pb_common.LangCode_value[Lang])
	model := pb.Model{Model: Model, SampleRate: int32(nSampleRate), Lang: nLang}
	status, err := c.Ping(ctx, &model)
	return status.GetRunning()
}

func run_stt(fname string) {
	if ServiceName == "simple" {
		test_simple(fname)
	} else if ServiceName == "stream" {
		test_stream(fname)
	} else if ServiceName == "streaming" {
		test_streaming(fname)
	}
}

func run_worker() {
	for i := 0; i < ChannelCnt; i++ {
		go func() {
			for {
				fname := <-chan_file
				if fname == "end" {
					done <- true
					break
				} else {
					run_stt(fname)
				}
			}
		}()
	}
}

func main() {
	mainStart := time.Now()
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s [-option] <filename>\n", os.Args[0])
		flag.PrintDefaults()
	}

	total_time = 0
	// chan_cnt := flag.Int("c", 1, "thread count")
	// loop_cnt := flag.Int("l", 1, "loop count")
	flag.IntVar(&ChannelCnt, "c", 1, "thread count")
	flag.IntVar(&LoopCnt, "l", 1, "loop count")
	// Fname = flag.String("f", defaultFname, "file path")
	Dname = flag.String("d", "", "directory path")
	flag.StringVar(&ServiceName, "s", "simple", "service name (simple, stream)")
	flag.StringVar(&Lang, "lang", "kor", "language")
	flag.StringVar(&Model, "m", "baseline", "model")
	flag.StringVar(&SampleRate, "r", "8000", "sample rate")
	flag.StringVar(&Host, "host", address, "stt server address")
	flag.BoolVar(&RealTime, "rt", false, "read file stream like realtime")
	flag.StringVar(&Cert, "cert", "", "if exist, GRPC SSL Certiciate file name")
	flag.BoolVar(&Interim, "interim", false, "recv interim result")
	flag.StringVar(&ResultSuffix, "w", "", "write result to file with extension name")
	flag.BoolVar(&ShowVersion, "v", false, "show version")

	flag.Parse()
	log.SetFlags(log.Lmicroseconds)

	if ShowVersion {
		fmt.Fprintf(os.Stderr, "sttcli version: %s\n", GitVersion)
		return
	}

	if flag.NArg() > 0 {
		cmd := flag.Arg(0)
		if cmd == "start" {
			Ping()
			return
		} else if cmd == "stop" {
			Stop()
			return
		} else if cmd == "restart" {
			Restart()
			Ping()
			return
		} else {
			Fname = &cmd
		}
	} else if *Dname == "" {
		flag.Usage()
		return
	}

	var files []string
	total_cnt := ChannelCnt * LoopCnt
	if *Dname != "" {
		pcmFiles, err := ioutil.ReadDir(*Dname)
		if err != nil {
			log.Fatal(err)
		} else {
			for _, f := range pcmFiles {
				files = append(files, *Dname+"/"+f.Name())
			}
		}
		total_cnt = len(pcmFiles) * LoopCnt
	} else {
		for i := 0; i < ChannelCnt; i++ {
			files = append(files, *Fname)
		}
	}

	run_worker()

	for i := 0; i < LoopCnt; i++ {
		for _, fname := range files {
			chan_file <- fname
		}
	}

	// Wait All Routine
	for i := 0; i < ChannelCnt; i++ {
		chan_file <- "end"
		<-done
	}

	log.Printf("total elapsed time is %s", total_time)
	log.Printf("average elapsed time is %s",
		time.Duration(float64(total_time)/float64(total_cnt)))
	mainEnd := time.Since(mainStart)
	log.Printf("execution time is %s", mainEnd)
}
