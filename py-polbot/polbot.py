#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import time
import grpc
import getopt
import requests
import urllib
import json
from freeswitch import *
from ESL import *

con = EventConsumer()
# 와이즈넛 폴봇용 챗봇 서버 정보
BASE_URL = 'http://211.39.140.44:8080'

# WARNING: known bugs with hangup hooks, use with extreme caution
def hangup_hook(session, what, arg=''):
    con.cleanup()
    consoleLog("info","hangup hook for %s!!\n\n" % what)
    return
  
def input_callback(session, what, obj, arg=''):
    consoleLog("info", "input_callback() called!! \n")
    if (what == "dtmf"):
        consoleLog("info", what + " " + obj.digit + "\n")
    else:
        consoleLog("info", what + " " + obj.serialize() + "\n")
    return 

def open_dialog():
    url = BASE_URL + '/api/sessionRequest'
    headers = \
    {
        'Content-Type':'application/json', 
        'charset':'utf-8',
    }

    response = requests.post(url, headers=headers)
    resp = response.json()
    json.dumps(resp, ensure_ascii=False, encoding='utf8').encode('utf8')

    key = resp['sessionKey']
    consoleLog('info', 'sessionKey: {} \n'.format(key))
    return key

def dialog_chat(key, intent, text):
    url = BASE_URL + '/api/iChatResponse'
    headers = \
    {
        'Content-Type':'application/json', 
        'charset':'utf-8',
    }
    payload = \
    {
      'projectId':'713a66231ba5',
      'query':'%s' % text,
      'sessionKey':'%s' % key,
      'process':'%s' % intent
    }

    response = requests.post(url, headers=headers, params=payload)
    resp = response.json()
    #consoleLog('info', 'response {} \n'.format(resp)) 
    json.dumps(resp, ensure_ascii=False).encode('utf-8')
    return resp

def stt_event_loop(answer):
    result = '' 
    event_count = 0
    while True:
        event = con.pop()
        if event: 
            event_count = event_count + 1
            consoleLog("info", "Event: " + event.getHeader('Event-Subclass') + " \n")
            consoleLog("info", "Event body: " + event.getBody() + " \n")
            try:
                body = json.loads(event.getBody())
                text = body['text'].rstrip('\n').lstrip('\n')
                result += ' ' + text.encode('utf-8')
                if '차량번호' in answer:
                    result = '39라 5511'
                #session.execute('break')
            except Exception as ex: 
                consoleLog("info", "Exception: %s \n" % ex)
        else:
            break

    consoleLog("info", "event count: %d %s \n" % (event_count, result))
    return result

def handler(session, args):
    uuid = session.get_uuid()
    session.sleep(100)
    session.answer()
    session.setHangupHook(hangup_hook)
    session.setInputCallback(input_callback)
    session.setVariable('InputCallback', 'input_callback')

    consoleLog("info", "session uuid: " + uuid + " \n")

    stt_data = ' ' + uuid + ' start ko-KR'
    consoleLog("info", "uuid_brain_stt" + stt_data + " \n")
    new_api_obj = API()
    new_api_obj.execute('uuid_brain_stt', stt_data) 
    
    # TODO: need to fix
    con.bind('CUSTOM', 'brain_stt::transcription')

    session.set_tts_params('simple_tts', 'ko-KR')
    session.execute('set', 'playback_terminators=#') 

    key = open_dialog()
    # 대화 시작
    intent = 'greeting'
    resp = dialog_chat(key, 'greeting', 'hi')

    if resp['isSuccess'] == True:
        answer = resp['answer']['message'].encode('utf-8')
        intent = resp['answer']['process']
    else:
        answer = '챗봇서버와 연결 오류로 통화를 종료합니다'
        session.execute('speak', 'simple_tts|ko-KR|' + answer)
        session.hangup()

    while session.ready():
        session.execute('speak', 'simple_tts|ko-KR|' + answer)
        result = stt_event_loop(answer)

        # TODO: need to fix
        if len(result) == 0:
            session.execute('playback', 'silence_stream://3000')
            result = stt_event_loop(answer)

        if len(result):
            resp = dialog_chat(key, intent, result) 
            if resp['isSuccess'] == True:
                answer = resp['answer']['message'].encode('utf-8')
                intent = resp['answer']['process']
                consoleLog('info', 'answer message {} to input {} \n'.format(answer, result)) 
                if resp['answer']['scenario_end'] == True:
                    break
            
    session.execute('speak', 'simple_tts|ko-KR|' + answer)
    session.hangup() #hangup the call

