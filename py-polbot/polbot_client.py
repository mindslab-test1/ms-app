#!/usr/bin/python
# -*- coding: utf-8 -*-
import time
import json
import requests
import urllib
import logging
import threading

import ESL

# 와이즈넛 폴봇용 챗봇 서버 정보
BASE_URL = 'http://211.39.140.44:8080'
WEBHOOK_URL = 'http://127.0.0.1:5000/webhook'

LOG_FORMAT = "[%(asctime)-10s] (%(filename)s:%(lineno)d) %(levelname)s - %(message)s"
logging.basicConfig(format=LOG_FORMAT)
logger = logging.getLogger("polbot")
logger.setLevel(logging.INFO)

class EventThread(threading.Thread):
    def __init__(self, target, args):
        super(EventThread, self).__init__()
        self.target = target
        self.args = args

    def run(self):
        logger.debug("Running target thread...")
        self.target(*self.args)

class PolbotClient(object):
    def __init__(self, host, port, password):
        self.host = host
        self.port = port
        self.password = password
        self.timeout = 5
        self.connected = False
        self.intent = 'greeting'
        self.result = 'hi'
        self.headers = \
        {
            'content-type':'application/json', 
            'charset':'utf-8',
        }

    def connect(self):
        self.conn = ESL.ESLconnection(self.host, self.port, self.password)
        if self.conn:
            self.connected = True
            self.conn.events('plain', 'all')

    def send_custom_event(self, resp):
        self.event = ESL.ESLevent("CUSTOM", "brain_stt::dialog")
        self.event.addHeader('projectId', '713a66231ba5')
        self.event.addHeader("query", self.result)
        self.event.addHeader("seesionKey", str(self.key))
        self.event.addBody(json.dumps(resp))
        self.conn.sendEvent(self.event)

    def open_dialog(self):
        url = BASE_URL + '/api/sessionRequest'
        response = requests.post(url, headers=self.headers)
        resp = response.json()
        json.dumps(resp, ensure_ascii=False, encoding='utf8').encode('utf8')

        self.key = resp['sessionKey']
        logger.info('sessionKey: {}'.format(self.key))

    def dialog_chat(self):
        url = BASE_URL + '/api/iChatResponse'
        payload = \
        {
          'projectId':'713a66231ba5',
          'query':'%s' % self.result,
          'sessionKey':'%s' % self.key,
          'process':'%s' % self.intent
        }

        response = requests.post(url, headers=self.headers, params=payload)
        resp = response.json()
        #json.dumps(resp, ensure_ascii=False).encode('utf-8')
        logger.debug('response: {}'.format(resp)) 
        self.send_custom_event(resp)

    def send_webhook(self, path, payload):
        url = WEBHOOK_URL + path
        response = requests.post(url, headers=self.headers, data=json.dumps(payload))
        #resp = response.json()
        #json.dumps(resp, ensure_ascii=False).encode('utf-8')
        logger.info('webhook response: {}'.format(response)) 

    def on_channel_park(self, e):
        self.uuid = e.getHeader('Unique-ID')
        self.app_name = e.getHeader('app_name')
        self.open_dialog()
        # 대화 시작
        self.conn.execute('answer', '', self.uuid)

    def on_channel_answer(self, e):
        self.dialog_chat()
        stt_command = 'uuid_brain_stt ' + self.uuid + ' start ko-KR'
        self.conn.bgapi(stt_command) 
        logger.debug('--- End of on_channel_answer() ---')

    def on_channel_hangup(self, e):
        self.intent = 'greeting'
        self.result = 'hi'
        logger.info('--- End of on_channel_hangup() ---')

    def on_custom(self, e):
        uuid = e.getHeader('Unique-ID')
        logger.info('uuid: {}'.format(uuid))

        body = json.loads(e.getBody())
        text = body['text'].rstrip('\n').lstrip('\n')
        self.result = text.encode('utf-8')
        if '차량번호' in self.result:
            self.result = '39라 5511'
        payload = \
        {
          'uuid':'%s' % self.uuid,
          'result':'%s' % self.result
        }
        self.send_webhook('/stt', payload)
        logger.info('result: {}'.format(self.result))
        if len(self.result):
            self.dialog_chat() 
        logger.debug('--- End of on_custom() ---')

    def process_chat(self, e):
        obj = json.loads(e.getBody(), encoding='utf-8')
        logger.debug('type: {}'.format(type(obj)))
        if obj['isSuccess'] == False:
            return

        self.answer = obj['answer']['message'].encode('utf-8')
        self.intent = obj['answer']['process']
        logger.info('answer: {}'.format(self.answer)) 
        logger.info('intent: {}'.format(self.intent)) 
        self.conn.execute('break', '', self.uuid)
        self.conn.execute('speak', 'simple_tts|ko-KR|' + self.answer, self.uuid)
        payload = \
        {
          'uuid':'%s' % self.uuid,
          'answer':'%s' % self.answer,
          'intent':'%s' % self.intent
        }
        self.send_webhook('/chat', payload)

#        if 'scenario_end' in self.intent:
#            self.conn.execute('sleep', '5000', self.uuid)
#            self.conn.execute('hangup')

    def process_event(self):
        while self.connected:
            e = self.conn.recvEvent()
            evt = json.loads(e.serialize('json'))
            event_name = evt['Event-Name'].lower()
            method_name = 'on_%s' % event_name
            #logger.info(method_name)
            if event_name == 'channel_park':
                self.on_channel_park(e)

            if event_name == 'channel_answer':
                self.on_channel_answer(e)

            if event_name == 'channel_hangup':
                self.on_channel_hangup(e)

            if event_name == 'channel_callstate':
                callstate = evt['Channel-Call-State']
                logger.info('callstate: {}'.format(callstate))
            
            if event_name == 'channel_state':
                state = evt['Channel-State']
                logger.info('state: {}'.format(state))

            if event_name == 'custom':
                subclass = evt['Event-Subclass'].lower()
                logger.info('subclass: {}'.format(subclass))
                if subclass == 'brain_stt::transcription':
                    self.on_custom(e)
                elif subclass == 'brain_stt::dialog':
                    self.process_chat(e)

            if event_name == 'channel_execute_complete':
                app = evt['variable_current_application']
                if 'speak' in app:
                    logger.debug('execute complete: {}'.format(app)) 
                    app_data = evt['variable_current_application_data'].encode('utf-8') 
                    logger.debug('execute complete: {}'.format(app_data))


if __name__ == '__main__':
    polbot = PolbotClient('127.0.0.1', '8021', 'ClueCon')
    polbot.connect()
    polbot.process_event()

