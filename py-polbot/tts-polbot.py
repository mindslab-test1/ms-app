#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import time
import grpc
import getopt
import requests
import urllib
import json
from freeswitch import *

con = EventConsumer()
# 와이즈넛 폴봇용 챗봇 서버 정보
BASE_URL = 'http://211.39.140.44:8080'

# WARNING: known bugs with hangup hooks, use with extreme caution
def hangup_hook(session, what, arg=''):
    con.cleanup()
    consoleLog("info","hangup hook for %s!!\n\n" % what)
    return
  
def input_callback(session, what, obj, arg=''):
    consoleLog("info", "input_callback() called!! \n")
    if (what == "dtmf"):
        consoleLog("info", what + " " + obj.digit + "\n")
    else:
        consoleLog("info", what + " " + obj.serialize() + "\n")
    return 

def open_dialog():
    url = BASE_URL + '/api/sessionRequest'
    headers = \
    {
        'Content-Type':'application/json', 
        'charset':'utf-8',
    }

    response = requests.post(url, headers=headers)
    resp = response.json()
    json.dumps(resp, ensure_ascii=False, encoding='utf8').encode('utf8')

    key = resp['sessionKey']
    consoleLog('info', 'sessionKey: {} \n'.format(key))
    return key

def dialog_chat(key, text):
    url = BASE_URL + '/api/iChatResponse'
    headers = \
    {
        'Content-Type':'application/json', 
        'charset':'utf-8',
    }
    payload = \
    {
      'projectId':'713a66231ba5',
      'query':'%s' % text,
      'sessionKey':'%s' % key,
      'process':'greeting'
    }

    response = requests.post(url, headers=headers, params=payload)
    resp = response.json()
    consoleLog('info', 'response {} \n'.format(resp)) 
    json.dumps(resp, ensure_ascii=False).encode('utf-8')
    message = resp['answer']['message']
    return resp

def handler(session, args):
    uuid = session.get_uuid()
    session.sleep(100)
    session.answer()
    session.setHangupHook(hangup_hook)
    session.setInputCallback(input_callback)
    session.setVariable('InputCallback', 'input_callback')

    consoleLog("info", "session uuid: " + uuid + " \n")

    stt_data = ' ' + uuid + ' start ko-KR'
    consoleLog("info", "uuid_brain_stt" + stt_data + " \n")
    new_api_obj = API()
    new_api_obj.execute('uuid_brain_stt', stt_data) 
    con.bind("CUSTOM", "brain_stt::transcription")

    session.set_tts_params('simple_tts', 'ko-KR')
    session.execute('set', 'playback_terminators=#') 

    key = open_dialog()
    # 대화 시작
    resp = dialog_chat(key, 'hi')

    if resp['isSuccess'] == True:
        answer = resp['answer']['message']
    else:
        answer = '챗봇서버와 연결 오류로 통화를 종료합니다'
        session.execute('speak', 'simple_tts|ko-KR|' + answer.encode('utf-8'))
        session.hangup()

    event_count = 0
    while session.ready():
        session.setVariable('InputCallback', 'input_callback')
        session.execute('speak', 'simple_tts|ko-KR|' + answer.encode('utf-8'))
        result = ''
        while True:
            event = con.pop()
            if event: 
                event_count = event_count + 1
                consoleLog("info", "Event: " + event.getHeader('Event-Subclass') + " \n")
                consoleLog("info", "Event body: " + event.getBody() + " \n")
                try:
                    body = json.loads(event.getBody())
                    text = body['text'].rstrip('\n').lstrip('\n')
                    result += ' ' + text.encode('utf-8')
                    #session.execute('break')
                except Exception as ex: 
                    consoleLog("info", "Exception: %s \n" % ex)
            else:
                break

        consoleLog("info", "event count: %d %s \n" % (event_count, result))
        event_count = 0
        if result != '':
            resp = dialog_chat(key, result) 
            if resp['isSuccess'] == True:
                answer = resp['answer']['message']
                consoleLog('info', 'answer message {} to input {} \n'.format(answer.encode('utf-8'), result)) 
                if resp['answer']['scenario_end'] == True:
                    break
        else:
            session.sleep(3000)
            
    session.execute('speak', 'simple_tts|ko-KR|' + answer.encode('utf-8'))
    session.hangup() #hangup the call

