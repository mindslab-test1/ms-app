#!/usr/bin/python
# -*- coding: utf-8 -*-
import time
import json
import requests
import urllib
import logging
import threading

import ESL

# 와이즈넛 폴봇용 챗봇 서버 정보
BASE_URL = 'http://211.39.140.44:8080'

LOG_FORMAT = "[%(asctime)-10s] (%(filename)s:%(lineno)d) %(levelname)s - %(message)s"
logging.basicConfig(format=LOG_FORMAT)
logger = logging.getLogger("polbot")
logger.setLevel(logging.DEBUG)

def open_dialog():
    url = BASE_URL + '/api/sessionRequest'
    headers = \
    {
        'Content-Type':'application/json', 
        'charset':'utf-8',
    }

    response = requests.post(url, headers=headers)
    resp = response.json()
    json.dumps(resp, ensure_ascii=False, encoding='utf8').encode('utf8')

    key = resp['sessionKey']
    logger.info('sessionKey: {}'.format(key))
    return key

def dialog_chat(key, intent, text):
    url = BASE_URL + '/api/iChatResponse'
    headers = \
    {
        'Content-Type':'application/json', 
        'charset':'utf-8',
    }
    payload = \
    {
      'projectId':'713a66231ba5',
      'query':'%s' % text,
      'sessionKey':'%s' % key,
      'process':'%s' % intent
    }

    response = requests.post(url, headers=headers, params=payload)
    resp = response.json()
    logger.debug('response: {}'.format(resp)) 
         
    #json.dumps(resp, ensure_ascii=False).encode('utf-8')

    event = ESL.ESLevent("CUSTOM", "brain_stt::dialog")
    event.addHeader('projectId', '713a66231ba5')
    event.addHeader("query", text)
    event.addHeader("seesionKey", str(key))
    event.addBody(json.dumps(resp))
    conn.sendEvent(event)
    return resp

class EventThread(threading.Thread):
    def __init__(self, target, args):
        super(EventThread, self).__init__()
        self.target = target
        self.args = args

    def run(self):
        logger.debug("Running target thread...")
        self.target(*self.args)

def on_channel_park(e):
    uuid = e.getHeader('Unique-ID')
    app_name = e.getHeader('app_name')
    key = open_dialog()
    # 대화 시작
    resp = dialog_chat(key, 'greeting', 'hi')
#    conn.execute('sleep', '1000', uuid)
    conn.execute('answer', '', uuid)
    return (key, resp)

def on_channel_answer(e):
    uuid = e.getHeader('Unique-ID')
    stt_command = 'uuid_brain_stt ' + uuid + ' start ko-KR'
    conn.api(stt_command) 

    #conn.execute('playback', 'silence_stream://5000', uuid)
    logger.info('--- End of on_channel_answer() ---')

def on_custom(e, key, intent, answer):
    #uuid = e.getHeader('Unique-ID')
    body = json.loads(e.getBody())
    text = body['text'].rstrip('\n').lstrip('\n')
    result = text.encode('utf-8')
    if '차량번호' in answer:
        result = '39라 5511'
    logger.info('[{}], result: {}'.format(uuid, result))
    if len(result):
        dialog_chat(key, intent, result) 
    logger.info('--- End of on_custom() ---')

def process_chat(uuid, eb):
    logger.debug('uuid: {}'.format(uuid))
    #logger.info('event body: {}'.format(eb))
    #logger.info('type: {}'.format(type(eb)))
    obj = json.loads(eb, encoding='utf-8')
    logger.debug('type: {}'.format(type(obj)))
    if obj['isSuccess'] == False:
        return

    answer = obj['answer']['message'].encode('utf-8')
    intent = obj['answer']['process']
    logger.info('answer: {}'.format(answer)) 
    logger.info('intent: {}'.format(intent)) 
    conn.execute('break', '', uuid)
    conn.execute('speak', 'simple_tts|ko-KR|' + answer, uuid)

    #if obj['answer']['scenario_end']:
    #    conn.execute('hangup', '', uuid)
    return (answer, intent)


if __name__ == '__main__':
    conn = ESL.ESLconnection('127.0.0.1', '8021', 'ClueCon')
    conn.events('plain', 'all')
    result = ''
    while True:
        e = conn.recvEvent()
        events_message = json.loads(e.serialize('json'))
        event_name = events_message['Event-Name'].lower()
        method_name = 'on_%s' % event_name
        #logger.info(method_name)

        if event_name == 'channel_park':
            uuid = e.getHeader('Unique-ID')
            code = e.getHeader('code')
            key, resp = on_channel_park(e)

        if event_name == 'channel_answer':
            #logger.info("spawning thread")
            #t = EventThread(on_channel_answer, args=(e, key))
            #t.start()
            on_channel_answer(e)

        if event_name == 'channel_callstate':
            callstate = events_message['Channel-Call-State']
            logger.debug('callstate: {}'.format(callstate))
        
        if event_name == 'channel_state':
            state = events_message['Channel-State']
            logger.debug('state: {}'.format(state))

        if event_name == 'custom':
            subclass = events_message['Event-Subclass'].lower()
            logger.info('subclass: {}'.format(subclass))
            if subclass == 'brain_stt::transcription':
                on_custom(e, key, intent, answer)
            elif subclass == 'brain_stt::dialog':
                event_body = e.getBody()
                answer, intent = process_chat(uuid, event_body)

        if event_name == 'playback_stop':
            logger.info(events_message)

        if event_name == 'channel_execute_complete':
            app = events_message['variable_current_application']
            if 'speak' in app:
                logger.info('execute complete: {}'.format(app)) 
                app_data = events_message['variable_current_application_data'].encode('utf-8') 
                logger.info('execute complete: {}'.format(app_data))

