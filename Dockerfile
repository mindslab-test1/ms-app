FROM debian:buster
MAINTAINER Wonchang Shin "shinwc@mindslab.ai"

ENV FS_MAJOR 1.10
ENV FS_VERSION 1.10.5

RUN apt-get update && apt-get install -yq gnupg2 wget lsb-release \
    && wget -O - https://files.freeswitch.org/repo/deb/debian-release/fsstretch-archive-keyring.asc | apt-key add - \
    && echo "deb http://files.freeswitch.org/repo/deb/debian-release/ `lsb_release -sc` main" > /etc/apt/sources.list.d/freeswitch.list \
    && echo "deb-src http://files.freeswitch.org/repo/deb/debian-release/ `lsb_release -sc` main" >> /etc/apt/sources.list.d/freeswitch.list

RUN apt-get update && apt-get -y build-dep freeswitch \
    && apt-get install -y cmake \
    && apt-get install -y sudo \
    && apt-get install -y ansible \
    && apt-get install -y vim

WORKDIR /usr/local/src
RUN git clone https://github.com/signalwire/freeswitch.git -bv1.10.5 ./freeswitch
RUN cd freeswitch
#RUN git config pull.rebase true
#RUN ./bootstrap.sh -j 
#RUN ./configure
#RUN make && make install

ENV TZ=Asia/Seoul
#RUN localedef -f UTF-8 -i ko_KR ko_KR.utf8
#ENV LC_ALL=ko_KR.UTF-8
ENV USER=root

RUN mkdir -p /srv/maum
COPY . /srv/maum
WORKDIR /srv/maum
ENV PYTHONPATH=/srv/maum/lib/python

EXPOSE 5060
EXPOSE 5080

VOLUME /fsapplog

