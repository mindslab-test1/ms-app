# README for maumswitch docker image
---
## How to build image
### 도커 베이스 이미지 생성
현재 경로에 있는 Dockerfile을 기반으로 base 이미지를 아래와 같이 생성한다
```
docker build --tag shinwc/maumswitch:0.0.1 .
```
### 도커 컨테이너 생성
hostname을 localhost로 함
```
docker run -it --hostname localhost --net host  --name maumswitch-0.0.1 -v /fsapplog:/fsapplog shinwc/maumswitch:0.0.1 /bin/bash
```
### 도커 컨테이너가 실행되면 현재 작업 경로(`/srv/maum`)에서 ansible-role-maumswitch로 이동하여 ansible-playbook을 실행
```
cd ansible-role-maumswitch/tests
ansible-playbook -vv test.yaml
```
### Docker container commit
Playbook 실행을 완료하고 docker container에서 exit하고 현재까지 수정된 container를 다시 이미지로 저장
```

docker commit maumswitch-0.0.1 shinwc/maumswitch:0.0.2 
```

## build Python ESL
- 파이썬 ESL 모듈은 libesl(Event Socket Library)기반으로 빌드
- python으로 ESL connection을 만들고 ESL event를 수신하고 command를 전송할 수 있음
- build 방법
	```
	cd /usr/local/src/freeswitch/libs/esl
	make pymod
	make pymod-install
	```
- 위의 명령을 수행하면 `/usr/lib/python2.7/dist-packates`에 `ESL.py`과 `_ESL.so`가 설치됨. 소스 경로 
```
root@localhost:/usr/local/src/freeswitch/libs/esl/python# vim ESL.py
```
- 동작확인
	```
	root@localhost:/usr/lib/python2.7/dist-packages# python
	Python 2.7.16 (default, Oct 10 2019, 22:02:15) 
	[GCC 8.3.0] on linux2
	Type "help", "copyright", "credits" or "license" for more information.
	>>> import ESL
	>>>
	```
## ESL 실행하기 위해 설정해야 할  참고사항

Adding to PYTHONPATH environment variable, if the file is in /usr/src/foo/bar.py, add the following to your system environment startup

export PYTHONPATH=$PYTHONPATH:/usr/src
Don't forget, the foo package directory will need an __init__.py.

In the shell where freeswitch is started, this environment variable will need to be defined.




