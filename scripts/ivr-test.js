var languageCode = "en";
var soundDir = "ivr/";
var dtmf_digits ="";
var artist_content;
var category_content;

function on_dtmf(session, type, digits, arg)
{
	console_log('notice', 'type: ' + type + '\n');
	if (type == 'event') {
		console_log('notice', 'Event: ' + digits.serialize() + '\n');
		dtmf_digits += digits.getHeader('DTMF-Digit');
	}
	else {
		console_log('notice', 'digits: ' + digits.digit + '\n');
		dtmf_digits += digits.digit;
	}

	return(false);
}

function recursiveGetProperty(obj, lookup, callback) 
{
	for (var property in obj) {
		if (property == lookup) {
			callback(obj[property]);
		} else if (obj[property] instanceof Object) {
			recursiveGetProperty(obj[property], lookup, callback);
		}
	}
}

function traverse_json_data(obj) 
{
	var str = '';
	for (var property in obj) {
		if (obj[property] instanceof Object) {
			str += '+---';
			console_log('notice', str + property + '\n');
			traverse_json_data(obj[property]);
		} else {
			console_log('notice', property + ': ' + obj[property]);
		}
	}
}

function build_ivr_submenu() 
{
	var result;
	var data;
	
	result = fetchUrl("http://192.168.100.53:8080/Listen-backend-api/restapi/vivoivr/getArtistContentList?tnbFree=true&artistShortCode=82");
	data = JSON.parse(result);
	//traverse_json_data(data);
	
	for (var i in data.contentOffers) {
		ivrPreviewUrl = data.contentOffers[i].contentMeta.ivrVoiceWavUrl;
		console_log('notice', 'ivrPreviewUrl: ' + ivrPreviewUrl);
	}
	artist_content = data.contentOffers;

	result = fetchUrl("http://192.168.100.53:8080/Listen-backend-api/restapi/vivoivr/getCategoryContentList?categoryId=1190");
	data = JSON.parse(result);
	//traverse_json_data(data);

	for (var i in data.contentOffers) {
		ivrPreviewUrl = data.contentOffers[i].contentMeta.ivrVoiceWavUrl;
		console_log('notice', 'ivrPreviewUrl: ' + ivrPreviewUrl);
	}
	category_content = data.contentOffers;
}

function say_ivr_menu(ivrsession, menu, timeout) 
{
	var repeat = 0;

	ivrsession.flushDigits();
	dtmf_digits  = "";

	var validdigits = "";
	for (var digit in menu.digits) {
		validdigits += digit;
	}

	console_log('notice', 'menu=[' + menu.list + "] validdigits=[" + validdigits + ']\n');
	sendAppEvent(JSON.stringify(menu));

	while (ivrsession.ready() && dtmf_digits == "" && repeat < 3) {
		if(menu.greeting) {
			ivrsession.sleep(500);
			ivrsession.speak("flite", "slt", menu.greeting.text, on_dtmf);
		}
		else if(menu.content) {
			ivrsession.sleep(500);
			var ret = ivrsession.streamFile(menu.content.ivrPreviewUrl, on_dtmf);
			if (dtmf_digits != "") {
				console_log('notice', '1>>> dtmf_digits: ' + dtmf_digits);
				break;
			}
		}

		if (ivrsession.ready()) {
			for(var i=0; i<menu.list.length; i++) {
				ivrsession.sleep(500);
				ivrsession.speak("flite", "slt", menu.list[i], on_dtmf);
				if (dtmf_digits != "") {
					console_log('notice', '2>>> dtmf_digits: ' + dtmf_digits);
					break;
				}
			}
		}

		if (ivrsession.ready() && dtmf_digits == "") {
			dtmf_digits = ivrsession.getDigits(1, "", timeout);
			if (dtmf_digits == "") {
				repeat++;
			}
		}
	}
	return(dtmf_digits);
}

function nav_preview_contents(sel, menu, contents) 
{
	var size = Object.keys(contents).length;
	var idx = 0;
	console_log('notice', '>>> size: ' + size);

	while (sel != "0" && idx < size  && idx >= 0) {
		console_log('notice', '>>> idx: ' + idx);
		if (contents[idx].contentMeta.ivrVoiceWavUrl) {
			menu.content.title = contents[idx].contentMeta.title;
			menu.content.artist = contents[idx].contentMeta.artist;
			menu.content.imageUrl = contents[idx].contentMeta.imageUrl;
			menu.content.ivrPreviewUrl = contents[idx].contentMeta.ivrVoiceWavUrl;

			sel = say_ivr_menu(session, menu, 3000);
		}
		else {
			console_log('notice', '>>> ivrVoiceWavUrl is null!');
			size = idx;
		}

		if(sel == "2") {
			idx--;
			if (idx < 0) idx = size - 1;
			console_log('notice', '>>> sel 2: ' + idx);

		} else if(sel == "3") {
			idx++;
			if (idx > (size-1)) idx = 0;
		}
	}

	return(sel);
}

function sendAppEvent(msg)
{
   e = new Event("CUSTOM", "MyEvent::Notify");
   e.addHeader("My-Header", "IVR App Data");
   e.addHeader("Unique-ID", session.uuid);
   e.addBody(msg + '\n');
   e.fire();
}

function playFile(fileName, callback, callbackArgs)
{
    sendAppEvent(JSON.stringify(ivr_menu_json.greeting));
    session.streamFile(soundDir + fileName, callback, callbackArgs);
}

var msel = "";
var config = include('./config.json');

session.answer();

var m = ivr_menu_json.mainmenu;
msel = say_ivr_menu(session, m, 6000);
session.execute('detect_speech', 'pocketsphinx yes_no');

build_ivr_submenu();

while (session.ready()) {
	if (msel == "0") {
		m = ivr_menu_json.mainmenu;
		msel = say_ivr_menu(session, m, 3000);
	} else if(msel == "3") {
		msel = nav_preview_contents(msel, ivr_menu_json.submenu1, artist_content);
	} else if(msel == "9") {
		msel = nav_preview_contents(msel, ivr_menu_json.submenu1, category_content);
	} else {
		m = ivr_menu_json.mainmenu;
		msel = say_ivr_menu(session, m, 3000);
	};
}

