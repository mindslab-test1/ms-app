var languageCode = "ko-KR";
var soundDir = "ivr/";
var dtmf_digits;

function my_dtmf_callback(dtmf_str, digits, args)
{
    console_log('debug', 'you dialed the following ' + dtmf_str + ':' + digits + '\n');
    return true;
}

function on_dtmf(session, type, digits, arg)
{
	console_log('notice', 'type: ' + type + '\n');
	if (type == 'event') {
		console_log('notice', 'Event: ' + digits.serialize() + '\n');
		dtmf_digits += digits.getHeader('DTMF-Digit');
	}
	else {
		console_log('notice', 'digits: ' + digits.digit + '\n');
		dtmf_digits += digits.digit;
	}

	return true;
}

// Define callback function
function on_event(session, type, event_obj, user_data) {
    console_log("notice", "Event: " + type + "\n"); 
}

function sendAppEvent(msg)
{
   e = new Event("CUSTOM", "MyEvent::Notify");
   e.addHeader("My-Header", "IVR App Data");
   e.addHeader("Unique-ID", session.uuid);
   e.addBody(msg + '\n');
   e.fire();
}

function playFile(fileName, callback, callbackArgs)
{
    sendAppEvent("Playback: " + soundDir + fileName);
    sendAppEvent("Announcement: Thank you for calling. If you know your party's extension, please enter it now or dia 9 for directory");
    session.streamFile(soundDir + fileName, callback, callbackArgs);
}

var message;

if (session.ready()) {
    uuid = session.uuid;
    console_log('info', 'session uuid: ' + uuid + '\n');
    session.sleep(100);
    session.answer();
    sendAppEvent(">>>>> Answering call");

    stt_data = ' ' + uuid + ' start ko-KR';
    apiExecute('uuid_brain_stt', stt_data);
    console_log('info', 'uuid_brain_stt: ' + stt_data + '\n');

    session.execute('set', 'playback_terminators=none');
    session.execute('speak', 'simple_tts|ko-KR|KB국민 굿데이올림카드 시그니처 커피에서 일시불 4000원 사용하셨습니다.');
    sendAppEvent("speak(simple_tts, ko-KR): ");

    session.speak('simple_tts', 'ko-KR', 'KB국민 굿데이올림카드 시그니처 커피에서 일시불 4000원 사용하셨습니다.', on_event);
    /*
       sendAppEvent("Speak(flite, awb): Please click the call button to make a call to see visually!");
       session.speak("flite", "awb", "Please click the call button to make a call to see visually.");
       sendAppEvent("Speak(flite, awb): You can select the menu using DTMF key or voices!");
       session.speak("flite", "awb", "You can select the menu using DTMF key or voices.");
     */
    sendAppEvent(">>>>> End of Call");
}

exit();
