#ifndef __MOD_BRAIN_STT_H__
#define __MOD_BRAIN_STT_H__

#include <switch.h>
#include <speex/speex_resampler.h>

#include <unistd.h>

#define MAX_SESSION_ID (256)

#define MY_BUG_NAME "brain_stt"
#define TRANSCRIBE_EVENT_RESULTS "brain_stt::transcription"

// simply write a wave file
//#define DEBUG_TRANSCRIBE 0


#ifdef DEBUG_TRANSCRIBE

/* per-channel data */
struct cap_cb {
	switch_buffer_t *buffer;
	switch_mutex_t *mutex;
	char *base;
    SpeexResamplerState *resampler;
    FILE* fp;
};
#else
/* per-channel data */
typedef void (*responseHandler_t)(switch_core_session_t* session, const char* json);

struct cap_cb {
	switch_mutex_t *mutex;
	char sessionId[MAX_SESSION_ID];
	char *base;
  SpeexResamplerState *resampler;
	void* streamer;
	responseHandler_t responseHandler;
	switch_thread_t* thread;
	int end_of_utterance;
};
#endif

#endif