# mod_brain_stt

A Freeswitch module that generates real-time transcriptions on a Freeswitch channel by using Mindslab's Brain Speech-to-Text API.

## API

### Commands
The freeswitch module exposes the following API commands:

```
uuid_brain_stt <uuid> start <lang-code>
```
Attaches media bug to channel and performs streaming recognize request.
- `uuid` - unique identifier of Freeswitch channel
- `lang-code` - language to use for speech recognition, only `ko-KR` lang code is available

```
uuid_google_transcribe <uuid> stop
```
Stop transcription on the channel.

### Events
`brain_stt::transcription` - returns an final transcription.  The event contains a JSON body describing the transcription result:
```js
{
    "text":"속도위반", 
    "start":373800,
    "end":623772
}
```

### How to build `mod_brain_stt` custom module 
- 마인즈랩 brain STT와 grpc(w2l.proto 기반)연동하여 STT(Speech-To-Text) 수행하는 모듈
- `mod_google_transcribe`를 참고하여 개발함
- `/usr/local/src/freeswitch/src/mod/application/mod_brain_stt`에 해당 코드와 빌드에 필요한 파일이 위치함
```
-rw-r--r-- 1 root root     499 Jan 18 12:56 Makefile.am
-rw-r--r-- 1 root root    4774 Jan 17 16:32 README.md
-rw-r--r-- 1 root root     411 Jan 17 16:32 build.sh.w2l
-rw-r--r-- 1 root root   10092 Jan 18 16:35 google_glue.cpp
-rw-r--r-- 1 root root     511 Jan 18 11:19 google_glue.h
-rw-r--r-- 1 root root    6983 Jan 18 15:41 mod_brain_stt.c
-rw-r--r-- 1 root root     850 Jan 17 16:32 mod_brain_stt.h
-rw-r--r-- 1 root root     333 Jan 17 16:32 w2l.proto
```
- `mod_google_transcribe`를 참고하여 개발함
- `build.sh.w2l`를 실행해서 w2l.proto 파일의 grpc, protobuf 관련 C++ 코드를 생성한다.
    ```
    sh build.sh.w2l
    ```
- `Makefile.am`에 빌드에 필요한 설정
- `/usr/local/freeswitch/src`로 이동하여 다음과 같은 작업을 수행
	+ `modules.conf`에 `application/mod_brain_stt`를 추가
	+ `configure.ac`에 아래와 같이 `src/mod/application/mod_brain_stt` 라인을 추가 
	```
	AC_CONFIG_FILES([Makefile
			build/Makefile
			tests/unit/Makefile
			src/Makefile
			src/mod/Makefile
			src/mod/applications/mod_audio_fork/Makefile
			src/mod/applications/mod_brain_stt/Makefile
			src/mod/applications/mod_simple_tts/Makefile
	``` 
	+ `./bootstrap.sh -j`를 실행하면 automake가 실행되어 `Makefile.am`기반의 `Makefile.in`을 생성
	+ `./configure --with-extra --with-lws`를 실행하면 `Makefile`이 생성됨
	+ `make -j`를 실행하면 build가 진행된다.
	+ 마지막으로 `make install`을 수행하며 설치가 완료된다.
- 이후 `mod_brain_stt` 관련 코드가 수정되면 `/usr/local/src/freeswitch/src/mod/application/mod_brain_stt`에서 `make && make install`만 수행하면 된다.

