#include <cstdlib>

#include <switch.h>
#include <switch_json.h>
#include <grpc++/grpc++.h>

#include "w2l.grpc.pb.h"

#include "mod_brain_stt.h"

#define BUFFER_SECS (3)

using grpc::Status;

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReaderWriter;

using maum::brain::w2l::SpeechToText;
using maum::brain::w2l::Speech;
using maum::brain::w2l::Text;
using maum::brain::w2l::TextSegment;

class GStreamer;

class GStreamer {
public:
	GStreamer(switch_core_session_t *session, u_int16_t channels, char* lang) : 
    m_session(session), m_writesDone(false) {
    switch_channel_t *channel = switch_core_session_get_channel(session);

		//auto creds = grpc::InsecureChannelCredentials();
  		m_channel = grpc::CreateChannel("125.132.250.242:15001", grpc::InsecureChannelCredentials());
		//m_channel = grpc::CreateChannel("125.132.250.242:15001", creds);
		
  	m_stub = SpeechToText::NewStub(m_channel);

    // number of channels in the audio stream (default: 1)
    /*
    if (channels > 1) {
      config->set_audio_channel_count(channels);
      switch_log_printf(SWITCH_CHANNEL_SESSION_LOG(m_session), SWITCH_LOG_DEBUG, "audio_channel_count %d\n", channels);
    }
    */

    m_context.AddMetadata("in.lang", "kor");
    m_context.AddMetadata("in.samplerate", "8000");
    m_context.AddMetadata("in.model", "callbot");

  	// Begin a stream.
  	m_streamer = m_stub->StreamRecognize(&m_context);

  	// Write the first request, containing the config only.
  	m_streamer->Write(m_request);
	}

	~GStreamer() {
		switch_log_printf(SWITCH_CHANNEL_SESSION_LOG(m_session), SWITCH_LOG_DEBUG, "GStreamer::~GStreamer - deleting channel and stub\n");
	}

	bool write(void* data, uint32_t datalen) {
    m_request.set_bin(data, datalen);
    bool ok = m_streamer->Write(m_request);
    return ok;
  }

	uint32_t nextMessageSize(void) {
		uint32_t size = 0;
		m_streamer->NextMessageSize(&size);
		return size;
	}

	bool read(TextSegment* response) {
		return m_streamer->Read(response);
	}

	grpc::Status finish() {
		return m_streamer->Finish();
	}

	void writesDone() {
    // grpc crashes if we call this twice on a stream
    if (!m_writesDone) {
      m_streamer->WritesDone();
      m_writesDone = true;
    }
	}

protected:

private:
	switch_core_session_t* m_session;
  grpc::ClientContext m_context;
	std::shared_ptr<grpc::Channel> m_channel;
	std::unique_ptr<SpeechToText::Stub> m_stub;
	std::unique_ptr< grpc::ClientReaderWriterInterface<Speech, TextSegment> > m_streamer;
	Speech m_request;
  bool m_writesDone;
};

static void *SWITCH_THREAD_FUNC grpc_read_thread(switch_thread_t *thread, void *obj) {
	struct cap_cb *cb = (struct cap_cb *) obj;
	GStreamer* streamer = (GStreamer *) cb->streamer;

  switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_DEBUG, "grpc_read_thread: session %s started \n", cb->sessionId) ;
  // Read responses.
  TextSegment response;
  while (streamer->read(&response)) {  // Returns false when no more to read.
    /*
    if (status.error_code() != gprc::StatusCode::OK) {
      switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "grpc_read_thread: error %s \n", status.error_message().c_str(), status.code()) ;
    }
    */
    switch_core_session_t* session = switch_core_session_locate(cb->sessionId);
    if (!session) {
      switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "grpc_read_thread: session %s is gone!\n", cb->sessionId) ;
      return nullptr;
    }

    switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_DEBUG, "STT result: %s, {%d} ~ {%d} \n", response.txt().c_str(), response.start(), response.end()) ;

    // TODO
    cJSON* jResult = cJSON_CreateObject();
    cJSON* jText = cJSON_CreateString(response.txt().c_str());
    cJSON* jStart = cJSON_CreateNumber(response.start());
    cJSON* jEnd = cJSON_CreateNumber(response.end());

    cJSON_AddItemToObject(jResult, "text", jText);
    cJSON_AddItemToObject(jResult, "start", jStart);
    cJSON_AddItemToObject(jResult, "end", jEnd);

    char* json = cJSON_PrintUnformatted(jResult);
    cb->responseHandler(session, (const char *) json);
    free(json);
    cJSON_Delete(jResult);

    switch_core_session_rwunlock(session);
    //switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_DEBUG, "grpc_read_thread: got %d responses\n", response.results_size());
  }

  {
    switch_core_session_t* session = switch_core_session_locate(cb->sessionId);
    if (session) {
      grpc::Status status = streamer->finish();
      switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_DEBUG, "grpc_read_thread: finish() status %s (%d)\n", status.error_message().c_str(), status.error_code()) ;
      switch_core_session_rwunlock(session);
    }
  }

  return nullptr;
}

extern "C" {
    switch_status_t google_speech_init() {
      return SWITCH_STATUS_SUCCESS;
    }

    switch_status_t google_speech_cleanup() {
      return SWITCH_STATUS_SUCCESS;
    }
    switch_status_t google_speech_session_init(switch_core_session_t *session, responseHandler_t responseHandler, 
          uint32_t samples_per_second, uint32_t channels, char* lang, void **ppUserData) {

      switch_channel_t *channel = switch_core_session_get_channel(session);
      struct cap_cb *cb;
      int err;

      cb =(struct cap_cb *) switch_core_session_alloc(session, sizeof(*cb));
      strncpy(cb->sessionId, switch_core_session_get_uuid(session), MAX_SESSION_ID);

      switch_mutex_init(&cb->mutex, SWITCH_MUTEX_NESTED, switch_core_session_get_pool(session));

      if (samples_per_second != 8000) {
          cb->resampler = speex_resampler_init(channels, samples_per_second, 8000, SWITCH_RESAMPLE_QUALITY, &err);
        if (0 != err) {
           switch_log_printf(SWITCH_CHANNEL_SESSION_LOG(session), SWITCH_LOG_ERROR, "%s: Error initializing resampler: %s.\n",
                                 switch_channel_get_name(channel), speex_resampler_strerror(err));
          return SWITCH_STATUS_FALSE;
        }
      } else {
        switch_log_printf(SWITCH_CHANNEL_SESSION_LOG(session), SWITCH_LOG_DEBUG, "%s: no resampling needed for this call\n", switch_channel_get_name(channel));
      }

      GStreamer *streamer = NULL;
      try {
        streamer = new GStreamer(session, channels, lang);
        cb->streamer = streamer;
      } catch (std::exception& e) {
        switch_log_printf(SWITCH_CHANNEL_SESSION_LOG(session), SWITCH_LOG_ERROR, "%s: Error initializing gstreamer: %s.\n", 
          switch_channel_get_name(channel), e.what());
        return SWITCH_STATUS_FALSE;
      }

      cb->responseHandler = responseHandler;

      // create the read thread
      switch_threadattr_t *thd_attr = NULL;
      switch_memory_pool_t *pool = switch_core_session_get_pool(session);

      switch_threadattr_create(&thd_attr, pool);
      switch_threadattr_stacksize_set(thd_attr, SWITCH_THREAD_STACKSIZE);
      switch_thread_create(&cb->thread, thd_attr, grpc_read_thread, cb, pool);

      *ppUserData = cb;
      return SWITCH_STATUS_SUCCESS;
    }

    switch_status_t google_speech_session_cleanup(switch_core_session_t *session, int channelIsClosing) {
      switch_channel_t *channel = switch_core_session_get_channel(session);
      switch_media_bug_t *bug = (switch_media_bug_t*) switch_channel_get_private(channel, MY_BUG_NAME);

      if (bug) {
        struct cap_cb *cb = (struct cap_cb *) switch_core_media_bug_get_user_data(bug);
        switch_mutex_lock(cb->mutex);

        // close connection and get final responses
        GStreamer* streamer = (GStreamer *) cb->streamer;
        if (streamer) {
          streamer->writesDone();

          switch_log_printf(SWITCH_CHANNEL_SESSION_LOG(session), SWITCH_LOG_INFO, "google_speech_session_cleanup: waiting for read thread to complete\n");
          switch_status_t st;
          switch_thread_join(&st, cb->thread);
          switch_log_printf(SWITCH_CHANNEL_SESSION_LOG(session), SWITCH_LOG_INFO, "google_speech_session_cleanup: read thread completed\n");

          delete streamer;
          cb->streamer = NULL;
        }

        if (cb->resampler) {
          speex_resampler_destroy(cb->resampler);
        }
        switch_channel_set_private(channel, MY_BUG_NAME, NULL);
			  switch_mutex_unlock(cb->mutex);

        if (!channelIsClosing) {
          switch_core_media_bug_remove(session, &bug);
        }

			  switch_log_printf(SWITCH_CHANNEL_SESSION_LOG(session), SWITCH_LOG_INFO, "google_speech_session_cleanup: Closed stream\n");

			  return SWITCH_STATUS_SUCCESS;
      }

      switch_log_printf(SWITCH_CHANNEL_SESSION_LOG(session), SWITCH_LOG_INFO, "%s Bug is not attached.\n", switch_channel_get_name(channel));
      return SWITCH_STATUS_FALSE;
    }

    switch_bool_t google_speech_frame(switch_media_bug_t *bug, void* user_data) {
    	switch_core_session_t *session = switch_core_media_bug_get_session(bug);
    	struct cap_cb *cb = (struct cap_cb *) user_data;
      GStreamer* streamer = (GStreamer *) cb->streamer;
      uint8_t data[SWITCH_RECOMMENDED_BUFFER_SIZE];
      switch_frame_t frame = {};
      frame.data = data;
      frame.buflen = SWITCH_RECOMMENDED_BUFFER_SIZE;

      if (switch_mutex_trylock(cb->mutex) == SWITCH_STATUS_SUCCESS) {
        while (switch_core_media_bug_read(bug, &frame, SWITCH_TRUE) == SWITCH_STATUS_SUCCESS && !switch_test_flag((&frame), SFF_CNG)) {
          if (frame.datalen) {

            if (cb->resampler) {
              spx_int16_t out[SWITCH_RECOMMENDED_BUFFER_SIZE];
              spx_uint32_t out_len = SWITCH_RECOMMENDED_BUFFER_SIZE;
              spx_uint32_t in_len = frame.samples;
              size_t written;

              speex_resampler_process_interleaved_int(cb->resampler,
                (const spx_int16_t *) frame.data,
                (spx_uint32_t *) &in_len,
                &out[0],
                &out_len);

              streamer->write( &out[0], sizeof(spx_int16_t) * out_len);
            }

            else {
              streamer->write( frame.data, sizeof(spx_int16_t) * frame.samples);
            }
          }
        }
        switch_mutex_unlock(cb->mutex);
      }
    return SWITCH_TRUE;
  }
}
