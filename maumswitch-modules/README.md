# README for maumswitch modules 
---
A source collection of freeswitch modules, primarily built for for use with Mindslab AI STT and TTS engines.  These modules have beeen tested with Freeswitch version 1.10.5.

#### [mod_audio_fork](modules/mod_audio_fork/README.md)
A Freeswitch module that attaches a bug to a media server endpoint and streams L16 audio via websockets to a remote server. The audio is never stored to disk locally on the media server, making it ideal for "no data at rest" type of applications.  This module also supports receiving media from the server to play back to the caller, enabling the creation of full-fledged IVR or dialog-type applications.

#### [mod_simple_tts](modules/mod_simple_tts/README.md)
A tts provider module that integrates with Mindslab Brain Text-to-Speech API and integrates into freeswitch's TTS framework (i.e., usable with the mod_dptools 'speak' application)

#### [mod_brain_stt](modules/mod_brain_stt/README.md)
Adds a Freeswitch API call to start (or stop) real-time transcription on a Freeswitch channel using Mindslab Speech-to-Text API.
