/* 
 *
 * mod_simple_tts.c -- Mindslab GRPC-based text to speech
 *
 */
#include "mod_simple_tts.h"
#include "brain_glue.h"

#include <unistd.h>

SWITCH_MODULE_LOAD_FUNCTION(mod_simple_tts_load);
SWITCH_MODULE_SHUTDOWN_FUNCTION(mod_simple_tts_shutdown);
SWITCH_MODULE_DEFINITION(mod_simple_tts, mod_simple_tts_load, mod_simple_tts_shutdown, NULL);


static switch_status_t speech_open(switch_speech_handle_t *sh, const char *voice_name, int rate, int channels, switch_speech_flag_t *flags)
{	
	switch_uuid_t uuid;
	char uuid_str[SWITCH_UUID_FORMATTED_LENGTH + 1];
	char outfile[512] = "";
	simple_t *simple = switch_core_alloc(sh->memory_pool, sizeof(*simple));

	simple->rate = rate;

	/* Construct temporary file name with a new UUID */
	switch_uuid_get(&uuid);
	switch_uuid_format(uuid_str, &uuid);
	switch_snprintf(outfile, sizeof(outfile), "%s%s%s.tmp.wav", SWITCH_GLOBAL_dirs.temp_dir, SWITCH_PATH_SEPARATOR, uuid_str);
	simple->file = switch_core_strdup(sh->memory_pool, outfile);

	simple->fh = (switch_file_handle_t *) switch_core_alloc(sh->memory_pool, sizeof(switch_file_handle_t));

	sh->private_info = simple;

	switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_DEBUG, "speech_open - created file %s for rate %d\n", 
		simple->file, rate);

	return simple_speech_open(simple);

}

static switch_status_t speech_close(switch_speech_handle_t *sh, switch_speech_flag_t *flags)
{
	simple_t *simple = (simple_t *) sh->private_info;
	assert(simple != NULL);

	switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_DEBUG, "speech_close - closing file %s\n", simple->file);
	if (switch_test_flag(simple->fh, SWITCH_FILE_OPEN)) {
		switch_core_file_close(simple->fh);
	}
	unlink(simple->file);

	return SWITCH_STATUS_SUCCESS;
}

static switch_status_t speech_feed_tts(switch_speech_handle_t *sh, char *text, switch_speech_flag_t *flags)
{
	simple_t *simple = (simple_t *) sh->private_info;

	switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_DEBUG, "speech_feed_tts\n");
	if (SWITCH_STATUS_SUCCESS != simple_speech_feed_tts(simple, text)) {
		return SWITCH_STATUS_FALSE;
	}

	if (switch_core_file_open(simple->fh, simple->file, 0,	//number_of_channels,
							  simple->rate,	//samples_per_second,
							  SWITCH_FILE_FLAG_READ | SWITCH_FILE_DATA_SHORT, NULL) != SWITCH_STATUS_SUCCESS) {
		switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "Failed to open file: %s\n", simple->file);
		return SWITCH_STATUS_FALSE;
	}

	return SWITCH_STATUS_SUCCESS;
}

static void speech_flush_tts(switch_speech_handle_t *sh)
{
	simple_t *simple = (simple_t *) sh->private_info;
	assert(simple != NULL);

	switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_DEBUG, "speech_flush_tts\n");

	if (simple->fh != NULL && simple->fh->file_interface != NULL) {
		switch_core_file_close(simple->fh);
	}
}

static switch_status_t speech_read_tts(switch_speech_handle_t *sh, void *data, size_t *datalen, switch_speech_flag_t *flags)
{
	simple_t *simple = (simple_t *) sh->private_info;
	size_t my_datalen = *datalen / 2;

	assert(simple != NULL);

	if (simple->fh->file_interface == (void *)0) {
		switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "file [%s] has already been closed\n", simple->file);
		unlink(simple->file);
		return SWITCH_STATUS_FALSE;
	}

	if (switch_core_file_read(simple->fh, data, &my_datalen) != SWITCH_STATUS_SUCCESS) {
		switch_core_file_close(simple->fh);
		unlink(simple->file);
		return SWITCH_STATUS_FALSE;
	}
	*datalen = my_datalen * 2;
	if (datalen == 0) {
		switch_core_file_close(simple->fh);
		unlink(simple->file);
		return SWITCH_STATUS_BREAK;
	}
	return SWITCH_STATUS_SUCCESS;

}

static void text_param_tts(switch_speech_handle_t *sh, char *param, const char *val)
{

}

static void numeric_param_tts(switch_speech_handle_t *sh, char *param, int val)
{

}

static void float_param_tts(switch_speech_handle_t *sh, char *param, double val)
{

}

SWITCH_MODULE_LOAD_FUNCTION(mod_simple_tts_load)
{
	switch_speech_interface_t *speech_interface;

	/* connect my internal structure to the blank pointer passed to me */
	*module_interface = switch_loadable_module_create_module_interface(pool, modname);
	speech_interface = switch_loadable_module_create_interface(*module_interface, SWITCH_SPEECH_INTERFACE);
	speech_interface->interface_name = "simple_tts";
	speech_interface->speech_open = speech_open;
	speech_interface->speech_close = speech_close;
	speech_interface->speech_feed_tts = speech_feed_tts;
	speech_interface->speech_read_tts = speech_read_tts;
	speech_interface->speech_flush_tts = speech_flush_tts;
	speech_interface->speech_text_param_tts = text_param_tts;
	speech_interface->speech_numeric_param_tts = numeric_param_tts;
	speech_interface->speech_float_param_tts = float_param_tts;

	return simple_speech_load();
}

SWITCH_MODULE_SHUTDOWN_FUNCTION(mod_simple_tts_shutdown)
{

	return SWITCH_STATUS_UNLOAD;
}
