#ifndef __MOD_SIMPLE_TTS_H__
#define __MOD_SIMPLE_TTS_H__

#include <switch.h>

struct simple_data {
	int rate;
	char *file;
	switch_file_handle_t *fh;
};

typedef struct simple_data simple_t;

#endif