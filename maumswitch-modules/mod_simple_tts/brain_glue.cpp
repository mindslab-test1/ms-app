#include <unordered_set>
#include <switch.h>
#include <grpc++/grpc++.h>
#include <sstream>
#include <fstream>

#include "ng_tts.grpc.pb.h"
#include "mod_simple_tts.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReaderWriter;

using maum::brain::tts::NgTtsService;
using maum::brain::tts::TtsRequest;
using maum::brain::tts::TtsMediaResponse;
using maum::brain::tts::TtsJobStatusRequest;
using maum::brain::tts::TtsJobStatusResponse;

extern "C" {
	switch_status_t simple_speech_load() {
		return SWITCH_STATUS_SUCCESS;
	}

	switch_status_t simple_speech_open(simple_t* brain) {
		return SWITCH_STATUS_SUCCESS;
	}
	switch_status_t simple_speech_feed_tts(simple_t* brain, char* text) {
		ClientContext context;
	    TtsRequest request;
		TtsMediaResponse response;

		auto creds = grpc::InsecureChannelCredentials();
		auto channel = grpc::CreateChannel("125.132.250.242:20051", creds);
		auto stub = NgTtsService::NewStub(channel);
			
		context.AddMetadata("tempo", "1.0");
		context.AddMetadata("campaign", "1");
		context.AddMetadata("samplerate", "8000");

		request.set_lang(maum::common::Lang::ko_KR);
		//request.set_text(std::to_string(text));
		request.set_text(text);

		auto stream = stub->SpeakWav(&context, request);

		switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_INFO, "simple_speech_feed_tts: synthesizing using brain tts engine: %s\n", text); 

		std::string remained;

		while (stream->Read(&response)) {  // Returns false when no more to read.
			std::string bytes = response.mediadata();
			remained += bytes;
		}
		switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_INFO, "TTS result: %d bytes received \n", remained.size());

		std::ofstream outfile(brain->file, std::ofstream::binary);
		outfile << remained;
		outfile.close();

		return SWITCH_STATUS_SUCCESS;
	}
	switch_status_t simple_speech_unload() {
		return SWITCH_STATUS_SUCCESS;
	}

}
