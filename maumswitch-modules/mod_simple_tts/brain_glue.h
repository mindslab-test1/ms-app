#ifndef __BRAIN_GLUE_H__
#define __BRAIN_GLUE_H__

switch_status_t simple_speech_load();
switch_status_t simple_speech_open(simple_t* brain);
switch_status_t simple_speech_feed_tts(simple_t* brain, char* text);
switch_status_t simple_speech_unload();


#endif