# mod_simple_tts

A Freeswitch module that allows Mindslab Brain Text-to-Speech API to be used as a tts provider.

## API

### Commands
This freeswitch module does not add any new commands.  Rather, it integrates into the Freeswitch TTS interface such that it is invoked when an application uses the mod_dptools `speak` command with a tts engine of `simple_tts` with the language code `ko-KR` only 

### Events
None.

### How to build `mod_simple_tts` custom module 
- 마인즈랩 brain TTS와 grpc(ng_proto.proto 기반)연동하여 TTS(Text-To-Speech) 수행하는 모듈
- `mod_google_tts`를 참고하여 개발함
- `/usr/local/src/freeswitch/src/mod/application/mod_simple_tts에 해당 코드와 빌드에 필요한 파일이 위치함
    ```
    -rw-r--r-- 1 root root  563 Feb 22 13:43 Makefile.am
    -rw-r--r-- 1 root root  876 Feb 22 13:43 README.md
    -rw-r--r-- 1 root root 1891 Feb 22 13:43 brain_glue.cpp
    -rw-r--r-- 1 root root  259 Feb 22 13:43 brain_glue.h
    -rw-r--r-- 1 root root  552 Feb 22 15:32 build.sh
    drwxr-xr-x 3 root root   30 Feb 22 13:43 conf
    drwxr-xr-x 3 root root   20 Feb 22 13:43 maum
    -rw-r--r-- 1 root root 4841 Feb 22 13:43 mod_simple_tts.c
    -rw-r--r-- 1 root root  200 Feb 22 13:43 mod_simple_tts.h
    -rw-r--r-- 1 root root 1117 Feb 22 13:43 ng_tts.proto
    ```
- `build.sh`를 실행해서 ng_tts.proto 파일과 maum/common/lang.proto 파일의 grpc, protobuf 관련 C++ 코드를 생성한다.
    ```
    sh build.sh
    ```
- `Makefile.am`에 빌드에 필요한 설정
- `/usr/local/freeswitch/src`로 이동하여 다음과 같은 작업을 수행
	+ `modules.conf`에 `application/mod_simple_tts`를 추가
	+ `configure.ac`에 아래와 같이 `src/mod/application/mod_simple_tts` 라인을 추가 
	```
	AC_CONFIG_FILES([Makefile
			build/Makefile
			tests/unit/Makefile
			src/Makefile
			src/mod/Makefile
			src/mod/applications/mod_audio_fork/Makefile
			src/mod/applications/mod_brain_stt/Makefile
			src/mod/applications/mod_simple_tts/Makefile
	``` 
	+ `./bootstrap.sh -j`를 실행하면 automake가 실행되어 `Makefile.am`기반의 `Makefile.in`을 생성
	+ `./configure --with-extra --with-lws`를 실행하면 `Makefile`이 생성됨
	+ `make -j`를 실행하면 build가 진행된다.
	+ 마지막으로 `make install`을 수행하며 설치가 완료된다.
- 이후 `mod_simple_tts` 관련 코드가 수정되면 `/usr/local/src/freeswitch/src/mod/application/mod_simple_tts`에서 `make && make install`만 수행하면 된다.

