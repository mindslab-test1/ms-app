#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import time
import grpc
import getopt
import ng_tts_pb2
import ng_tts_pb2_grpc
from maum.common import lang_pb2
import multiprocessing
from multiprocessing import Process
from multiprocessing import Pool
import biz_log

logger = biz_log.getLogger('test')

# For your reference
SDN = '10.122.64.119:31050'
TTS = '10.122.64.119:30301'

def usage():
    logger.info('%s [-h host:port] textfile' % sys.argv[0])

def text_from_file(filename):
    with open(filename, "r") as f:         
        for line in f.readlines():
            li=line.lstrip()
            if not li.startswith("#"):
                tts_text = li 
        return tts_text

def unittask(index):
    channel = grpc.insecure_channel(host)
    stub = ng_tts_pb2_grpc.NgTtsServiceStub(channel)
    req = ng_tts_pb2.TtsRequest()
    req.lang = lang_pb2.kor
    req.sampleRate = sample_rate
    req.text = text_from_file(filename)
    req.speaker = 0
    logger.info('TTS task[%3d] req.text: %s' % (index, req.text))

    st = time.time()
    try:
        metadata={(b'tempo', b'1.0'), (b'campaign', b'1')}
        # resp = stub.SpeakWav(req, timeout=60)
        resp = stub.SpeakWav(req, metadata=metadata)
        f = open('test%03d.wav' % index, 'w')
        remained = 0 
        cnt = 0
        for tts in resp:
            f.write(tts.mediaData)
            remained += len(tts.mediaData)
            if (remained >= 22094): 
                cnt = cnt + 1
                if (cnt == 1):
                    text = 'TTS task[%3d] 1st media at: %.3f' % (index, (time.time() -st))
                    logger.info(text)
                remained = 0
    except grpc.RpcError as e:
        print e
    text = 'TTS task[%3d] total time: %.3f' % (index, (time.time() - st))
    logger.info(text)
    f.close()

def test(index):
    channel = grpc.insecure_channel(host)
    stub = ng_tts_pb2_grpc.NgTtsServiceStub(channel)
    req = ng_tts_pb2.TtsRequest()
    req.lang = lang_pb2.kor
    req.sampleRate = sample_rate
    req.text = TTS
    req.speaker = 0
    logger.info('req.text is %s' % req.text)

    st = time.time()
    try:
        metadata={(b'tempo', b'1.0'), (b'campaign', b'1')}
        # resp = stub.SpeakWav(req, timeout=60)
        resp = stub.SpeakWav(req, metadata=metadata)
        logger.info('[%03d ] start read wav file' % index)
        f = open('test%02d.wav' % index, 'w')
        for tts in resp:
           #logger.debug('write file')
            f.write(tts.mediaData)
    except grpc.RpcError as e:
        print e
    logger.info('end time is %.3f' % (time.time() - st))
    f.close()

def main():
    cmd = raw_input('Select the number of concurrent tasks: ')
    concurrent_cnt = int(cmd)

    logger.info('Start...')
    st = time.time()

    num_cores = multiprocessing.cpu_count()
    logger.info('# fo CPU cores: %d' % num_cores)
    pool = Pool(num_cores)
    pool_outputs = pool.map(unittask, range(concurrent_cnt))
    pool.close()
    pool.join()
    """
    plist = []
    for i in range(concurrent_cnt):
        #p = Process(target=test, args=(i,))
        p = Process(target=unittask, args=(i,))
        p.start()
        plist.append(p)

    for p in plist:
        p.join()
    """ 
    logger.info('End time is %.3f' % (time.time() - st))

if __name__ == '__main__':
    host = '10.122.64.191:9999'
    sample_rate = 8000

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'h:r:m:', [])
        for option, value in opts:
            if option == '-h':
                host = value
            elif option == '-r':
                sample_rate = int(value)

        if len(args) < 1:
            usage()
            sys.exit(2)
        filename = args[0]

    except getopt.GetoptError as err:
        # print help information and exit:
        print str(err)  # will print something like "option -a not recognized"
        usage()
        sys.exit(2)

    main()

