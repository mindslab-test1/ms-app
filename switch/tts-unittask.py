#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import time
import grpc
import getopt
import ng_tts_pb2
import ng_tts_pb2_grpc
from maum.common import lang_pb2
#import multiprocessing
#from multiprocessing import Process
#from multiprocessing import Pool

# For your reference
SDN = '125.132.250.242:20051'
#TTS = '10.122.64.191:9999'
TTS = '125.132.250.242:20051'

def text_from_file(filename):
    with open(filename, "r") as f:         
        for line in f.readlines():
            li=line.lstrip()
            if not li.startswith("#"):
                tts_text = li 
        return tts_text

def unittask():
    channel = grpc.insecure_channel(host)
    stub = ng_tts_pb2_grpc.NgTtsServiceStub(channel)
    req = ng_tts_pb2.TtsRequest()
    req.lang = lang_pb2.kor
    req.sampleRate = sample_rate
    req.text = text_from_file(filename)
    req.speaker = 0
    print('TTS req.text: {}'.format(req.text.encode('utf-8')))

    st = time.time()
    try:
        metadata={(b'tempo', b'1.0'), (b'campaign', b'1')}
        # resp = stub.SpeakWav(req, timeout=60)
        resp = stub.SpeakWav(req, metadata=metadata)
        f = open('test00.wav', 'w')
        remained = 0 
        cnt = 0
        for tts in resp:
            f.write(tts.mediaData)
            remained += len(tts.mediaData)
            if (remained >= 22094): 
                cnt = cnt + 1
                if (cnt == 1):
                    print('TTS task 1st media at: {:.2f} ms'.format((time.time() - st) * 1000))
                remained = 0
    except grpc.RpcError as e:
        print >> sys.stderr, str('SpeakWav() failed with {0}: {1}'.format(e.code(), e.details()))
    f.close()

if __name__ == '__main__':
    host = TTS
    sample_rate = 8000

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'h:r:', [])
        for option, value in opts:
            if option == '-h':
                host = value
            elif option == '-r':
                sample_rate = int(value)

        if len(args) < 1:
            sys.exit(2)
        filename = args[0]

    except getopt.GetoptError as err:
        print >> sys.stderr, str(err)  
        sys.exit(2)

    start = time.time()
    unittask()

    total = (time.time() - start) * 1000
    print('TTS task total time: {:.2f} ms'.format(total))

