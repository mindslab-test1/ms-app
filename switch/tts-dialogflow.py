#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import time
import grpc
import getopt
import json
import subprocess
from functools import partial
from freeswitch import *
import ESL
import dialogflow

con = EventConsumer()

# WARNING: known bugs with hangup hooks, use with extreme caution
def hangup_hook(session, what, arg=''):
    con.cleanup()
    consoleLog("info","hangup hook for %s!!\n\n" % what)
    return
  
def input_callback(session, what, obj, arg=''):
    consoleLog("info", "input_callback() called!! \n")
    if (what == "dtmf"):
        consoleLog("info", what + " " + obj.digit + "\n")
    else:
        consoleLog("info", what + " " + obj.serialize() + "\n")
    return 
#    return "pause"

def detect_intent_texts(project_id, uuid, result, language_code):
    """Returns the result of detect intent with texts as inputs.
    Using the same `uuid` between requests allows continuation
    of the conversation."""
    session_client = dialogflow.SessionsClient()

    sess = session_client.session_path(project_id, uuid)
    consoleLog('info', 'Session path: {}\n'.format(sess))

    if result:
        text_input = dialogflow.types.TextInput(text=result, language_code=language_code)
        query_input = dialogflow.types.QueryInput(text=text_input)

        response = session_client.detect_intent(session=sess, query_input=query_input)

        consoleLog('info', ('=' * 30 + '\n'))
        consoleLog('info', 'Query text: {}'.format(response.query_result.query_text.encode('utf-8')))
        consoleLog('info', 'Detected intent: {} (confidence: {})\n'.format(
                    response.query_result.intent.display_name,
                    response.query_result.intent_detection_confidence))
        consoleLog('info', 'Fulfillment text: {}\n'.format(
                    response.query_result.fulfillment_text.encode('utf-8')))

def handler(session, args):
    uuid = session.get_uuid()
    session.sleep(100)
    session.answer()
    session.setHangupHook(hangup_hook)
    session.setInputCallback(input_callback)
    session.setVariable('InputCallback', 'input_callback')

    DIALOGFLOW_PROJECT_ID = 'maum-switch'

    consoleLog("info", "session uuid: " + uuid + " \n")

    stt_data = ' ' + uuid + ' start ko-KR'
    consoleLog("info", "uuid_brain_stt" + stt_data + " \n")
    new_api_obj = API()
    new_api_obj.execute('uuid_brain_stt', stt_data) 
    con.bind("CUSTOM", "brain_stt::transcription")

    # NOTE: 2021-01-14 by shinwc
    # NOTE: if cwd isn't defined, the file wasn't changed even though subprocess.call is done successfully
    #ret = subprocess.call(['python', 'tts-unittask.py', 'tts.txt'], cwd='/srv/maum/switch')
    #consoleLog("info", "result: " + str(ret) + " \n")

    session.execute('set', 'playback_terminators=#') 
    session.execute('speak', 'simple_tts|ko-KR|KB국민 굿데이올림카드 시그니처 커피에서 일시불 4000원 사용하셨습니다.')

    session.set_tts_params('simple_tts', 'ko-KR')
    event_count = 0
    result = ''
    while session.ready():
        session.streamFile('/srv/maum/KB_POC/sounds/Normal_1.0_20201216_22116383_7818.wav')
        event = con.pop(0)
        if event:
            event_count = event_count + 1
            consoleLog("info", "Event: " + event.getHeader('Event-Subclass') + " \n")
            consoleLog("info", "Event body: " + event.getBody() + " \n")
            try:
                body = json.loads(event.getBody())
                text = body['text'].rstrip('\n').lstrip('\n')
                result += ' ' + text.encode('utf-8')
                session.execute('break')
                result = '카드 분실했어요'
            except Exception as ex: 
                consoleLog("info", "Exception: %s \n" % ex)
        else:
            consoleLog("info", "event count: %d %s \n" % (event_count, result))
            event_count = 0
            detect_intent_texts(DIALOGFLOW_PROJECT_ID, uuid, result, 'ko')
            result = ''
            

        #session.speak('신차구매 및 특별한도 상담은 1번, 신차 구입대금 결제를 위한 가상 계좌 출금 동의 등록은 2번, 중고차 할부관련 상담은 3번을 눌러주십시오.')
        #session.speak('본인확인을 위하여 생년월일 앞 6자리를 눌러주십시오.')

    session.hangup() #hangup the call

