#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import requests
import json
import urllib
from datetime import datetime

# BASE_URL = 'http://13.209.63.182:8080/front-api'
BASE_URL = 'http://211.39.140.44:8080'
session_key = '35f98d8f-e82a-488e-9630-ccce9a0a87ca'

def open_dialog():

    url = BASE_URL + '/api/sessionRequest'
    headers = \
    {
        'Content-Type':'application/json', 
        'charset':'utf-8',
    }

    response = requests.post(url, headers=headers)
    # pp = pprint.PrettyPrinter(indent=4)
    print response
    dic = response.json()
    print json.dumps(dic, ensure_ascii=False, encoding='utf8').encode('utf8')

    global session_key
    session_key = dic['sessionKey']
    print session_key

def send_greeting():
    url = BASE_URL + '/api/iChatResponse'
    headers = \
    {
        'Content-Type':'application/json', 
        'charset':'utf-8',
    }
    payload = \
    {
      'projectId':'713a66231ba5',
      'query':'hi',
      'sessionKey':'%s' % session_key,
      'process':'greeting'
    }

    response = requests.post(url, headers=headers, params=payload)
    dic = response.json()
    print json.dumps(dic, ensure_ascii=False).encode('utf-8')
    sentence = dic['answer']['message']
    print sentence

def send_chat():
    url = BASE_URL + '/api/iChatResponse'
    headers = \
    {
        'Content-Type':'application/json', 
        'charset':'utf-8',
    }

    text = raw_input('고객 대화:')

    payload = \
    {
      'projectId':'713a66231ba5',
      'query': '%s' % text,
      'sessionKey':'%s' % session_key,
      'process':'greeting'
    }
    urllib.urlencode(payload).encode('UTF-8')

    response = requests.post(url, headers=headers, params=payload)
    dic = response.json()
    print json.dumps(dic, ensure_ascii=False).encode('utf-8')
    sentence = dic['answer']['message']
    isSuccess = dic['isSuccess']
    print isSuccess

def main():
    while True:
        print '1. open dialog'
        print '2. send greeting'
        print '3. send chat'

        cmd = raw_input('choose number: ')
        if cmd == '1':
            open_dialog()
        elif cmd == '2':
            send_greeting()
        elif cmd == '3':
            send_chat()
            break


if __name__ == '__main__':
    main()
