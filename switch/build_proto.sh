#!/bin/sh
#python -m grpc_tools.protoc -I. -I/usr/local/include --python_out=. --grpc_python_out=. ng_tts.proto
#python -m grpc_tools.protoc -I. -I/usr/local/include --python_out=. --grpc_python_out=. maum/common/lang.proto
protoc -I. -I/usr/local/include --grpc_out=. --plugin=protoc-gen-grpc=`which grpc_python_plugin` ng_tts.proto
protoc -I. -I/usr/local/include --python_out=. ng_tts.proto
protoc -I. -I/usr/local/include --grpc_out=. --plugin=protoc-gen-grpc=`which grpc_python_plugin` maum/common/lang.proto
protoc -I. -I/usr/local/include --python_out=. maum/common/lang.proto
#protoc -I. -I/usr/local/include --grpc_out=. --plugin=protoc-gen-grpc=`which grpc_cpp_plugin` ng_tts.proto
#protoc -I. -I/usr/local/include --cpp_out=. ng_tts.proto
#protoc -I. -I/usr/local/include --grpc_out=. --plugin=protoc-gen-grpc=`which grpc_cpp_plugin` maum/common/lang.proto
#protoc -I. -I/usr/local/include --cpp_out=. maum/common/lang.proto
