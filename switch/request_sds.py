#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import requests
import json
import pprint
from datetime import datetime

# BASE_URL = 'http://13.209.63.182:8080/front-api'
BASE_URL = 'http://127.0.0.1:5001'
SESSION_ID = '35f98d8f-e82a-488e-9630-ccce9a0a87ca'

def open_dialog():
    url = BASE_URL + '/dialog/openDialog'
    data = \
    {
      "chatbotId": "32",
      "accessChannel": "WEB_BROWSER",
      "user": {
        "userName": "test1",
        "userId":  "id1"
      },
      "showDebug": True
    }
    url = BASE_URL + '/dialog/openDialog'
    response = requests.post(url, json=data)
    # pp = pprint.PrettyPrinter(indent=4)
    dic = response.json()
    print json.dumps(dic, ensure_ascii=False, encoding='utf8').encode('utf8')

    global SESSION_ID
    SESSION_ID = dic['data']['sessionId']
    sentence = dic['data']['uttrData']['sentence']
    print sentence

def send_text():
    url = BASE_URL + '/dialog/sendText'
    now = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
    data = \
    {
      "speaker":"USER",
      "userId":"id1",
      "sessionId":"%s" % SESSION_ID,
      "timestamp":now
    }

    sent = raw_input('고객 대화:')
    data["sentence"] = sent
    response = requests.post(url, json=data)
    dic = response.json()
    print json.dumps(dic, ensure_ascii=False).encode('utf-8')
    sentence = dic['data']['sentence']
    print sentence

def close_dialog(session_id):
    url = BASE_URL + '/dialog/closeDialog'
    data = \
    {
      "chatbotId": "1",
      "accessChannel": "WEB_BROWSER",
      "sessionId":"%s" % session_id,
      "user": {
        "userName": "test1",
        "userId":  "id1"
      },
      "showDebug": True
    }
    response = requests.post(url, json=data)
    dic = response.json()
    print json.dumps(dic, ensure_ascii=False).encode('utf-8')
    return True

def main():
    while True:
        print '1. open dialog'
        print '2. send text'
        print '3. close dialog'

        cmd = raw_input('choose number: ')
        if cmd == '1':
            open_dialog()
        elif cmd == '2':
            send_text()
        elif cmd == '3':
            close_dialog(SESSION_ID)
            break


if __name__ == '__main__':
    main()
