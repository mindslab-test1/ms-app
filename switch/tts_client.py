#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import time
import grpc
import ng_tts_pb2
import ng_tts_pb2_grpc
from maum.common import lang_pb2
from multiprocessing import Process
from multiprocessing import Pool
import subprocess
import biz_log

logger = biz_log.getLogger('test')

TEXT = '''안녕하세요 마인즈랩입니다'''
#TEXT = '''지금부터 진행하는 내용은 고객님의 권리보호를 위해 녹음되며, 답변하신 내용은 향후 민원 발생시, 중요한 근거자료로 활용되오니, 정확한 답변 부탁드리겠습니다'''
#TEXT = '''말씀해주셔서 감사합니다, 고객님의 주소는 경기도 성남시 중원구 성남동, 사공오일번지로'''
#TEXT = '''지금부터 진행하는 내용은 고객님의 권리 보호를 위해 녹음되며'''
# TTS 발음이 이상한 경우
#TEXT = '''지금부터 진행하는 내용은 고객님의 권리보호를 위해 녹음되며, 답변하신 내용은 향후 민원 발생시, 중요한 근거자료로 활용되오니, 정확한 답변 부탁드리겠습니다'''
#TEXT = '''00보험에서 00만원 00보험에서 00만원 대출 실행됩니다'''
#TEXT = '고객님께서 선택하신 계약대출 신청을 도와 드리겠습니다'
#TEXT= '네 휴대전화번호 010 1234 5678 로 준비하실 서류를 발송해 드리겠습니다. 상황에 따라 추가 서류가 발생될 수 있고, 만기가 경과되면 이용하신 대출잔액에 대해 높은 연체이자가 부과되오니, 방문일까지의 이자를 준비하여 만기일인 1월 1일 전까지는 방문해주시고 연장시점까지 미납이나 제한사유 등이 없도록 관리 부탁드립니다. 저는 상담원 드리미였습니다. 감사합니다.'
#TEXT= '실명확인증표는 국가기관 등이 발급한 것으로서 성명, 주민등록번호 등이 기재되어 있고, 부착된 사진에 의해 본인확인이 가능해야 합니다. 실명확인증표의 예로는 주민등록증, 운전면허증, 학생증, 청소년증, 여권 등이 있으며, 학생증의 경우 사진이 부착된 생년월일 확인 가능한 학생증이어야 하며, 주민등록번호 뒷자리가 없는 학생증의 경우, 전체 주민번호를 확인할 수있는 건강보험증이나 최근 3개월 이내 발급된 주민등록초본, 가족관계증명서 중 하나가 필요합니다. 다른 문의사항 없으십니까?'
#TEXT='최근 보이스피싱 등 금융사기 피해가 급증하고 있어 범죄의 온상인 대포통장 근절을 위해 통장개설, 제신고 업무 시 금융거래목적 확인서 작성 및 객관적 증빙자료를 확인하고 있습니다. 금융거래목적확인제도의 증빙자료는 본인이 통장을 개설하는 목적을 확인할 수 있는 서류이며, 금융거래목적에 맞는 증빙서류를 제출하셔야 합니다. 증빙서류에 대한 자세한 사항은 에스엠에스로 발송해 드릴 수 있습니다. 다만, 미성년자, 대학생, 주부 등 증빙자료 제출이 어려운 경우나 별도의 증빙자료를 제출하지 못하신 경우에는 금융거래한도계좌로 개설하실 수 있습니다. 다른 문의사항 없으십니까?'
#TEXT='금융거래한도계좌란 증빙자료를 제출하지 못하였으나 계좌개설 목적이 인정되는 경우에 한하여 증빙자료 제출 시까지 금융거래한도계좌로 개설이 가능합니다. 금융거래한도계좌는 1인 1계좌만 개설 가능하며, 금융거래 목적에 대한 증빙자료 제출 시까지 다음과 같습니다.  창구 1일 출금한도는 100만원이고 국내/외 자동화기기에서 현금카드, 체크카드 1일 인출 및 이체한도 각각 30만원이며, 인터넷, 폰뱅킹 1일 이체한도는 30만원입니다. 자동화기기 통장출금 및 무통장과 무카드 등 무매체출금 등록이 불가합니다. 다른 문의사항 없으십니까?'
#TEXT='KB스타뱅킹을 통해 입출금 통장을 개설하고자 하실 경우, 만 17세 이상 개인 고객이 이용가능하며, 본인 신분증인 주민등록증나 운전면허증과 본인명의 스마트폰을 준비해 주셔야 합니다. 본인계좌 또는 영상통화로 인증을 진행하며, 영상통화를 통한 신규는 평일 08시부터 22시, 주말,공휴일 09시부터 18시에 이용이 가능합니다. 다만, 최근 20 영업일 이내에 서로 다른 금융회사에서 입출금 통장을 개설한 이력이 없어야 가입이 가능하고, KB스타뱅킹 앱을 통한 입출금통장 개설은 금융거래한도제한 계좌로만 개설이 가능합니다. 다른 문의사항 없으십니까?'
#TEXT= '안녕하세요? 국민의 평생 금융파트너 KB국민은행 콜봇입니다.  무엇을 도와드릴까요?'
#TEXT= '고객님께서 만 14세 이상이고, 본인명의로 가입하시는 경우 본인의 실명확인증표, 거래도장, 금융거래 목적 증빙서류를 가지고 영업점에 방문하시면 통장을 신규로 개설하실 수 있습니다. 다만, 서명거래를 원하시는 경우 거래도장인 생략하셔도 됩니다. 다른 문의사항 없으십니까?'
#TEXT='통장 사본제출 목적으로 예금종류 및 계좌번호, 신규일 등의 내용을 확인할 수 있는 계좌개설확인서가 있으며, 인터넷뱅킹이나 Paywell기로 발급할 수 있습니다. 계좌개설확인서는 본인계좌개설 확인용 이외의 어떠한 용도의 증명서로도 사용하실 수 없으니 참고하시기 바랍니다. 다른 문의사항 없으십니까?'
#TEXT='안녕하세요? KB국민은행 신용상담센터 AI상담원 드리미입니다. 실례지만 김국민 고객님 맞으신가요?'
#TEXT='네. 상담을 종료하겠습니다.'
ADDR = ''
SDN = '125.132.250.242:20051'
#SDN = '10.122.64.95:31050'
#SDN = '10.122.64.95:50051'
#SDN = '10.122.64.95:31050'
# OLD
TTS = '125.132.250.242:19999'
#TTS = '10.122.64.119:9999'
#TTS = '10.122.64.95:9999'
#TTS = '10.122.64.83:9999'
# 상담사 목소리
#TTS = '10.122.64.119:19999'
#TTS = '10.122.64.192:9999'
# 시니어케어 목소리
#TTS = '10.122.64.82:9999'
#TTS = '182.162.19.20:9999'

def test(index):
    channel = grpc.insecure_channel(ADDR)
    stub = ng_tts_pb2_grpc.NgTtsServiceStub(channel)
    req = ng_tts_pb2.TtsRequest()
    req.lang = lang_pb2.kor
    #req.sampleRate = 16000
    req.sampleRate = 8000
    req.text = TEXT
    req.speaker = 0

    # Write the TTS request back to disk
    fb = open('TtsRequest.bin', 'wb')
    fb.write(req.SerializeToString())
    fb.close()

    st = time.time()
    try:
        metadata={(b'tempo', b'1.0'), (b'campaign', b'1'), (b'samplerate', b'8000'), (b'codec', b'pcm')}
        # resp = stub.SpeakWav(req, timeout=60)
        resp = stub.SpeakWav(req, metadata=metadata)
        logger.info('[%03d ] start read wav file' % index)
        f = open('test%02d.wav' % index, 'w')
        for tts in resp:
            #logger.debug('write file')
            f.write(tts.mediaData)
    except grpc.RpcError as e:
        print e
    logger.info('end time is %.3f' % (time.time() - st))
    f.close()

def main():
    global ADDR
    cmd = raw_input('Select Server - 1. SDN (%s), 2. TTS (%s): ' % (SDN, TTS))
    if cmd == '1':
        ADDR = SDN
    else:
        ADDR = TTS

    cmd = raw_input('Select Concurrent count: ')
    concurrent_cnt = int(cmd)

    logger.info('start...')
    plist = []
    st = time.time()
    """
    pool = Pool(6)
    pool_outputs = pool.map(test, range(concurrent_cnt))
    pool.close()
    pool.join()
    print('Pool: ', pool_outputs)
    """
    for i in range(concurrent_cnt):
        p = Process(target=test, args=(i,))
        p.start()
        plist.append(p)

    for p in plist:
        p.join()
    logger.info('total time is %.3f' % (time.time() - st))

if __name__ == '__main__':
    main()

