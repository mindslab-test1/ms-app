#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import time
import grpc
import ng_tts_pb2
import ng_tts_pb2_grpc
from maum.common import lang_pb2
import multiprocessing
from multiprocessing import Process
from multiprocessing import Pool
import biz_log

logger = biz_log.getLogger('test')

#TEXT = '국민의 행복생활 파트너 KB국민카드입니다. 양해말씀드립니다. 사회적 거리두기 2단계 시행에 따라 상담인원 축소로 대기시간이 늘어나 고객님의 불편이 있을 수 있습니다.'
#TEXT = '죄송하지만 ARS , 홈페이지, 모바일 앱, 챗봇 등을 이용해주시기 바랍니다.'
#TEXT = '양해 말씀 드립니다. 현재 모바일앱 지문로그인 및 삼성페이 지문인식 장애가 있습니다. 다른 로그인 방식및 결제 이용을 부탁드립니다. 불편을 드려 죄송합니다.'
#TEXT = '지금 안내 드린 내용에 동의하시면 1번, 2시간 지연입금을 원하시면 2번, 취소하실려면 9번을 눌러주세요. 대출신청이 완료되었으며, 대출금은 2시간이후 입금됩니다. KB국민카드를 이용해주셔서 감사합니다.'
#TEXT = '통화 내용은 녹음되며, 본인이 아닌 경우 상담이 제한될 수 있습니다.'
#TEXT = '신차구매 및 특별한도 상담은 1번, 신차 구입대금 결제를 위한 가상 계좌 출금 동의 등록은 2번, 중고차 할부관련 상담은 3번을 눌러주십시오.'
#TEXT = '본인확인을 위하여 생년월일 앞 6자리를 눌러주십시오.'
#TEXT = '본 서비스외에 내용은 상담이 제한될 수 있습니다. 상담직원을 연결해 드리겠습니다.'
#TEXT = '비밀번호가 등록된 개인신용카드 또는 개인체크카드에 대해서만 가능하며, 분실접수 시 안내 받은 접수번호를 아는 경우 ARS를 통해 직접 해제 가능합니다.'
#TEXT = '다만, 버스, 지하철 등 교통기능의 해제는 2~3영업일 정도 소요될 수 있으며 우체국을 통해 발급받은 에버리치 제휴카드는 우체국에서 분실신고해제가 가능하니 참고 부탁드립니다.'
#TEXT = '고객님의,신용카드는 연체중이며,오늘 현재 연체금액은 ^*10896원 입니다. 또한 방금 안내 받으신 금액을 제외하고 오늘 기준으로 남아 있는 미청구 카드사용금액은 ^*184260원 입니다.'
#TEXT = '감사합니다. 요청하신 단기카드대출, 5000000원이 고객님의 결제계좌통장으로 이체되었습니다. 단기카드대출 이체 후, 고객님의 통합한도내에서 사용가능한 단기카드대출 한도는 600000원입니다.'
#TEXT = 'KB국민 nori 체크카드 RF, 주 이비카드택시법인,에서 일시불 ^*184260원, 사용'
#TEXT ='일시상환방식으로 고객님의 프리미엄론 가능금액은 최고 12000000원이고 대출기간은 36 개월로 신청가능합니다. 고객님의 자격유효기간은 2020년 07월 31일까지이고 이자율은 2020년 07월 31일까지 정상 이자율인 16.05%에서 20% 할인되어 실제 적용 이자율은 12.84%입니다.'
#TEXT = '고객님께서 신청하신 대출금액은 600만원이고 대출 기간은 36 개월이며 거치기간은 없습니다.'
#TEXT = '고객님께서 신청하신 대출금액은 550만원이고 분할상환기간은 24 개월이며 거치기간은 3 개월입니다. 거치기간 선택시 총대출기간은 27 개월이며 거치기간만큼 대출기간이 연장됩니다. 거치기간종료후 잔여기간동안 원금을 분할상환하오니 착오 없으시기 바랍니다.'
#TEXT = '2,0,2,0,0,6,3,0,0,9,2,0, 코웨이 KB국민카드 삼릉주유소에서 일시불 50000원 사용'
#TEXT = '0 6 3 0   0 7 3 8 KB국민 굿데이올림카드 지에스25 쌍림중앙점에서 일시불 4500원 사용'
#TEXT = '0 6 2 9   0 8 2 3 KB국민 굿데이올림카드 홈플러스익스프레스고양성사점일반에서 일시불 ^*14590원 사용'
TEXT = '0 6 2 9   0 8 3 0 KB국민 굿데이올림카드 시그니처 커피에서 일시불 4000원 사용하셨습니다.'
ADDR = ''
SDN = '10.122.64.191:50051'
#SDN = '10.122.64.152:50051'
# OLD
#TTS = '10.122.64.83:9999'
# 상담사 목소리
#TTS = '10.122.64.65:10000'
#TTS = '10.122.64.191:9999'
#TTS = '10.122.64.192:9999'
TTS = '10.122.64.191:9999'
# 시니어케어 목소리
#TTS = '10.122.64.82:9999'
#TTS = '182.162.19.20:9999'

def load_test(index):
    channel = grpc.insecure_channel(ADDR)
    stub = ng_tts_pb2_grpc.NgTtsServiceStub(channel)
    req = ng_tts_pb2.TtsRequest()
    req.lang = lang_pb2.kor
    #req.sampleRate = 16000
    req.sampleRate = 8000
    req.text = TEXT
    logger.info('req.text is %s' % req.text)
    req.speaker = 0

    st = time.time()
    try:
        metadata={(b'tempo', b'1.0'), (b'campaign', b'1')}
        # resp = stub.SpeakWav(req, timeout=60)
        resp = stub.SpeakWav(req, metadata=metadata)
        remained = 0 
        cnt = 0
        for tts in resp:
            remained += len(tts.mediaData)
            #text = '[%3d ] count: %3d' % (index, cnt)
            #logger.info(text)
            if (remained >= 22094): 
                cnt = cnt + 1
                if (cnt == 1):
                    text = '---> [%3d ] rx[%3d ]: %3d, time: %.3f' % (index, cnt, remained, (time.time() -st))
                else:
                    text = '[%3d ] rx[%3d ]: %3d' % (index, cnt, remained)
                logger.info(text)
                remained = 0
    except grpc.RpcError as e:
        print e
    text = '[%3d ] end time: %.3f' % (index, (time.time() - st))
    logger.info(text)

def test(index):
    channel = grpc.insecure_channel(ADDR)
    stub = ng_tts_pb2_grpc.NgTtsServiceStub(channel)
    req = ng_tts_pb2.TtsRequest()
    req.lang = lang_pb2.kor
    #req.sampleRate = 16000
    req.sampleRate = 8000
    req.text = TEXT
    logger.info('req.text is %s' % req.text)
    req.speaker = 0

    st = time.time()
    try:
        metadata={(b'tempo', b'1.0'), (b'campaign', b'1')}
        # resp = stub.SpeakWav(req, timeout=60)
        resp = stub.SpeakWav(req, metadata=metadata)
        logger.info('[%03d ] start read wav file' % index)
        f = open('test%02d.wav' % index, 'w')
        for tts in resp:
           #logger.debug('write file')
            f.write(tts.mediaData)
    except grpc.RpcError as e:
        print e
    logger.info('end time is %.3f' % (time.time() - st))
    f.close()

def main():
    global ADDR
    cmd = raw_input('Select Server - 1. SDN (%s), 2. TTS (%s): ' % (SDN, TTS))
    if cmd == '1':
        ADDR = SDN
    else:
        ADDR = TTS

    cmd = raw_input('Select Concurrent count: ')
    concurrent_cnt = int(cmd)

    logger.info('start...')
    plist = []
    st = time.time()

    num_cores = multiprocessing.cpu_count()
    logger.info('# fo CPU cores: %d' % num_cores)
    pool = Pool(8)
    pool_outputs = pool.map(load_test, range(concurrent_cnt))
    pool.close()
    pool.join()
    print('Pool: ', pool_outputs)
    """ 
    for i in range(concurrent_cnt):
        #p = Process(target=test, args=(i,))
        p = Process(target=load_test, args=(i,))
        p.start()
        plist.append(p)

    for p in plist:
        p.join()
    """
    logger.info('total time is %.3f' % (time.time() - st))

if __name__ == '__main__':
    main()

