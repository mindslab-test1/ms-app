#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask import Flask, request, jsonify, render_template
import os
import dialogflow
import requests
import json
import pusher
from flask_socketio import SocketIO

app = Flask(__name__)

app.config['SECRET_KEY'] = 'vnkdjnfjknfl1232#'
socketio = SocketIO(app)

# run Flask app
if __name__ == "__main__":
    socketio.run(app, debug=True)
    #app.run(host='0.0.0.0', port=5000, debug=False)

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/webhook', methods=['POST'])
def webhook():
    data = request.get_json(silent=True)
    print data['uuid']
    print data['answer']
    print data['intent']
    reply = {
        "fulfillmentText": "Ok",
    }
    return jsonify(reply)
"""
def webhook():
    data = request.get_json(silent=True)
    if data['queryResult']['queryText'] == '네':
        reply = {
            "fulfillmentText": "Ok. Tickets booked successfully.",
        }
        return jsonify(reply)
    elif data['queryResult']['queryText'] == '아니요':
        reply = {
            "fulfillmentText": "Ok. Booking cancelled.",
        }
        return jsonify(reply)
"""

@app.route('/send_message', methods=['POST'])
def send_message():
    message = request.form['message']
    project_id = os.getenv('DIALOGFLOW_PROJECT_ID')
    fulfillment_text = detect_intent_texts(project_id, "unique", message, 'en')
    response_text = { "message":  fulfillment_text }
    return jsonify(response_text)

def detect_intent_texts(project_id, session_id, text, language_code):
    session_client = dialogflow.SessionsClient()
    session = session_client.session_path(project_id, session_id)

    if text:
        text_input = dialogflow.types.TextInput(text=text, language_code=language_code)
        query_input = dialogflow.types.QueryInput(text=text_input)
        response = session_client.detect_intent(session=session, query_input=query_input)
        return response.query_result.fulfillment_text

