# ansible-role-maumswitch 

This is an ansible role for building a [Freeswitch](https://freeswitch.org/) v1.10.5 that is configured to work with the [maumswitch](https://github.com/shinwc/maumswitch) module.  The [mod_audio_fork](https://github.com/davehorton/drachtio-freeswitch-modules/blob/master/modules/mod_audio_fork/README.md) module is built and installed as part of this role.

Additionally, by default, these additional modules will be built and included:
- [mod_brain_stt](https://github.com/shinwc/maumswitch/maumswitch-modules/mod_brain_stt)
- [mod_simple_tts](https://github.com/shinwc/maumswitch/maumswitch-modules/mod_simple_tts)

## Role variables

Available variables are listed below, along with default values (see defaults/main.yml):

```
grpc_version: v1.24.1
```
The version of grpc to build

```
freeswitch_sources_path: /usr/local/src/freeswitch/
```
path to the FreeSwitch source directory

```
freeswitch_path: /usr/local/freeswitch/
```
path to the FreeSwitch install directory

```
freeswitch_secret: ClueCon 
```
freeswitch secret

```
freeswitch_sip_port: 5080
freeswitch_dtls_sip_port: 5081
```
sip ports to listen on

```
freeswitch_socket_acl:
  - 0.0.0.0/0
```
add any addresses (in cidr format) that should be allowed to connect to event socket

```
freeswitch_modules_template: ../templates/modules.conf 
```
modules.conf file used for Freeswitch compilation

```
freeswitch_configure_command: configure --with-lws=yes
```
freeswitch configure command - you can add option

## Example playbook
```
---
- hosts: localhost
  remote_user: root
  vars:
    - remove_source: false
  roles:
    - ansible-role-maumswitch
```
