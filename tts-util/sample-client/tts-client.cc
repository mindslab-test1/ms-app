#include <memory>
#include <iostream>
#include <thread>
#include <stdio.h>
#include <unistd.h>

#include <grpc++/grpc++.h>
#include "ng_tts.grpc.pb.h"
#include "ng_tts.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReaderWriter;

using maum::brain::tts::NgTtsService;
using maum::brain::tts::TtsRequest;
using maum::brain::tts::TtsMediaResponse;

void ReadStream(ClientReaderWriter<TtsRequest, TtsMediaResponse> *stream, FILE *fp)
{
  TtsMediaResponse resp;
  while (stream->Read(&resp)) {
    std::string bytes = resp.mediadata();
    std::cout << "[RX] TTS data: " << bytes.size() << std::endl;
    fwrite(bytes.c_str(), 1, bytes.size(), fp);
  }
  std::cout << "stream read end..." << std::endl;
}

int main(int argc, char *argv[])
{
  auto channel = grpc::CreateChannel("221.168.33.192:50051", grpc::InsecureChannelCredentials());
  auto stub = NgTtsService::NewStub(channel);

  TtsRequest req;
  //req.set_lang(maum::common::Lang::ko_KR);
  req.set_text("인생은 왜 이렇게 힘들지");

  ClientContext ctx;
  ctx.AddMetadata("tempo", "1.1");
  ctx.AddMetadata("campaign", "1");
  ctx.AddMetadata("samplerate", "8000");

  auto stream = stub->SpeakWav(&ctx, req);

  const char *filename = "intro.pcm";
  FILE *fp = fopen(filename, "wb");
  if (fp == NULL) {
    std::cerr << "cannot open file : " << filename << std::endl;
    return -1;
  }

  TtsMediaResponse resp;

  while (stream->Read(&resp)) {
    std::string bytes = resp.mediadata();
    std::cout << "[RX] TTS data: " << bytes.size() << std::endl;
    fwrite(bytes.c_str(), 1, bytes.size(), fp);
  }
  std::cout << "stream read end..." << std::endl;

  //std::thread result_thrd(ReadStream, stream.get());
  //result_thrd.join();

  auto status = stream->Finish();
  if (status.ok()) {
    std::cout << "result: OK" << std::endl;
  } else {
    std::cout << "failed: " << status.error_message() << std::endl;
  }

  return 0;
}
