#!/bin/sh
#protoc --cpp_out=. --grpc_out=. --plugin=protoc-gen-grpc=$MAUM_ROOT/bin/grpc_cpp_plugin ng_tts.proto
#g++ -std=c++11 -I /usr/local/include/ -L /usr/local/lib/ tts-client.cc ng_tts.grpc.pb.cc ng_tts.pb.cc maum/common/lang.pb.cc -o tts-client -l grpc++ -l protobuf 
g++ -std=c++11 -I /usr/local/include/ -I. -L /usr/local/lib/ tts-client.cc ng_tts.grpc.pb.cc ng_tts.pb.cc maum/common/lang.pb.cc -o tts-client -l grpc++ -l protobuf 
