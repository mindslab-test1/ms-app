#/usr/bin/env pyehon
# -*- coding: utf-8 -*-
import time
import datetime
import re
import json
import requests
import urllib
import threading
import logging
import SocketServer
from ESL import *

# 와이즈넛 폴봇용 챗봇 서버 정보
BASE_URL = 'http://211.39.140.44:8080'
#WEBHOOK_URL = 'http://10.122.65.130:3000/webhook'
WEBHOOK_URL = 'http://221.168.32.165:5000/webhook'
LOG_FORMAT = "[%(asctime)-10s] (%(filename)s:%(lineno)d) %(levelname)s %(threadName)s - %(message)s"

logging.basicConfig(format=LOG_FORMAT)
logger = logging.getLogger("polbot")
logger.setLevel(logging.INFO)

class IVRServer(object):
    def __init__(self, name, conn):
        self.name = name
        self.conn = conn
        logger = logging.getLogger("visual-ivr.{}".format(self.name))
        self.timeout = 5
        self.intent = 'greeting'
        self.result = 'hi'
        self.enable_chat = False
        self.headers = \
        {
            'content-type':'application/json', 
            'charset':'utf-8',
        }
        self.pages = []
        self.page_id = 0
        self.div_id = 0
        self.digit = 0

    def read_json(self):
        with open('ivr_menu.json') as json_file:
            self.ivr = json.load(json_file)
            logger.debug('ivr meun json: {}'.format(self.ivr['pages']))
            for index, page in enumerate(self.ivr['pages']):
                self.pages.append(page)
                
                logger.info('index: {}, json: {}'.format(index, self.pages[index]['div']))
                #logger.info('index: {}, json: {}'.format(index, x.encode('utf-8') for x in self.pages[index]['div']))

    def setup(self):
        # 'IVR menu' json 파일을 읽는다
        self.read_json()

        self.info = self.conn.getInfo()
        self.uuid = self.info.getHeader('caller-unique-id')
        evt = json.loads(self.info.serialize('json'))
        #self.uuid = evt['Caller-Unique-ID']
        self.caller_id_number = evt['Caller-Caller-ID-Number']
        self.caller_id_name = evt['Caller-Caller-ID-Name']
        self.callstate = evt['Channel-Call-State']
        #logger.debug('info: {}'.format(evt))
        logger.info('{}, {}, {}, {}'.format(self.uuid, self.caller_id_number, self.caller_id_name, self.callstate))
        self.conn.filter('unique-id', self.uuid)
        #self.conn.sendRecv("".format(self.uuid))
        self.conn.events('plain', 'all')
        #self.conn.sendRecv("linger")
        self.conn.execute('set', 'hangup_complete_with_xml=true', self.uuid)
        self.conn.execute('ring_ready', '', self.uuid)
        self.conn.execute('sleep', '2000', self.uuid)
        self.conn.execute('answer', '', self.uuid)

    def send_webhook(self, path, payload):
        url = WEBHOOK_URL + path
        response = requests.post(url, headers=self.headers, data=json.dumps(payload))
        #resp = response.json()
        #json.dumps(resp, ensure_ascii=False).encode('utf-8')
        logger.info('webhook response: {}'.format(response)) 

    def dialog_chat(self):
        # json 파일(ivr menu) 기반 대화 진행
        self.timestamp = (int)(time.time() * 1000000)
        self.datetime = datetime.datetime.fromtimestamp(self.timestamp/1000000.0) 
        logger.info('chat time: {}, {}'.format(self.timestamp, self.datetime))

        if self.page_id < len(self.pages):
            items = self.pages[self.page_id]['div']
            logger.info('div_id: {}, json: {}'.format(self.div_id, items[self.div_id]['text'].encode('utf-8')))
            self.answer = items[self.div_id]['text'].encode('utf-8')
            self.intent = 'in_process'
            logger.info('answer: {}'.format(self.answer)) 
            logger.info('intent: {}'.format(self.intent))
            #logger.info('stt-hint: {}'.format(x.encode('utf-8') for x in items[self.div_id]['menu'][self.div_id]['stt-hint']))
            self.conn.execute('break', '', self.uuid)
            self.conn.execute('speak', 'simple_tts|ko-KR|' + self.answer, self.uuid)
            # TODO: need to send div items in pages to form HTML page
            payload = \
            {
              'uuid':'%s' % self.uuid,
              'answer':'%s' % self.answer,
              'intent':'%s' % self.intent,
              'caller':'%s' % self.caller,
              'callee':'%s' % self.callee,
              'isConsultant':'%s' % 'True',
              #'isConsultant':'%s' % self.enable_chat,
              'timestamp':'%d' % self.timestamp,
              'datetime':'%s' % self.datetime,
              'div': items
            }
            logger.debug('payload: {}'.format(payload))
            self.send_webhook('/chat', payload)
            # TODO:
            #if self.result.decode('utf-8') in items[self.div_id]['menu'][0]['stt-hint']:
            #    self.digit = items[self.div_id]['menu'][0]['digit']
            #    logger.info('digit: {}'.format(self.digit))
            #    self.div_id += 1
            #    self.page_id +=1
            #elif self.result.decode('utf-8') in items[self.div_id]['menu'][1]['stt-hint']:
            #    self.digit = items[self.div_id]['menu'][1]['digit']
            #    logger.info('digit: {}'.format(self.digit))
            #    self.div_id += 1
            #    self.page_id +=1
            #else:
            #    self.conn.execute('speak', 'simple_tts|ko-KR|다시 말씀해주세요', self.uuid) 

            self.div_id += 1
            if self.div_id >= len(items):
                self.div_id = 0
                self.page_id += 1
            
        else:
            self.conn.setEventLock('1')
            self.conn.execute('speak', 'simple_tts|ko-KR|상담 종료합니다', self.uuid)
            self.conn.execute('hangup')
            self.conn.setEventLock('0')

    def on_channel_park(self, e):
        self.code = e.getHeader('code')
        logger.debug('on_channel_park: code={}'.format(self.code))

    def on_channel_answer(self, e):
        logger.info('~~~ answer channel a-leg ~~~')
        evt = json.loads(e.serialize('json'))
        self.caller_id_number = evt['Caller-Caller-ID-Number']
        self.caller_id_name = evt['Caller-Caller-ID-Name']
        logger.info('{}, {}, {}, {}'.format(self.uuid, self.caller_id_number, self.caller_id_name, self.callstate))
        stt_command = 'uuid_brain_stt ' + self.uuid + ' start ko-KR'
        logger.info('stt_command: {}'.format(stt_command))
        self.conn.bgapi(stt_command) 
        # 대화 시작
        self.dialog_chat()

    def on_channel_bridge(self, e):
        evt = json.loads(e.serialize('json'))
        #logger.info('bridge: {}'.format(e.serialize('plain')))
        dest_num = e.getHeader('other-leg-destination-number')
        if dest_num == '182':
            logger.info('~~~ bridge channel c-leg ~~~')
            self.cleg_uuid = evt['Other-Leg-Unique-ID']
            self.cleg_callee_id_number = evt['Other-Leg-Callee-ID-Number']
            self.cleg_callee_id_name = evt['Other-Leg-Callee-ID-Name']
            logger.info('{}, {}, {}, {}'.format(self.cleg_uuid, self.cleg_callee_id_number, 
                self.cleg_callee_id_name, self.callstate))
            self.conn.filter('unique-id', str(self.cleg_uuid))
            stt_command = 'uuid_brain_stt ' + str(self.uuid) + ' stop ko-KR'
            logger.info('stt_command: {}'.format(stt_command))
            self.conn.bgapi(stt_command) 
            stt_command = 'uuid_brain_stt ' + str(self.bleg_uuid) + ' stop ko-KR'
            logger.info('stt_command: {}'.format(stt_command))
            self.conn.bgapi(stt_command) 
            #self.conn.execute('send dtmf', '0', self.uuid) 
        else:
            logger.info('~~~ bridge channel b-leg ~~~')
            self.bleg_uuid = evt['Other-Leg-Unique-ID']
            self.bleg_callee_id_number = evt['Other-Leg-Callee-ID-Number']
            self.bleg_callee_id_name = '상담사'
            logger.info('{}, {}, {}, {}'.format(self.bleg_uuid, self.bleg_callee_id_number, 
                self.bleg_callee_id_name, self.callstate))
            stt_command = 'uuid_brain_stt ' + str(self.bleg_uuid) + ' start ko-KR'
            logger.info('stt_command: {}'.format(stt_command))
            self.conn.bgapi(stt_command) 
            self.conn.filter('unique-id', str(self.bleg_uuid))

    def on_channel_unbridge(self, e):
        evt = json.loads(e.serialize('json'))
        #logger.info('unbridge: {}'.format(e.serialize('plain')))
        uuid = e.getHeader('Other-Leg-Unique-ID')
        if uuid == self.bleg_uuid:
            logger.info('~~~ unbridge channel b-leg ~~~')
        else:
            logger.info('~~~ unbridge channel c-leg ~~~')

    def on_channel_hangup(self, e):
        uuid = e.getHeader('Caller-Unique-ID')
        if uuid == self.uuid:
            logger.info('~~~ channel a-leg hangup ~~~')
        elif uuid == self.bleg_uuid:
            logger.info('~~~ channel b-leg hangup ~~~')
        elif uuid == self.cleg_uuid:
            logger.info('~~~ channel c-leg hangup ~~~')
            if self.callstate == 'ACTIVE':
                stt_command = 'uuid_brain_stt ' + str(self.uuid) + ' start ko-KR'
                logger.info('stt_command: {}'.format(stt_command))
                self.conn.bgapi(stt_command) 

    def on_channel_hangup_complete(self, e):
        logger.debug('hangup_complete: {}'.format(e.serialize('plain')))

    def on_channel_execute_complete(self, e):
        evt = json.loads(e.serialize('json'))
        app = evt['Application']
        #app_data = evt['Application-Data'] 
        logger.info('execute complete: {}'.format(app)) 

    def on_custom(self, e):
        uuid = e.getHeader('Unique-ID')
        #logger.info('uuid: {}'.format(uuid))
        body = json.loads(e.getBody())
        text = body['text'].rstrip('\n').lstrip('\n')
        self.result = text.encode('utf-8')
        logger.info('on_custom_result : {}'.format(self.result))

        payload = \
        {
          'uuid':'%s' % uuid,
          'result':'%s' % self.result,
          'timestamp':'%s' % self.timestamp,
          'datetime':'%s' % self.datetime,
          'caller':'%s' % self.caller,
          'callee':'%s' % self.callee,
          'isConsultant':'%s' % 'False' if self.enable_chat else 'True',
          'callerIdNumber':'%s' % self.caller_id_number if uuid == self.uuid else self.bleg_callee_id_number,
          'callerIdName':'%s' % self.caller_id_name if uuid == self.uuid else self.bleg_callee_id_name
        }
        logger.debug('payload: {}'.format(payload))
        self.send_webhook('/stt', payload)
        logger.info('[{}] result: {}, {}'.format(uuid[:8], self.result, self.datetime))
        if len(self.result):
            self.dialog_chat()
        logger.debug('--- End of on_custom() ---')

    def process_callstate(self, e):
        uuid = e.getHeader('Unique-ID')
        payload = \
        {
            'uuid':'%s' % self.uuid,
            'caller':'%s' % self.caller,
            'callee':'%s' % self.callee,
            'callstate':'%s' % self.callstate,
            'datetime':'%s' % self.datetime,
        }
        logger.info('callstate: {}'.format(str(payload)))
        self.send_webhook('/callstate', payload)

    def alert_disconnection(self):
        payload = \
        {
            'disconnected':self.disconnected,
            'uuid':'%s' % self.uuid,
            'caller':'%s' % self.caller,
            'callee':'%s' % self.callee,
            'datetime':'%s' % self.datetime,
        }
        logger.info('disconnection payload: {}'.format(str(payload)))
        self.send_webhook('/disconnection', payload)


    def process_event(self):
        while self.conn.connected:
            e = self.conn.recvEvent()
            evt = json.loads(e.serialize('json'))
            event_name = evt['Event-Name'].lower()
            self.timestamp = e.getHeader('Event-Date-Timestamp')
            if self.timestamp !=  None:
                self.datetime = datetime.datetime.fromtimestamp((int)(self.timestamp)/1000000.0) 
            caller = e.getHeader('Caller-Username')
            callee = e.getHeader('Caller-Destination-Number')
            if caller and callee:
                self.caller = caller
                self.callee = callee
            method_name = 'on_%s' % event_name
#            logger.info(method_name)
            if event_name == 'server_disconnected':
                self.disconnected = True
                self.alert_disconnection()
                logger.info('disconnected!')
                break
            if event_name == 'channel_park':
                logger.info('on_channel_park()')
                self.on_channel_park(e)
            if event_name == 'channel_answer':
                self.on_channel_answer(e)
            if event_name == 'channel_hangup':
                self.on_channel_hangup(e)
            if event_name == 'channel_hangup_complete':
                self.on_channel_hangup_complete(e)
            if event_name == 'channel_bridge':
                self.on_channel_bridge(e)
            if event_name == 'channel_unbridge':
                self.on_channel_unbridge(e)
            if event_name == 'channel_callstate':
                self.callstate = evt['Channel-Call-State']
                self.process_callstate(e)
                logger.info('callstate: {}'.format(self.callstate))
            if event_name == 'channel_state':
                self.state = evt['Channel-State']
                logger.info('state: {}'.format(self.state))
            if event_name == 'custom':
                subclass = evt['Event-Subclass'].lower()
                logger.debug('subclass: {}'.format(subclass))
                if subclass == 'brain_stt::transcription':
                    self.on_custom(e)
            if event_name == 'channel_execute_complete':
                self.on_channel_execute_complete(e)

        
class ESLRequestHandler(SocketServer.BaseRequestHandler):
    def setup(self):
        logger.info('{} connected!'.format(self.client_address))

        fd = self.request.fileno()
        logger.debug('fd: {}'.format(fd))

        con = ESLconnection(fd)
        logger.debug('Connected: {}'.format(con.connected))
        logger.debug('Thread Name:{}'.format(threading.current_thread().name))
        name = threading.current_thread().name
        if con.connected():
            polbot = IVRServer(name, con)
            polbot.setup()
            polbot.process_event()

#server host is a tuple ('host', port)
server = SocketServer.ThreadingTCPServer(('127.0.0.1', 8046), ESLRequestHandler)
server.serve_forever()
